﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.IO;

namespace HotetecWS
{
    /// <summary>
    /// Descripción breve de wsHotetec
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class wsHotetec : System.Web.Services.WebService
    {
        clsFuncionesReservas objFuncionesReservas;

        [WebMethod]
        public BookingNotificationResponse BookingNotification(BookingNotificationBookingNotificationRequest BookingNotificationRequest)
        {
            BookingNotificationResponse objBookingNotificationResponse = new BookingNotificationResponse();
            string strIDESES = "";
            string strBookingReference = "";
            DataTable dtRESERVAS;
            bool blnLOCALIZADOR_EXISTE_EN_RUTA = false;
            string strColaNombreFichero = string.Format("{0:ddMMyyyy}_{1:hh_mm_ss}.xml", DateTime.Now, DateTime.Now);
            objFuncionesReservas = new clsFuncionesReservas();
            string strLOG_HOTETEC = "";
            string strDelimitadorLog = "-SALTO-";
            string strHOTEL = "";
            string strAGENCIA = "";

            try
            {
                strLOG_HOTETEC += "**********************************************************************************************" + strDelimitadorLog;
                strLOG_HOTETEC += "INICIO DE LA TRANSACCIÓN" + strDelimitadorLog;
                strLOG_HOTETEC += string.Format("Fecha Notificación: {0} {1}{2}", DateTime.Now.ToLongDateString(), DateTime.Now.ToShortTimeString(), strDelimitadorLog);

                //Vemos si se recupera algo
                if (BookingNotificationRequest == null)
                {
                    strLOG_HOTETEC += string.Format("NO SE RECUPERA NINGÚN OBJETO: {0}{1}", DateTime.Now.ToLongDateString(), strDelimitadorLog);
                    return objBookingNotificationResponse;
                }

                //Comprobamos que hay reservas de habitaciones
                if (BookingNotificationRequest.bookingRoom == null || BookingNotificationRequest.bookingRoom.Length == 0)
                {
                    if (BookingNotificationRequest.ideses != null && BookingNotificationRequest.ideses.Trim() != "")
                        strLOG_HOTETEC += string.Format("IDENTIFICADOR DE SESIÓN: {0}{1}", BookingNotificationRequest.ideses.Trim(), strDelimitadorLog);

                    strLOG_HOTETEC += "NO SE RECUPERA NINGUNA RESERVA EN EL OBJETO" + strDelimitadorLog;

                    return objBookingNotificationResponse;
                }

                //Recuperamos el identificador de sesión y se lo asignamos ya a la respuesta
                strIDESES = BookingNotificationRequest.ideses.Trim();
                strLOG_HOTETEC += string.Format("IDENTIFICADOR DE SESIÓN: {0}{1}", strIDESES, strDelimitadorLog);
                objBookingNotificationResponse.ideses = BookingNotificationRequest.ideses;

                //Recuperamos el localizador del sistema de reserva
                strBookingReference = BookingNotificationRequest.bookingReference;
                if (strBookingReference.Trim() == "")
                {
                    strLOG_HOTETEC += "NO SE RECUPERA EL LOCALIZADOR DEL SISTEMA DE RESERVA (DE HOTETEC)" + strDelimitadorLog;
                    return objBookingNotificationResponse;
                }
                strLOG_HOTETEC += string.Format("LOCALIZADOR DEL SISTEMA DE RESERVA (DE HOTETEC): {0}{1}", strBookingReference, strDelimitadorLog);

                strAGENCIA = BookingNotificationRequest.stockist; //stockist
                if (strAGENCIA.Trim() == "")
                {
                    strLOG_HOTETEC += "NO SE RECUPERA LA AGENCIA" + strDelimitadorLog;
                    return objBookingNotificationResponse;
                }
                strLOG_HOTETEC += string.Format("AGENCIA IDENTIFICADA: {0}{1}", strAGENCIA, strDelimitadorLog);

                //Veamos si la referencia existe ya en la bd
                blnLOCALIZADOR_EXISTE_EN_RUTA = false;
                dtRESERVAS = objFuncionesReservas.RecuperaReservasXBono(strBookingReference);
                if (dtRESERVAS != null && dtRESERVAS.Rows.Count > 0)
                {
                    //Existen reservas con ese localizador en nuestro sistema Ruta.
                    blnLOCALIZADOR_EXISTE_EN_RUTA = true;
                    strLOG_HOTETEC += "EXISTEN RESERVAS CON ESTE LOCALIZADOR EN EL SISTEMA DE RUTA" + strDelimitadorLog;
                }

                //Veamos qué tipo de transacción se está operando: creación, cancelación o modificación
                switch (BookingNotificationRequest.typeTransaction.Trim().ToLower())
                {
                    case "creation":
                        strLOG_HOTETEC += "EL TIPO DE TRANSACCIÓN A REALIZAR ES: INSERTAR (creation)" + strDelimitadorLog;
                        if (blnLOCALIZADOR_EXISTE_EN_RUTA)
                        {
                            //La reserva no debería haberse encontrado en el sistema...
                            strLOG_HOTETEC += "SE HA(N) ENCONTRADO) RESERVA(S) EN EL SISTEMA PARA EL LOCALIZADOR, NO SE PUEDE(N) INSERTAR" + strDelimitadorLog;
                        }
                        else
                            this.Creation_Booking(BookingNotificationRequest, strAGENCIA, ref strLOG_HOTETEC);
                        break;
                    case "cancellation":
                        strLOG_HOTETEC += "EL TIPO DE TRANSACCIÓN A REALIZAR ES: CANCELAR (cancellation)" + strDelimitadorLog;
                        if (!blnLOCALIZADOR_EXISTE_EN_RUTA)
                        {
                            //La reserva debería haberse encontrado en el sistema...
                            strLOG_HOTETEC += "LA(S) RESERVA(S) NO SE HAN ENCONTRADO EN EL SISTEMA PARA EL LOCALIZADOR, NO SE PUEDE(N) CANCELAR" + strDelimitadorLog;
                        }
                        else
                            this.Cancellation_Booking(BookingNotificationRequest, dtRESERVAS, strAGENCIA, ref strLOG_HOTETEC);
                        break;
                    case "modification":
                        strLOG_HOTETEC += "EL TIPO DE TRANSACCIÓN A REALIZAR ES: EDICIÓN (modification)" + strDelimitadorLog;
                        if (!blnLOCALIZADOR_EXISTE_EN_RUTA)
                        {
                            //La reserva debería haberse encontrado en el sistema...
                            strLOG_HOTETEC += "LA(S) RESERVA(S) NO SE HAN ENCONTRADO EN EL SISTEMA PARA EL LOCALIZADOR, NO SE PUEDE(N) EDITAR" + strDelimitadorLog;
                        }
                        else
                            this.Modification_Booking(BookingNotificationRequest, dtRESERVAS, strAGENCIA, ref strLOG_HOTETEC);
                        break;
                }

                return objBookingNotificationResponse;
            }
            catch (Exception ex)
            {
                //strLOG_HOTETEC += "SE HA PRODUCCIDO UN ERROR (1): " + ex.Message + strDelimitadorLog;
                return objBookingNotificationResponse;
            }
            finally
            {
                strLOG_HOTETEC += "FIN DE LA TRANSACCIÓN" + strDelimitadorLog;
                strLOG_HOTETEC += "**********************************************************************************************";
                EscribeHotetecLog(strLOG_HOTETEC, strColaNombreFichero);
            }
        }

        private void Creation_Booking(BookingNotificationBookingNotificationRequest objBookingNotificationRequest, string strAGENCIA, ref string strLOG_HOTETEC)
        {
            string strRESULTADO = "";
            int intRESERVA_CREADA = -1;
            string strDelimitadorLog = "-SALTO-";
            string strPrimerOcupante = "";
            string strPrefix = "";
            string strGivenName = "";
            string strSurName = "";

            try
            {
                //Vamos a recorrer el árbol de reservas bookingRoom
                for (int intRES = 0; intRES < objBookingNotificationRequest.bookingRoom.Length; intRES++)
                {
                    strPrimerOcupante = "";
                    strPrefix = "";
                    strGivenName = "";
                    strSurName = "";

                    if(objBookingNotificationRequest.contactCustomer!=null)
                    {
                        if (objBookingNotificationRequest.contactCustomer[0].personName != null)
                        {
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].namePrefix != null) strPrefix = objBookingNotificationRequest.contactCustomer[0].personName[0].namePrefix;
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].givenName != null) strGivenName = objBookingNotificationRequest.contactCustomer[0].personName[0].givenName;
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].surname != null) strSurName = objBookingNotificationRequest.contactCustomer[0].personName[0].surname;
                        }
                    }

                    strPrimerOcupante = strPrefix.Trim();
                    strPrimerOcupante += strPrimerOcupante == "" ? strGivenName.Trim() : " " + strGivenName.Trim();
                    strPrimerOcupante += strPrimerOcupante == "" ? strSurName.Trim() : " " + strSurName.Trim();

                    //Insertamos la Reserva
                    strRESULTADO = this.objFuncionesReservas.InsertarPreReservaHOTETEC(objBookingNotificationRequest.bookingRoom[intRES],
                                                                                        DateTime.Parse(objBookingNotificationRequest.dateTimeCreate),
                                                                                        objBookingNotificationRequest.bookingReference, strAGENCIA,
                                                                                        0,
                                                                                        true,
                                                                                        null,
                                                                                        strPrimerOcupante,
                                                                                        ref intRESERVA_CREADA);
                    if (strRESULTADO.Trim() == "")
                        strLOG_HOTETEC += "SE HA CREADO LA RESERVA: " + intRESERVA_CREADA + strDelimitadorLog;
                    else
                        strLOG_HOTETEC += strRESULTADO + strDelimitadorLog;
                }
            }
            catch (Exception ex)
            {
                strLOG_HOTETEC += "SE HA PRODUCCIDO UN ERROR (Creation_Booking): " + ex.Message + strDelimitadorLog;
            }
        }
        private void Cancellation_Booking(BookingNotificationBookingNotificationRequest objBookingNotificationRequest, DataTable dtRESERVAS, string strAGENCIA, ref string strLOG_HOTETEC)
        {
            string strRESULTADO = "";
            int intRESERVA_CREADA = -1;
            string strDelimitadorLog = "-SALTO-";
            string strPrimerOcupante = "";
            string strPrefix = "";
            string strGivenName = "";
            string strSurName = "";

            try
            {
                //dtRESERVAS y objBookingNotificationRequest.bookingRoom.Length deberían tener el mismo número de reservas
                if (dtRESERVAS.Rows.Count != objBookingNotificationRequest.bookingRoom.Length)
                {
                    strLOG_HOTETEC += "NO SE INICIA LA CANCELACIÓN: EL NÚMERO DE RESERVAS A CANCELAR NO COINCIDE CON EL ENCONTRADO EN EL SISTEMA" + strDelimitadorLog;
                    return;
                }

                //Vamos a recorrer el árbol de reservas bookingRoom
                for (int intRES = 0; intRES < objBookingNotificationRequest.bookingRoom.Length; intRES++)
                {
                    strPrimerOcupante = "";
                    strPrefix = "";
                    strGivenName = "";
                    strSurName = "";

                    if (objBookingNotificationRequest.contactCustomer != null)
                    {
                        if (objBookingNotificationRequest.contactCustomer[0].personName != null)
                        {
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].namePrefix != null) strPrefix = objBookingNotificationRequest.contactCustomer[0].personName[0].namePrefix;
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].givenName != null) strGivenName = objBookingNotificationRequest.contactCustomer[0].personName[0].givenName;
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].surname != null) strSurName = objBookingNotificationRequest.contactCustomer[0].personName[0].surname;
                        }
                    }

                    strPrimerOcupante = strPrefix.Trim();
                    strPrimerOcupante += strPrimerOcupante == "" ? strGivenName.Trim() : " " + strGivenName.Trim();
                    strPrimerOcupante += strPrimerOcupante == "" ? strSurName.Trim() : " " + strSurName.Trim();

                    if (int.Parse(dtRESERVAS.Rows[intRES]["NESTADO_ERERES"].ToString()) != 2 & int.Parse(dtRESERVAS.Rows[intRES]["NESTADO_ERERES"].ToString()) != 6)
                    {
                        strLOG_HOTETEC += "LA RESERVA " + dtRESERVAS.Rows[intRES]["NIDENTIFICADOR_RES"].ToString() + " NO SE CANCELARÁ: SU ESTADO (" + dtRESERVAS.Rows[intRES]["NESTADO_ERERES"].ToString() + ") NO LO PERMITE" + strDelimitadorLog;
                    }
                    else
                    {
                        //Cancelamos la Reserva
                        strRESULTADO = this.objFuncionesReservas.InsertarPreReservaHOTETEC(objBookingNotificationRequest.bookingRoom[intRES],
                                                                                            DateTime.Parse(objBookingNotificationRequest.dateTimeCreate),
                                                                                            objBookingNotificationRequest.bookingReference, strAGENCIA,
                                                                                            2,
                                                                                            false,
                                                                                            dtRESERVAS.Rows[intRES],
                                                                                            strPrimerOcupante,
                                                                                            ref intRESERVA_CREADA);
                        if (strRESULTADO.Trim() == "")
                            strLOG_HOTETEC += "SE HA CANCELADO LA RESERVA " + dtRESERVAS.Rows[intRES]["NIDENTIFICADOR_RES"].ToString() + strDelimitadorLog;
                        else
                            strLOG_HOTETEC += strRESULTADO + strDelimitadorLog;
                    }
                }
            }
            catch (Exception ex)
            {
                strLOG_HOTETEC += "SE HA PRODUCCIDO UN ERROR (Cancellation_Booking): " + ex.Message + strDelimitadorLog;
            }
        }
        private void Modification_Booking(BookingNotificationBookingNotificationRequest objBookingNotificationRequest, DataTable dtRESERVAS, string strAGENCIA, ref string strLOG_HOTETEC)
        {
            string strRESULTADO = "";
            int intRESERVA_CREADA = -1;
            string strDelimitadorLog = "-SALTO-";
            string strPrimerOcupante = "";
            string strPrefix = "";
            string strGivenName = "";
            string strSurName = "";

            try
            {
                //dtRESERVAS y objBookingNotificationRequest.bookingRoom.Length deberían tener el mismo número de reservas
                if (dtRESERVAS.Rows.Count != objBookingNotificationRequest.bookingRoom.Length)
                {
                    strLOG_HOTETEC += "NO SE INICIA LA EDICIÓN: EL NÚMERO DE RESERVAS A EDITAR NO COINCIDE CON EL ENCONTRADO EN EL SISTEMA" + strDelimitadorLog;
                    return;
                }

                //Vamos a recorrer el árbol de reservas bookingRoom
                for (int intRES = 0; intRES < objBookingNotificationRequest.bookingRoom.Length; intRES++)
                {
                    strPrimerOcupante = "";
                    strPrefix = "";
                    strGivenName = "";
                    strSurName = "";

                    if (objBookingNotificationRequest.contactCustomer != null)
                    {
                        if (objBookingNotificationRequest.contactCustomer[0].personName != null)
                        {
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].namePrefix != null) strPrefix = objBookingNotificationRequest.contactCustomer[0].personName[0].namePrefix;
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].givenName != null) strGivenName = objBookingNotificationRequest.contactCustomer[0].personName[0].givenName;
                            if (objBookingNotificationRequest.contactCustomer[0].personName[0].surname != null) strSurName = objBookingNotificationRequest.contactCustomer[0].personName[0].surname;
                        }
                    }

                    strPrimerOcupante = strPrefix.Trim();
                    strPrimerOcupante += strPrimerOcupante == "" ? strGivenName.Trim() : " " + strGivenName.Trim();
                    strPrimerOcupante += strPrimerOcupante == "" ? strSurName.Trim() : " " + strSurName.Trim();

                    if (int.Parse(dtRESERVAS.Rows[intRES]["NESTADO_ERERES"].ToString()) != 2 & int.Parse(dtRESERVAS.Rows[intRES]["NESTADO_ERERES"].ToString()) != 6)
                    {
                        strLOG_HOTETEC += "LA RESERVA " + dtRESERVAS.Rows[intRES]["NIDENTIFICADOR_RES"].ToString() + " NO SE EDITARÁ: SU ESTADO (" + dtRESERVAS.Rows[intRES]["NESTADO_ERERES"].ToString() + ") NO LO PERMITE" + strDelimitadorLog;
                    }
                    else
                    {
                        //Modificamos la Reserva
                        strRESULTADO = this.objFuncionesReservas.InsertarPreReservaHOTETEC(objBookingNotificationRequest.bookingRoom[intRES],
                                                                                            DateTime.Parse(objBookingNotificationRequest.dateTimeCreate),
                                                                                            objBookingNotificationRequest.bookingReference, strAGENCIA,
                                                                                            1,
                                                                                            true,
                                                                                            dtRESERVAS.Rows[intRES],
                                                                                            strPrimerOcupante,
                                                                                            ref intRESERVA_CREADA);
                        if (strRESULTADO.Trim() == "")
                            strLOG_HOTETEC += "SE HA MODIFICADO LA RESERVA " + dtRESERVAS.Rows[intRES]["NIDENTIFICADOR_RES"].ToString() + ", NUEVO IDENTIFICADOR: " + intRESERVA_CREADA + strDelimitadorLog;
                        else
                            strLOG_HOTETEC += strRESULTADO + strDelimitadorLog;
                    }
                }
            }
            catch (Exception ex)
            {
                strLOG_HOTETEC += "SE HA PRODUCCIDO UN ERROR (Modification_Booking): " + ex.Message + strDelimitadorLog;
            }
        }
        private void EscribeHotetecLog(string strLOG_HOTETEC, string strColaNombreFichero)
        {
            string[] arrLOG_HOTETEC;
            string[] arrDelimitardores = { "-SALTO-" };
            string strNombreFichero = "Log_Integracion_RUTA_HOTETEC_" + strColaNombreFichero + ".txt";

            try
            {
                arrLOG_HOTETEC = strLOG_HOTETEC.Split(arrDelimitardores, StringSplitOptions.RemoveEmptyEntries);

                if (arrLOG_HOTETEC.GetLength(0) > 0)
                {

                    string fileName = HttpContext.Current.Request.MapPath(@"HotetecWSLog\" + strNombreFichero);

                    using (StreamWriter sw = File.AppendText(fileName))
                    {
                        for (int intR = 0; intR < arrLOG_HOTETEC.GetLength(0); intR++)
                        {
                            sw.WriteLine(arrLOG_HOTETEC[intR]);
                        }
                    }
                }
            }
            catch
            {

            }
        }
    }
}
