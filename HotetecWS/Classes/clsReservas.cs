﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotetecWS
{
    public class clsReservas : Object
    {
        #region Miembros Privados

        private int intID;
        private int intHotel;
        private DateTime dteFechaVenta;
        private string strUsuario;
        private string strUsuario_LOG;
        private DateTime dteHora;
        private int intTipoEstancia;
        private int intAgencia;
        private int intContrato;
        private DateTime dteEntrada;
        private DateTime dteSalida;
        private int intNoches;
        private bool blnDayUse;
        private int intReserva_Grupo;
        private bool blnMaster;
        private bool blnGrupo;
        private string strReferenciaFactura;
        private bool blnFullCredit;
        private int intTipoHabitacionUso;
        private int intTipoHabitacionFra;
        private int intRegimenUso;
        private int intRegimenFra;
        private string strHabitacion;
        private DateTime? dteHoraEntrada;
        private DateTime? dteHoraSalida;
        private string strVueloLlegada;
        private string strVueloSalida;
        private bool blnCredito;
        private string strNombrePrimerOcupante;
        private int intAdultos;
        private int intNins;
        private int intAdultos_Pre;
        private int intNins_Pre;
        private int intCunas;
        private string strBono;
        private int intTratoCliente;
        private bool blnDesayunoLlegada;
        private bool blnAlmuerzoLlegada;
        private bool blnCenaLlegada;
        private int intEstado;
        private int intEstadoFra;
        private int? intSegmento;
        private bool blnDesayunoSalida;
        private bool blnAlmuerzoSalida;
        private bool blnCenaSalida;
        private DateTime? dteDiaEntrada;
        private DateTime? dteDiaSalida;
        private int? intTurnoDesayuno;
        private int? intTurnoAlmuerzo;
        private int? intTurnoCena;
        private int? intMercado;
        private string strMatriculaCoche;
        private bool blnNoShow;
        private string strObservaciones;
        private bool blnDesglosada;
        private int intComplejo_Origen;
        private int intComplejo;
        private int? intTipoTarjetaCredito;
        private string strTitularTarjetaCredito;
        private string strNumeroTarjetaCredito;
        private string strMesCaducidadTarjetaCredito;
        private string strAnyoCaducidadTarjetaCredito;
        private string strCSVTarjetaCredito;
        private string strObservaciones_Llegada;
        private string strObservaciones_Salida;
        private int intCamasExtras;
        //private bool blnBloqueada;
        private int[] intEdadNins;
        private bool blnEdad_Nins;
        private bool blnEcotasa;
        private int intAdultos_Tasa;
        private int intNins_Tasa;
        private double dblTasa_Adultos;
        private double dblTasa_Nins;
        private double dblTotalTasa;

        #endregion

        public clsReservas()
        {
            intID = -1;
            intHotel = -1;
            dteFechaVenta = DateTime.Today;
            strUsuario = string.Empty;
            strUsuario_LOG = string.Empty;
            dteHora = DateTime.Now;
            intTipoEstancia = -1;
            intAgencia = -1;
            intContrato = -1;
            dteEntrada = DateTime.Today;
            dteSalida = DateTime.Today;
            intNoches = 0;
            blnDayUse = false;
            intReserva_Grupo = -1;
            blnMaster = false;
            blnGrupo = false;
            strReferenciaFactura = string.Empty;
            blnFullCredit = false;
            intTipoHabitacionUso = -1;
            intTipoHabitacionFra = -1;
            intRegimenUso = -1;
            intRegimenFra = -1;
            strHabitacion = "";
            dteHoraEntrada = DateTime.Now;
            dteHoraSalida = DateTime.Now;
            strVueloLlegada = string.Empty;
            strVueloSalida = string.Empty;
            blnCredito = false;
            strNombrePrimerOcupante = string.Empty;
            intAdultos = 0;
            intNins = 0;
            intCunas = 0;
            strBono = string.Empty;
            intTratoCliente = -1;
            blnDesayunoLlegada = false;
            blnAlmuerzoLlegada = false;
            blnCenaLlegada = false;
            intEstado = -1;
            intEstadoFra = -1;
            intSegmento = -1;
            blnDesayunoSalida = false;
            blnAlmuerzoSalida = false;
            blnCenaSalida = false;
            dteDiaEntrada = DateTime.Today;
            dteDiaSalida = DateTime.Today;
            intTurnoDesayuno = -1;
            intTurnoAlmuerzo = -1;
            intTurnoCena = -1;
            intMercado = -1;
            strMatriculaCoche = string.Empty;
            blnNoShow = false;
            strObservaciones = string.Empty;
            intComplejo = 1;
            intComplejo_Origen = 1;
            intTipoTarjetaCredito = -1;
            strTitularTarjetaCredito = string.Empty;
            strNumeroTarjetaCredito = string.Empty;
            strMesCaducidadTarjetaCredito = string.Empty;
            strAnyoCaducidadTarjetaCredito = string.Empty;
            strCSVTarjetaCredito = string.Empty;
            strObservaciones_Llegada = string.Empty;
            strObservaciones_Salida = string.Empty;
            intCamasExtras = 0;
            //blnBloqueada = false;
            blnEdad_Nins = false;
            blnEcotasa = false;
            intAdultos_Pre = 0;
            intNins_Pre = 0;
            intAdultos_Tasa = 0;
            intNins_Tasa = 0;
            dblTasa_Adultos = 0;
            dblTasa_Nins = 0;
            dblTotalTasa = 0;
        }

        #region Propiedades accesoras

        public int ID
        {
            get { return intID; }
            set { intID = value; }
        }

        public int Hotel
        {
            get { return intHotel; }
            set { intHotel = value; }
        }

        public DateTime FechaVenta
        {
            get { return dteFechaVenta; }
            set { dteFechaVenta = value; }
        }

        public string Usuario
        {
            get { return strUsuario; }
            set { strUsuario = value; }
        }

        public string Usuario_LOG
        {
            get { return strUsuario_LOG; }
            set { strUsuario_LOG = value; }
        }

        public DateTime Hora
        {
            get { return dteHora; }
            set { dteHora = value; }
        }

        public int TipoEstancia
        {
            get { return intTipoEstancia; }
            set { intTipoEstancia = value; }
        }

        public int Agencia
        {
            get { return intAgencia; }
            set { intAgencia = value; }
        }

        public int Contrato
        {
            get { return intContrato; }
            set { intContrato = value; }
        }

        public DateTime Entrada
        {
            get { return dteEntrada; }
            set { dteEntrada = value; }
        }

        public DateTime Salida
        {
            get { return dteSalida; }
            set { dteSalida = value; }
        }

        public int Noches
        {
            get { return intNoches; }
            set { intNoches = value; }
        }

        public bool DayUse
        {
            get { return blnDayUse; }
            set { blnDayUse = value; }
        }

        public int Reserva_Grupo
        {
            get { return intReserva_Grupo; }
            set { intReserva_Grupo = value; }
        }

        public bool Master
        {
            get { return blnMaster; }
            set { blnMaster = value; }
        }

        public bool Grupo
        {
            get { return blnGrupo; }
            set { blnGrupo = value; }
        }

        public string ReferenciaFactura
        {
            get { return strReferenciaFactura; }
            set { strReferenciaFactura = value; }
        }

        public bool FullCredit
        {
            get { return blnFullCredit; }
            set { blnFullCredit = value; }
        }

        public int TipoHabitacionUso
        {
            get { return intTipoHabitacionUso; }
            set { intTipoHabitacionUso = value; }
        }

        public int TipoHabitacionFra
        {
            get { return intTipoHabitacionFra; }
            set { intTipoHabitacionFra = value; }
        }

        public int RegimenUso
        {
            get { return intRegimenUso; }
            set { intRegimenUso = value; }
        }

        public int RegimenFra
        {
            get { return intRegimenFra; }
            set { intRegimenFra = value; }
        }

        public string Habitacion
        {
            get { return strHabitacion; }
            set { strHabitacion = value; }
        }

        public DateTime? HoraEntrada
        {
            get { return dteHoraEntrada; }
            set { dteHoraEntrada = value; }
        }

        public DateTime? HoraSalida
        {
            get { return dteHoraSalida; }
            set { dteHoraSalida = value; }
        }

        public string VueloLlegada
        {
            get { return strVueloLlegada; }
            set { strVueloLlegada = value; }
        }

        public string VueloSalida
        {
            get { return strVueloSalida; }
            set { strVueloSalida = value; }
        }

        public bool Credito
        {
            get { return blnCredito; }
            set { blnCredito = value; }
        }

        public string NombrePrimerOcupante
        {
            get { return strNombrePrimerOcupante; }
            set { strNombrePrimerOcupante = value; }
        }

        public int Adultos
        {
            get { return intAdultos; }
            set { intAdultos = value; }
        }

        public int Nins
        {
            get { return intNins; }
            set { intNins = value; }
        }
        public int Adul_Pre
        {
            get { return intAdultos_Pre; }
            set { intAdultos_Pre = value; }
        }

        public int Nins_Pre
        {
            get { return intNins_Pre; }
            set { intNins_Pre = value; }
        }
        public int Cunas
        {
            get { return intCunas; }
            set { intCunas = value; }
        }

        public string Bono
        {
            get { return strBono; }
            set { strBono = value; }
        }

        public int TratoCliente
        {
            get { return intTratoCliente; }
            set { intTratoCliente = value; }
        }

        public bool DesayunoLlegada
        {
            get { return blnDesayunoLlegada; }
            set { blnDesayunoLlegada = value; }
        }

        public bool AlmuerzoLlegada
        {
            get { return blnAlmuerzoLlegada; }
            set { blnAlmuerzoLlegada = value; }
        }

        public bool CenaLlegada
        {
            get { return blnCenaLlegada; }
            set { blnCenaLlegada = value; }
        }

        public int Estado
        {
            get { return intEstado; }
            set { intEstado = value; }
        }

        public int EstadoFra
        {
            get { return intEstadoFra; }
            set { intEstadoFra = value; }
        }

        public int? Segmento
        {
            get { return intSegmento; }
            set { intSegmento = value; }
        }

        public bool DesayunoSalida
        {
            get { return blnDesayunoSalida; }
            set { blnDesayunoSalida = value; }
        }

        public bool AlmuerzoSalida
        {
            get { return blnAlmuerzoSalida; }
            set { blnAlmuerzoSalida = value; }
        }

        public bool CenaSalida
        {
            get { return blnCenaSalida; }
            set { blnCenaSalida = value; }
        }

        public DateTime? DiaEntrada
        {
            get { return dteDiaEntrada; }
            set { dteDiaEntrada = value; }
        }

        public DateTime? DiaSalida
        {
            get { return dteDiaSalida; }
            set { dteDiaSalida = value; }
        }

        public int? TurnoDesayuno
        {
            get { return intTurnoDesayuno; }
            set { intTurnoDesayuno = value; }
        }

        public int? TurnoAlmuerzo
        {
            get { return intTurnoAlmuerzo; }
            set { intTurnoAlmuerzo = value; }
        }

        public int? TurnoCena
        {
            get { return intTurnoCena; }
            set { intTurnoCena = value; }
        }

        public int? Mercado
        {
            get { return intMercado; }
            set { intMercado = value; }
        }

        public string MatriculaCoche
        {
            get { return strMatriculaCoche; }
            set { strMatriculaCoche = value; }
        }

        public bool NoShow
        {
            get { return blnNoShow; }
            set { blnNoShow = value; }
        }

        public string Observaciones
        {
            get { return strObservaciones; }
            set { strObservaciones = value; }
        }

        public bool Desglosada
        {
            get { return blnDesglosada; }
            set { blnDesglosada = value; }
        }

        public int Complejo
        {
            get { return intComplejo; }
            set { intComplejo = value; }
        }

        public int Complejo_Origen
        {
            get { return intComplejo_Origen; }
            set { intComplejo_Origen = value; }
        }

        public int? TipoTarjetaCredito
        {
            get { return intTipoTarjetaCredito; }
            set { intTipoTarjetaCredito = value; }
        }

        public string TitularTarjetaCredito
        {
            get { return strTitularTarjetaCredito; }
            set { strTitularTarjetaCredito = value; }
        }

        public string NumeroTarjetaCredito
        {
            get { return strNumeroTarjetaCredito; }
            set { strNumeroTarjetaCredito = value; }
        }

        public string MesCaducidadTarjetaCredito
        {
            get { return strMesCaducidadTarjetaCredito; }
            set { strMesCaducidadTarjetaCredito = value; }
        }

        public string AnyoCaducidadTarjetaCredito
        {
            get { return strAnyoCaducidadTarjetaCredito; }
            set { strAnyoCaducidadTarjetaCredito = value; }
        }

        public string CSVTarjetaCredito
        {
            get { return strCSVTarjetaCredito; }
            set { strCSVTarjetaCredito = value; }
        }

        public string Observaciones_Llegada
        {
            get { return strObservaciones_Llegada; }
            set { strObservaciones_Llegada = value; }
        }

        public string Observaciones_Salida
        {
            get { return strObservaciones_Salida; }
            set { strObservaciones_Salida = value; }
        }

        public int CamasExtra
        {
            get { return intCamasExtras; }
            set { intCamasExtras = value; }
        }

        //public bool Bloqueada
        //{
        //    get { return blnBloqueada; }
        //    set { blnBloqueada = value; }
        //}

        public int[] EdadNins
        {
            get { return intEdadNins; }
            set { intEdadNins = value; }
        }

        public bool EdadNinsRequerida
        {
            get { return blnEdad_Nins; }
            set { blnEdad_Nins = value; }
        }

        public bool Ecotasa
        {
            get { return blnEcotasa; }
            set { blnEcotasa = value; }
        }

        public int Adultos_Tasa
        {
            get { return intAdultos_Tasa; }
            set { intAdultos_Tasa = value; }
        }

        public int Nins_Tasa
        {
            get { return intNins_Tasa; }
            set { intNins_Tasa = value; }
        }

        public double Tasa_Adultos
        {
            get { return dblTasa_Adultos; }
            set { dblTasa_Adultos = value; }
        }

        public double Tasa_Nins
        {
            get { return dblTasa_Nins; }
            set { dblTasa_Nins = value; }
        }

        public double TotalTasa
        {
            get { return dblTotalTasa; }
            set { dblTotalTasa = value; }
        }

        #endregion

        public void InicializarReserva()
        {
            this.ID = -1;
            this.Hotel = -1;
            this.FechaVenta = DateTime.Today;
            this.Usuario = "";
            this.Usuario_LOG = "";
            this.Hora = DateTime.Now;
            this.TipoEstancia = -1;
            this.Agencia = -1;
            this.Contrato = -1;
            this.Entrada = DateTime.Today;
            this.Salida = DateTime.Today;
            this.Noches = 0;
            this.DayUse = false;
            this.Reserva_Grupo = -1;
            this.Master = false;
            this.Grupo = false;
            this.ReferenciaFactura = "";
            this.FullCredit = false;
            this.TipoHabitacionUso = -1;
            this.TipoHabitacionFra = -1;
            this.RegimenUso = -1;
            this.RegimenFra = -1;
            this.Habitacion = "";
            this.HoraEntrada = null;
            this.HoraSalida = null;
            this.VueloLlegada = "";
            this.VueloSalida = "";
            this.Credito = false;
            this.NombrePrimerOcupante = "";
            this.Adultos = 0;
            this.Nins = 0;
            this.Cunas = 0;
            this.Bono = "";
            this.TratoCliente = -1;
            this.DesayunoLlegada = false;
            this.AlmuerzoLlegada = false;
            this.CenaLlegada = false;
            this.Estado = -1;
            this.EstadoFra = -1;
            this.Segmento = null;
            this.DesayunoSalida = false;
            this.AlmuerzoSalida = false;
            this.CenaSalida = false;
            this.DiaEntrada = null;
            this.DiaSalida = null;
            this.TurnoDesayuno = null;
            this.TurnoAlmuerzo = null;
            this.TurnoCena = null;
            this.Mercado = null;
            this.MatriculaCoche = "";
            this.NoShow = false;
            this.Observaciones = "";
            this.Desglosada = false;
            this.Complejo_Origen = 1;
            this.Complejo = 1;
            this.intTipoTarjetaCredito = -1;
            this.strTitularTarjetaCredito = "";
            this.strNumeroTarjetaCredito = "";
            this.strMesCaducidadTarjetaCredito = "";
            this.strAnyoCaducidadTarjetaCredito = "";
            this.strCSVTarjetaCredito = "";
            this.strObservaciones_Llegada = "";
            this.strObservaciones_Salida = "";
            this.intCamasExtras = 0;
            //this.blnBloqueada = false;
            this.EdadNinsRequerida = false;
            this.Ecotasa = false;
            this.Adultos_Tasa = 0;
            this.Nins_Tasa = 0;
            this.Tasa_Adultos = 0;
            this.Tasa_Nins = 0;
            this.TotalTasa = 0;
        }

        public bool ComparaSinFechas(clsReservas objReservasComparar)
        {
            if (!this.ID.Equals(objReservasComparar.ID)) return false;
            if (!this.Hotel.Equals(objReservasComparar.Hotel)) return false;
            if (!this.FechaVenta.Equals(objReservasComparar.FechaVenta)) return false;
            if (!this.Usuario.Equals(objReservasComparar.Usuario)) return false;
            if (!this.Usuario_LOG.Equals(objReservasComparar.Usuario_LOG)) return false;
            if (!this.Hora.Equals(objReservasComparar.Hora)) return false;
            if (!this.TipoEstancia.Equals(objReservasComparar.TipoEstancia)) return false;
            if (!this.Agencia.Equals(objReservasComparar.Agencia)) return false;
            if (!this.Contrato.Equals(objReservasComparar.Contrato)) return false;
            //if (!this.Entrada.Equals(objReservasComparar.Entrada)) return false;
            //if (!this.Salida.Equals(objReservasComparar.Salida)) return false;
            //if (!this.Noches.Equals(objReservasComparar.Noches)) return false;
            if (!this.DayUse.Equals(objReservasComparar.DayUse)) return false;
            if (!this.Reserva_Grupo.Equals(objReservasComparar.Reserva_Grupo)) return false;
            if (!this.Master.Equals(objReservasComparar.Master)) return false;
            if (!this.Grupo.Equals(objReservasComparar.Grupo)) return false;
            if (!this.ReferenciaFactura.Equals(objReservasComparar.ReferenciaFactura)) return false;
            if (!this.FullCredit.Equals(objReservasComparar.FullCredit)) return false;
            if (!this.TipoHabitacionUso.Equals(objReservasComparar.TipoHabitacionUso)) return false;
            if (!this.TipoHabitacionFra.Equals(objReservasComparar.TipoHabitacionFra)) return false;
            if (!this.RegimenUso.Equals(objReservasComparar.RegimenUso)) return false;
            if (!this.RegimenFra.Equals(objReservasComparar.RegimenFra)) return false;
            if (!this.Habitacion.Equals(objReservasComparar.Habitacion)) return false;
            if (!this.HoraEntrada.Equals(objReservasComparar.HoraEntrada)) return false;
            if (!this.HoraSalida.Equals(objReservasComparar.HoraSalida)) return false;
            if (!this.VueloLlegada.Equals(objReservasComparar.VueloLlegada)) return false;
            if (!this.VueloSalida.Equals(objReservasComparar.VueloSalida)) return false;
            if (!this.Credito.Equals(objReservasComparar.Credito)) return false;
            if (!this.NombrePrimerOcupante.Equals(objReservasComparar.NombrePrimerOcupante)) return false;
            if (!this.Adultos.Equals(objReservasComparar.Adultos)) return false;
            if (!this.Nins.Equals(objReservasComparar.Nins)) return false;
            if (!this.Cunas.Equals(objReservasComparar.Cunas)) return false;
            if (!this.Bono.Equals(objReservasComparar.Bono)) return false;
            if (!this.TratoCliente.Equals(objReservasComparar.TratoCliente)) return false;
            if (!this.DesayunoLlegada.Equals(objReservasComparar.DesayunoLlegada)) return false;
            if (!this.AlmuerzoLlegada.Equals(objReservasComparar.AlmuerzoLlegada)) return false;
            if (!this.CenaLlegada.Equals(objReservasComparar.CenaLlegada)) return false;
            if (!this.Estado.Equals(objReservasComparar.Estado)) return false;
            if (!this.EstadoFra.Equals(objReservasComparar.EstadoFra)) return false;
            if (!this.Segmento.Equals(objReservasComparar.Segmento)) return false;
            if (!this.DesayunoSalida.Equals(objReservasComparar.DesayunoSalida)) return false;
            if (!this.AlmuerzoSalida.Equals(objReservasComparar.AlmuerzoSalida)) return false;
            if (!this.CenaSalida.Equals(objReservasComparar.CenaSalida)) return false;
            //if (!this.DiaEntrada.Equals(objReservasComparar.DiaEntrada)) return false;
            //if (!this.DiaSalida.Equals(objReservasComparar.DiaSalida)) return false;
            if (!this.TurnoDesayuno.Equals(objReservasComparar.TurnoDesayuno)) return false;
            if (!this.TurnoAlmuerzo.Equals(objReservasComparar.TurnoAlmuerzo)) return false;
            if (!this.TurnoCena.Equals(objReservasComparar.TurnoCena)) return false;
            if (!this.Mercado.Equals(objReservasComparar.Mercado)) return false;
            if (!this.MatriculaCoche.Equals(objReservasComparar.MatriculaCoche)) return false;
            if (!this.NoShow.Equals(objReservasComparar.NoShow)) return false;
            if (!this.Observaciones.Equals(objReservasComparar.Observaciones)) return false;

            if (!this.Desglosada.Equals(objReservasComparar.Desglosada)) return false;
            if (!this.Complejo_Origen.Equals(objReservasComparar.Complejo_Origen)) return false;
            if (!this.Complejo.Equals(objReservasComparar.Complejo)) return false;
            if (!this.intTipoTarjetaCredito.Equals(objReservasComparar.intTipoTarjetaCredito)) return false;
            if (!this.strTitularTarjetaCredito.Equals(objReservasComparar.strTitularTarjetaCredito)) return false;
            if (!this.strNumeroTarjetaCredito.Equals(objReservasComparar.strNumeroTarjetaCredito)) return false;
            if (!this.strMesCaducidadTarjetaCredito.Equals(objReservasComparar.strMesCaducidadTarjetaCredito)) return false;
            if (!this.strAnyoCaducidadTarjetaCredito.Equals(objReservasComparar.strAnyoCaducidadTarjetaCredito)) return false;
            if (!this.strCSVTarjetaCredito.Equals(objReservasComparar.strCSVTarjetaCredito)) return false;
            if (!this.strObservaciones_Llegada.Equals(objReservasComparar.strObservaciones_Llegada)) return false;
            if (!this.strObservaciones_Salida.Equals(objReservasComparar.strObservaciones_Salida)) return false;
            if (!this.intCamasExtras.Equals(objReservasComparar.intCamasExtras)) return false;
            if (!this.EdadNinsRequerida.Equals(objReservasComparar.EdadNinsRequerida)) return false;
            if (!this.Ecotasa.Equals(objReservasComparar.Ecotasa)) return false;
            if (!this.Adultos_Tasa.Equals(objReservasComparar.Adultos_Tasa)) return false;
            if (!this.Nins_Tasa.Equals(objReservasComparar.Nins_Tasa)) return false;
            if (!this.Tasa_Adultos.Equals(objReservasComparar.Tasa_Adultos)) return false;
            if (!this.Tasa_Nins.Equals(objReservasComparar.Tasa_Nins)) return false;
            if (!this.TotalTasa.Equals(objReservasComparar.TotalTasa)) return false;

            return true;
        }

        public int ComparaSinFechasParaNoEliminarRED(clsReservas objReservasComparar, ref bool blnActualizarRED)
        {
            if (!this.DiaEntrada.Equals(objReservasComparar.Habitacion)) blnActualizarRED = false;
            if (!this.DiaSalida.Equals(objReservasComparar.NombrePrimerOcupante)) blnActualizarRED = false;
            if (!this.Observaciones.Equals(objReservasComparar.Observaciones)) blnActualizarRED = false;

            if (!this.ID.Equals(objReservasComparar.ID)) return 1;
            if (!this.Hotel.Equals(objReservasComparar.Hotel)) return 1;
            if (!this.FechaVenta.Equals(objReservasComparar.FechaVenta)) return 1;
            if (!this.Usuario.Equals(objReservasComparar.Usuario)) return 1;
            if (!this.Usuario_LOG.Equals(objReservasComparar.Usuario_LOG)) return 1;
            if (!this.Hora.Equals(objReservasComparar.Hora)) return 1;
            if (!this.TipoEstancia.Equals(objReservasComparar.TipoEstancia)) return 1;
            if (!this.Agencia.Equals(objReservasComparar.Agencia)) return 1;
            if (!this.Contrato.Equals(objReservasComparar.Contrato)) return 1;

            if (!this.DayUse.Equals(objReservasComparar.DayUse)) return 1;
            if (!this.Reserva_Grupo.Equals(objReservasComparar.Reserva_Grupo)) return 1;
            if (!this.Master.Equals(objReservasComparar.Master)) return 1;
            if (!this.Grupo.Equals(objReservasComparar.Grupo)) return 1;
            if (!this.ReferenciaFactura.Equals(objReservasComparar.ReferenciaFactura)) return 1;
            if (!this.FullCredit.Equals(objReservasComparar.FullCredit)) return 1;
            if (!this.TipoHabitacionUso.Equals(objReservasComparar.TipoHabitacionUso)) return 1;
            if (!this.TipoHabitacionFra.Equals(objReservasComparar.TipoHabitacionFra)) return 1;
            if (!this.RegimenUso.Equals(objReservasComparar.RegimenUso)) return 1;
            if (!this.RegimenFra.Equals(objReservasComparar.RegimenFra)) return 1;

            if (!this.HoraEntrada.Equals(objReservasComparar.HoraEntrada)) return 1;
            if (!this.HoraSalida.Equals(objReservasComparar.HoraSalida)) return 1;
            if (!this.VueloLlegada.Equals(objReservasComparar.VueloLlegada)) return 1;
            if (!this.VueloSalida.Equals(objReservasComparar.VueloSalida)) return 1;
            if (!this.Credito.Equals(objReservasComparar.Credito)) return 1;

            if (!this.Adultos.Equals(objReservasComparar.Adultos)) return 1;
            if (!this.Nins.Equals(objReservasComparar.Nins)) return 1;
            if (!this.Cunas.Equals(objReservasComparar.Cunas)) return 1;
            if (!this.Bono.Equals(objReservasComparar.Bono)) return 1;
            if (!this.TratoCliente.Equals(objReservasComparar.TratoCliente)) return 1;
            if (!this.DesayunoLlegada.Equals(objReservasComparar.DesayunoLlegada)) return 1;
            if (!this.AlmuerzoLlegada.Equals(objReservasComparar.AlmuerzoLlegada)) return 1;
            if (!this.CenaLlegada.Equals(objReservasComparar.CenaLlegada)) return 1;
            if (!this.Estado.Equals(objReservasComparar.Estado)) return 1;
            if (!this.EstadoFra.Equals(objReservasComparar.EstadoFra)) return 1;
            if (!this.Segmento.Equals(objReservasComparar.Segmento)) return 1;
            if (!this.DesayunoSalida.Equals(objReservasComparar.DesayunoSalida)) return 1;
            if (!this.AlmuerzoSalida.Equals(objReservasComparar.AlmuerzoSalida)) return 1;
            if (!this.CenaSalida.Equals(objReservasComparar.CenaSalida)) return 1;

            if (!this.TurnoDesayuno.Equals(objReservasComparar.TurnoDesayuno)) return 1;
            if (!this.TurnoAlmuerzo.Equals(objReservasComparar.TurnoAlmuerzo)) return 1;
            if (!this.TurnoCena.Equals(objReservasComparar.TurnoCena)) return 1;
            if (!this.Mercado.Equals(objReservasComparar.Mercado)) return 1;
            if (!this.MatriculaCoche.Equals(objReservasComparar.MatriculaCoche)) return 1;
            if (!this.NoShow.Equals(objReservasComparar.NoShow)) return 1;

            if (!this.Desglosada.Equals(objReservasComparar.Desglosada)) return 1;
            if (!this.Complejo_Origen.Equals(objReservasComparar.Complejo_Origen)) return 1;
            if (!this.Complejo.Equals(objReservasComparar.Complejo)) return 1;
            if (!this.intTipoTarjetaCredito.Equals(objReservasComparar.intTipoTarjetaCredito)) return 1;
            if (!this.strTitularTarjetaCredito.Equals(objReservasComparar.strTitularTarjetaCredito)) return 1;
            if (!this.strNumeroTarjetaCredito.Equals(objReservasComparar.strNumeroTarjetaCredito)) return 1;
            if (!this.strMesCaducidadTarjetaCredito.Equals(objReservasComparar.strMesCaducidadTarjetaCredito)) return 1;
            if (!this.strAnyoCaducidadTarjetaCredito.Equals(objReservasComparar.strAnyoCaducidadTarjetaCredito)) return 1;
            if (!this.strCSVTarjetaCredito.Equals(objReservasComparar.strCSVTarjetaCredito)) return 1;
            if (!this.strObservaciones_Llegada.Equals(objReservasComparar.strObservaciones_Llegada)) return 1;
            if (!this.strObservaciones_Salida.Equals(objReservasComparar.strObservaciones_Salida)) return 1;
            if (!this.intCamasExtras.Equals(objReservasComparar.intCamasExtras)) return 1;
            if (!this.EdadNinsRequerida.Equals(objReservasComparar.EdadNinsRequerida)) return 1;
            if (!this.Ecotasa.Equals(objReservasComparar.Ecotasa)) return 1;
            if (!this.Adultos_Tasa.Equals(objReservasComparar.Adultos_Tasa)) return 1;
            if (!this.Nins_Tasa.Equals(objReservasComparar.Nins_Tasa)) return 1;
            if (!this.Tasa_Adultos.Equals(objReservasComparar.Tasa_Adultos)) return 1;
            if (!this.Tasa_Nins.Equals(objReservasComparar.Tasa_Nins)) return 1;
            if (!this.TotalTasa.Equals(objReservasComparar.TotalTasa)) return 1;



            return 0;
        }
    }
}
