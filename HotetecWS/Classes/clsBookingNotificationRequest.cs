﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// Este código fuente fue generado automáticamente por xsd, Versión=4.0.30319.33440.
// 


/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = false)]
public partial class BookingNotification
{

    private BookingNotificationBookingNotificationRequest[] bookingNotificationRequestField;

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("BookingNotificationRequest")]
    public BookingNotificationBookingNotificationRequest[] BookingNotificationRequest
    {
        get
        {
            return this.bookingNotificationRequestField;
        }
        set
        {
            this.bookingNotificationRequestField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequest
{

    private string idesesField;

    private string supplierField;

    private string typeTransactionField;

    private string bookingReferenceField;

    private string bookingClientReferenceField;

    private string dateTimeCreateField;

    private string dateTimeEventField;

    private string stockistField;

    private string systemField;

    private string creationUserField;

    private string totalAmountField;

    private BookingNotificationBookingNotificationRequestBookingRoom[] bookingRoomField;

    private BookingNotificationBookingNotificationRequestContactCustomer[] contactCustomerField;

    private BookingNotificationBookingNotificationRequestDetailSourceDataValuationRoom[] detailSourceDataField;

    private BookingNotificationBookingNotificationRequestCommissionServiceCommission[] commissionField;

    private BookingNotificationBookingNotificationRequestPaymentPlanPayment[] paymentPlanField;

    private BookingNotificationBookingNotificationRequestSystemInternalInformation[] systemInternalInformationField;

    /// <comentarios/>
    public string ideses
    {
        get
        {
            return this.idesesField;
        }
        set
        {
            this.idesesField = value;
        }
    }

    /// <comentarios/>
    public string supplier
    {
        get
        {
            return this.supplierField;
        }
        set
        {
            this.supplierField = value;
        }
    }

    /// <comentarios/>
    public string typeTransaction
    {
        get
        {
            return this.typeTransactionField;
        }
        set
        {
            this.typeTransactionField = value;
        }
    }

    /// <comentarios/>
    public string bookingReference
    {
        get
        {
            return this.bookingReferenceField;
        }
        set
        {
            this.bookingReferenceField = value;
        }
    }

    /// <comentarios/>
    public string bookingClientReference
    {
        get
        {
            return this.bookingClientReferenceField;
        }
        set
        {
            this.bookingClientReferenceField = value;
        }
    }

    /// <comentarios/>
    public string dateTimeCreate
    {
        get
        {
            return this.dateTimeCreateField;
        }
        set
        {
            this.dateTimeCreateField = value;
        }
    }

    /// <comentarios/>
    public string dateTimeEvent
    {
        get
        {
            return this.dateTimeEventField;
        }
        set
        {
            this.dateTimeEventField = value;
        }
    }

    /// <comentarios/>
    public string stockist
    {
        get
        {
            return this.stockistField;
        }
        set
        {
            this.stockistField = value;
        }
    }

    /// <comentarios/>
    public string system
    {
        get
        {
            return this.systemField;
        }
        set
        {
            this.systemField = value;
        }
    }

    /// <comentarios/>
    public string creationUser
    {
        get
        {
            return this.creationUserField;
        }
        set
        {
            this.creationUserField = value;
        }
    }

    /// <comentarios/>
    public string totalAmount
    {
        get
        {
            return this.totalAmountField;
        }
        set
        {
            this.totalAmountField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("bookingRoom")]
    public BookingNotificationBookingNotificationRequestBookingRoom[] bookingRoom
    {
        get
        {
            return this.bookingRoomField;
        }
        set
        {
            this.bookingRoomField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("contactCustomer")]
    public BookingNotificationBookingNotificationRequestContactCustomer[] contactCustomer
    {
        get
        {
            return this.contactCustomerField;
        }
        set
        {
            this.contactCustomerField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlArrayItemAttribute("valuationRoom", typeof(BookingNotificationBookingNotificationRequestDetailSourceDataValuationRoom), IsNullable = false)]
    public BookingNotificationBookingNotificationRequestDetailSourceDataValuationRoom[] detailSourceData
    {
        get
        {
            return this.detailSourceDataField;
        }
        set
        {
            this.detailSourceDataField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlArrayItemAttribute("serviceCommission", typeof(BookingNotificationBookingNotificationRequestCommissionServiceCommission), IsNullable = false)]
    public BookingNotificationBookingNotificationRequestCommissionServiceCommission[] commission
    {
        get
        {
            return this.commissionField;
        }
        set
        {
            this.commissionField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlArrayItemAttribute("payment", typeof(BookingNotificationBookingNotificationRequestPaymentPlanPayment), IsNullable = false)]
    public BookingNotificationBookingNotificationRequestPaymentPlanPayment[] paymentPlan
    {
        get
        {
            return this.paymentPlanField;
        }
        set
        {
            this.paymentPlanField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("systemInternalInformation")]
    public BookingNotificationBookingNotificationRequestSystemInternalInformation[] systemInternalInformation
    {
        get
        {
            return this.systemInternalInformationField;
        }
        set
        {
            this.systemInternalInformationField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestBookingRoom
{

    private string checkInDateField;

    private string checkOutDateField;

    private string serviceCodeField;

    private string referenceRoomIdField;

    private string roomTypeField;

    private string roomTypeExtraField;

    private string boardField;

    private string fareNameField;

    private string roomQuantityField;

    private string amountField;

    private string currencyCodeField;

    private string bookingStatusField;

    private BookingNotificationBookingNotificationRequestBookingRoomGuest[] guestField;

    private BookingNotificationRequestBookingRoomRemark[] remarkField;
    private BookingNotificationRequestBookingRoomBookingSupplement[] bookingSupplementField;

    private BookingNotificationBookingNotificationRequestBookingRoomCancellationDataCancelPenaltyPolicy[] cancellationDataField;

    private BookingNotificationBookingNotificationRequestBookingRoomRoomRateRoomRateDay[] roomRateField;

    private string idField;

    /// <comentarios/>
    public string checkInDate
    {
        get
        {
            return this.checkInDateField;
        }
        set
        {
            this.checkInDateField = value;
        }
    }

    /// <comentarios/>
    public string checkOutDate
    {
        get
        {
            return this.checkOutDateField;
        }
        set
        {
            this.checkOutDateField = value;
        }
    }

    /// <comentarios/>
    public string serviceCode
    {
        get
        {
            return this.serviceCodeField;
        }
        set
        {
            this.serviceCodeField = value;
        }
    }

    /// <comentarios/>
    public string referenceRoomId
    {
        get
        {
            return this.referenceRoomIdField;
        }
        set
        {
            this.referenceRoomIdField = value;
        }
    }

    /// <comentarios/>
    public string roomType
    {
        get
        {
            return this.roomTypeField;
        }
        set
        {
            this.roomTypeField = value;
        }
    }

    /// <comentarios/>
    public string roomTypeExtra
    {
        get
        {
            return this.roomTypeExtraField;
        }
        set
        {
            this.roomTypeExtraField = value;
        }
    }

    /// <comentarios/>
    public string board
    {
        get
        {
            return this.boardField;
        }
        set
        {
            this.boardField = value;
        }
    }

    /// <comentarios/>
    public string fareName
    {
        get
        {
            return this.fareNameField;
        }
        set
        {
            this.fareNameField = value;
        }
    }

    /// <comentarios/>
    public string roomQuantity
    {
        get
        {
            return this.roomQuantityField;
        }
        set
        {
            this.roomQuantityField = value;
        }
    }

    /// <comentarios/>
    public string amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <comentarios/>
    public string currencyCode
    {
        get
        {
            return this.currencyCodeField;
        }
        set
        {
            this.currencyCodeField = value;
        }
    }

    /// <comentarios/>
    public string bookingStatus
    {
        get
        {
            return this.bookingStatusField;
        }
        set
        {
            this.bookingStatusField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("guest")]
    public BookingNotificationBookingNotificationRequestBookingRoomGuest[] guest
    {
        get
        {
            return this.guestField;
        }
        set
        {
            this.guestField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("remark", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public BookingNotificationRequestBookingRoomRemark[] remark
    {
        get
        {
            return this.remarkField;
        }
        set
        {
            this.remarkField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("bookingSupplement", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public BookingNotificationRequestBookingRoomBookingSupplement[] bookingSupplement
    {
        get
        {
            return this.bookingSupplementField;
        }
        set
        {
            this.bookingSupplementField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlArrayItemAttribute("cancelPenaltyPolicy", typeof(BookingNotificationBookingNotificationRequestBookingRoomCancellationDataCancelPenaltyPolicy), IsNullable = false)]
    public BookingNotificationBookingNotificationRequestBookingRoomCancellationDataCancelPenaltyPolicy[] cancellationData
    {
        get
        {
            return this.cancellationDataField;
        }
        set
        {
            this.cancellationDataField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlArrayItemAttribute("roomRateDay", typeof(BookingNotificationBookingNotificationRequestBookingRoomRoomRateRoomRateDay), IsNullable = false)]
    public BookingNotificationBookingNotificationRequestBookingRoomRoomRateRoomRateDay[] roomRate
    {
        get
        {
            return this.roomRateField;
        }
        set
        {
            this.roomRateField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestBookingRoomGuest
{

    private string birthDateField;

    private string amountField;

    private string idField;

    private string groupRoomField;

    private BookingNotificationRequestBookingRoomGuestPersonName personaNameField;

    /// <comentarios/>
    public string birthDate
    {
        get
        {
            return this.birthDateField;
        }
        set
        {
            this.birthDateField = value;
        }
    }

    /// <comentarios/>
    public string amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string groupRoom
    {
        get
        {
            return this.groupRoomField;
        }
        set
        {
            this.groupRoomField = value;
        }
    }
    [System.Xml.Serialization.XmlElementAttribute("personName", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public BookingNotificationRequestBookingRoomGuestPersonName personName
    {
        get
        {
            return this.personaNameField;
        }
        set
        {
            this.personaNameField = value;
        }
    }
}
/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BookingNotificationRequestBookingRoomGuestPersonName
{

    private string namePrefixField;

    private string givenNameField;

    private string surnameField;


    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string namePrefix
    {
        get
        {
            return this.namePrefixField;
        }
        set
        {
            this.namePrefixField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string givenName
    {
        get
        {
            return this.givenNameField;
        }
        set
        {
            this.givenNameField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string surname
    {
        get
        {
            return this.surnameField;
        }
        set
        {
            this.surnameField = value;
        }
    }
}
/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BookingNotificationRequestBookingRoomRemark
{

    private string remarkCodeField;

    private string languageCodeField;

    private string textField;

    private string idField;

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string remarkCode
    {
        get
        {
            return this.remarkCodeField;
        }
        set
        {
            this.remarkCodeField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string languageCode
    {
        get
        {
            return this.languageCodeField;
        }
        set
        {
            this.languageCodeField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string text
    {
        get
        {
            return this.textField;
        }
        set
        {
            this.textField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class BookingNotificationRequestBookingRoomBookingSupplement
{

    private string supplementTypeField;

    private string supplementNameField;

    private string checkInDateField;

    private string checkOutDateField;

    private string supplementStatusField;

    private string amountField;

    private string supplementCodeField;

    private string idField;

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string supplementType
    {
        get
        {
            return this.supplementTypeField;
        }
        set
        {
            this.supplementTypeField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string supplementName
    {
        get
        {
            return this.supplementNameField;
        }
        set
        {
            this.supplementNameField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string checkInDate
    {
        get
        {
            return this.checkInDateField;
        }
        set
        {
            this.checkInDateField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string checkOutDate
    {
        get
        {
            return this.checkOutDateField;
        }
        set
        {
            this.checkOutDateField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string supplementStatus
    {
        get
        {
            return this.supplementStatusField;
        }
        set
        {
            this.supplementStatusField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public string supplementCode
    {
        get
        {
            return this.supplementCodeField;
        }
        set
        {
            this.supplementCodeField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestBookingRoomCancellationDataCancelPenaltyPolicy
{

    private string dateField;

    private string timeRelevantField;

    private string amountField;

    private string idField;

    /// <comentarios/>
    public string date
    {
        get
        {
            return this.dateField;
        }
        set
        {
            this.dateField = value;
        }
    }

    /// <comentarios/>
    public string timeRelevant
    {
        get
        {
            return this.timeRelevantField;
        }
        set
        {
            this.timeRelevantField = value;
        }
    }

    /// <comentarios/>
    public string amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestBookingRoomRoomRateRoomRateDay
{

    private string dayField;

    private string salePlanCodeField;

    private string fareCodeField;

    private string fareNameField;

    private string amountField;

    /// <comentarios/>
    public string day
    {
        get
        {
            return this.dayField;
        }
        set
        {
            this.dayField = value;
        }
    }

    /// <comentarios/>
    public string salePlanCode
    {
        get
        {
            return this.salePlanCodeField;
        }
        set
        {
            this.salePlanCodeField = value;
        }
    }

    /// <comentarios/>
    public string fareCode
    {
        get
        {
            return this.fareCodeField;
        }
        set
        {
            this.fareCodeField = value;
        }
    }

    /// <comentarios/>
    public string fareName
    {
        get
        {
            return this.fareNameField;
        }
        set
        {
            this.fareNameField = value;
        }
    }

    /// <comentarios/>
    public string amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestContactCustomer
{

    private BookingNotificationBookingNotificationRequestContactCustomerPersonName[] personNameField;

    private string idField;

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("personName")]
    public BookingNotificationBookingNotificationRequestContactCustomerPersonName[] personName
    {
        get
        {
            return this.personNameField;
        }
        set
        {
            this.personNameField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestContactCustomerPersonName
{

    private string namePrefixField;

    private string givenNameField;

    private string surnameField;

    /// <comentarios/>
    public string namePrefix
    {
        get
        {
            return this.namePrefixField;
        }
        set
        {
            this.namePrefixField = value;
        }
    }

    /// <comentarios/>
    public string givenName
    {
        get
        {
            return this.givenNameField;
        }
        set
        {
            this.givenNameField = value;
        }
    }

    /// <comentarios/>
    public string surname
    {
        get
        {
            return this.surnameField;
        }
        set
        {
            this.surnameField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestDetailSourceDataValuationRoom
{

    private string textValuationField;

    private BookingNotificationBookingNotificationRequestDetailSourceDataValuationRoomLineValuation[] lineValuationField;

    private string refIdField;

    /// <comentarios/>
    public string textValuation
    {
        get
        {
            return this.textValuationField;
        }
        set
        {
            this.textValuationField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("lineValuation")]
    public BookingNotificationBookingNotificationRequestDetailSourceDataValuationRoomLineValuation[] lineValuation
    {
        get
        {
            return this.lineValuationField;
        }
        set
        {
            this.lineValuationField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string refId
    {
        get
        {
            return this.refIdField;
        }
        set
        {
            this.refIdField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestDetailSourceDataValuationRoomLineValuation
{

    private string baseBoardField;

    private string fareCodeField;

    private string fareNameField;

    private string applicationDayField;

    private string applicationGuestField;

    private string amountField;

    private string refIdField;

    /// <comentarios/>
    public string baseBoard
    {
        get
        {
            return this.baseBoardField;
        }
        set
        {
            this.baseBoardField = value;
        }
    }

    /// <comentarios/>
    public string fareCode
    {
        get
        {
            return this.fareCodeField;
        }
        set
        {
            this.fareCodeField = value;
        }
    }

    /// <comentarios/>
    public string fareName
    {
        get
        {
            return this.fareNameField;
        }
        set
        {
            this.fareNameField = value;
        }
    }

    /// <comentarios/>
    public string applicationDay
    {
        get
        {
            return this.applicationDayField;
        }
        set
        {
            this.applicationDayField = value;
        }
    }

    /// <comentarios/>
    public string applicationGuest
    {
        get
        {
            return this.applicationGuestField;
        }
        set
        {
            this.applicationGuestField = value;
        }
    }

    /// <comentarios/>
    public string amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string refId
    {
        get
        {
            return this.refIdField;
        }
        set
        {
            this.refIdField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestCommissionServiceCommission
{

    private string serviceIdField;

    private string commissionAmountField;

    private string commissionPercentageField;

    /// <comentarios/>
    public string serviceId
    {
        get
        {
            return this.serviceIdField;
        }
        set
        {
            this.serviceIdField = value;
        }
    }

    /// <comentarios/>
    public string commissionAmount
    {
        get
        {
            return this.commissionAmountField;
        }
        set
        {
            this.commissionAmountField = value;
        }
    }

    /// <comentarios/>
    public string commissionPercentage
    {
        get
        {
            return this.commissionPercentageField;
        }
        set
        {
            this.commissionPercentageField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestPaymentPlanPayment
{

    private string actionField;

    private string paymentStatusField;

    private string typeField;

    private string dateField;

    private string amountField;

    private string bookingRoomIdField;

    private string paymentTypeField;

    private BookingNotificationBookingNotificationRequestPaymentPlanPaymentCardDetail[] cardDetailField;

    private string idField;

    /// <comentarios/>
    public string action
    {
        get
        {
            return this.actionField;
        }
        set
        {
            this.actionField = value;
        }
    }

    /// <comentarios/>
    public string paymentStatus
    {
        get
        {
            return this.paymentStatusField;
        }
        set
        {
            this.paymentStatusField = value;
        }
    }

    /// <comentarios/>
    public string type
    {
        get
        {
            return this.typeField;
        }
        set
        {
            this.typeField = value;
        }
    }

    /// <comentarios/>
    public string date
    {
        get
        {
            return this.dateField;
        }
        set
        {
            this.dateField = value;
        }
    }

    /// <comentarios/>
    public string amount
    {
        get
        {
            return this.amountField;
        }
        set
        {
            this.amountField = value;
        }
    }

    /// <comentarios/>
    public string bookingRoomId
    {
        get
        {
            return this.bookingRoomIdField;
        }
        set
        {
            this.bookingRoomIdField = value;
        }
    }

    /// <comentarios/>
    public string paymentType
    {
        get
        {
            return this.paymentTypeField;
        }
        set
        {
            this.paymentTypeField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlElementAttribute("cardDetail")]
    public BookingNotificationBookingNotificationRequestPaymentPlanPaymentCardDetail[] cardDetail
    {
        get
        {
            return this.cardDetailField;
        }
        set
        {
            this.cardDetailField = value;
        }
    }

    /// <comentarios/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestPaymentPlanPaymentCardDetail
{

    private string cardTypeCodeField;

    private string cardTypeNameField;

    private string holderNameField;

    private string numberField;

    private string authorizationCodeField;

    private string seriesCodeField;

    private string expiryDateField;

    /// <comentarios/>
    public string cardTypeCode
    {
        get
        {
            return this.cardTypeCodeField;
        }
        set
        {
            this.cardTypeCodeField = value;
        }
    }

    /// <comentarios/>
    public string cardTypeName
    {
        get
        {
            return this.cardTypeNameField;
        }
        set
        {
            this.cardTypeNameField = value;
        }
    }

    /// <comentarios/>
    public string holderName
    {
        get
        {
            return this.holderNameField;
        }
        set
        {
            this.holderNameField = value;
        }
    }

    /// <comentarios/>
    public string number
    {
        get
        {
            return this.numberField;
        }
        set
        {
            this.numberField = value;
        }
    }

    /// <comentarios/>
    public string authorizationCode
    {
        get
        {
            return this.authorizationCodeField;
        }
        set
        {
            this.authorizationCodeField = value;
        }
    }

    /// <comentarios/>
    public string seriesCode
    {
        get
        {
            return this.seriesCodeField;
        }
        set
        {
            this.seriesCodeField = value;
        }
    }

    /// <comentarios/>
    public string expiryDate
    {
        get
        {
            return this.expiryDateField;
        }
        set
        {
            this.expiryDateField = value;
        }
    }
}

/// <comentarios/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.33440")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class BookingNotificationBookingNotificationRequestSystemInternalInformation
{

    private string xmlVersionField;

    private string nextIdField;

    /// <comentarios/>
    public string xmlVersion
    {
        get
        {
            return this.xmlVersionField;
        }
        set
        {
            this.xmlVersionField = value;
        }
    }

    /// <comentarios/>
    public string nextId
    {
        get
        {
            return this.nextIdField;
        }
        set
        {
            this.nextIdField = value;
        }
    }
}
