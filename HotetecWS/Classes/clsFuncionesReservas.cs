﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace HotetecWS
{
    public class clsFuncionesReservas
    {
        private string strCadenaDeConexion;

        public clsFuncionesReservas()
        {
            try
            {
                this.strCadenaDeConexion = System.Configuration.ConfigurationManager.AppSettings["CadenaConexionRutaWin"].ToString();
            }
            catch
            {
                this.strCadenaDeConexion = "";
            }
        }
        public DataTable RecuperaReservasXBono(string srBONO)
        {
            SqlDataAdapter daDataAdapter_AUX;
            DataTable dtDataTable_AUX;
            string strSQL = ""; //Guarda la query que enviamos a la base de datos.

            //Generamos la consulta.
            strSQL = "SELECT * ";
            strSQL += "FROM RESERVAS_NGHRES ";
            strSQL += string.Format("WHERE CBONO_RES = '{0}' ", srBONO);
            //strSQL += "AND NHOTELID_HOTRES = " + intHOTEL_WS + " ";
            strSQL += "ORDER BY NIDENTIFICADOR_RES";

            try
            {
                daDataAdapter_AUX = new SqlDataAdapter(strSQL, strCadenaDeConexion);

                dtDataTable_AUX = new DataTable();

                daDataAdapter_AUX.Fill(dtDataTable_AUX);

                return dtDataTable_AUX;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        //public DataTable RecuperaReservasXBonoATRATAR(string srBONO)
        //{
        //    SqlDataAdapter daDataAdapter_AUX;
        //    DataTable dtDataTable_AUX;
        //    string strSQL = ""; //Guarda la query que enviamos a la base de datos.

        //    //Generamos la consulta.
        //    strSQL = "SELECT * ";
        //    strSQL += "FROM RESERVAS_NGHRES ";
        //    strSQL += string.Format("WHERE CBONO_RES = '{0}' ", srBONO);
        //    strSQL += "AND NHOTELID_HOTRES = 1 ";
        //    strSQL += "AND NESTADO_ERERES IN (2, 6) ";
        //    strSQL += "ORDER BY NIDENTIFICADOR_RES";

        //    try
        //    {
        //        daDataAdapter_AUX = new SqlDataAdapter(strSQL, strCadenaDeConexion);

        //        dtDataTable_AUX = new DataTable();

        //        daDataAdapter_AUX.Fill(dtDataTable_AUX);

        //        return dtDataTable_AUX;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}
        public string InsertarPreReservaHOTETEC(BookingNotificationBookingNotificationRequestBookingRoom objBookingNotificationRequestBookingRoom, DateTime dtmCREACION, string strBookingReference, string strAGENCIA, int intQUE_HACEMOS, bool blnCrearPrecioManual, DataRow drRESERVA, string strPrimerOcupante, ref int intRESERVA_CREADA)
        {
            #region Declaración de Variables
            string sCon = this.strCadenaDeConexion;
            string strSQL_Edit = "";
            string strNombreTabla;
            int intNoches;
            int intI;
            int intAD = 0; //Adultos
            int intNI = 0; //Niños
            string strUSU_LOG = "";
            string strSQL_PRERESERVAS_NGHPRX = "";
            string strSQL_PRERESERVASDIA_NGHPRX = "";
            string strSQL_PRECIOS_PRERESERVA_NGHPRX = "";
            int intTHA = -1;
            string strTHA = "";
            string strTHA_Extra = "";
            int intREG = -1;
            string strREG = "";
            int intRES_PRX = -1;
            int intOCUPACION = 0;
            DataTable dtPRE_HOT = new DataTable();
            double dblDIA = 0;
            double dblALOJAMIENTO = 0;
            double dblDESAYUNO = 0;
            double dblALMUERZO = 0;
            double dblCENA = 0;
            double dblALLINCLUSIVE = 0;
            double dblPREMIUM = 0;
            int intESTATUS = -1;
            int intMODIFIED_RESERVATIONS = -1;
            int intRESERVATION_CODE = -1;
            int intRESERVATION_CODE_MODIFICADO = -1;
            bool blnMODIFICAR_RESERVA = false;
            int intRESERVA_RUTA_MODIFICVAR = -1;
            int t = -1;
            int intESTADO_RES = 6;
            int intResultadoUpdate = -1;
            string strCODIGO_RESERVA_EN_CANAL = ""; //channel_reservation_code
            int intCANAL = 0;
            int intDIMENSION_ARRAY = 0;
            bool blnESCANDALLAR = true;
            double dblTOTAL = 0;
            string strERROR_DESC = "";
            double dblAdutosTasa = 0;
            double dblNinsTasa = 0;
            TimeSpan tspNOCHES;
            int intNOCHES_RES = 0;
            int intAGE_GI = -1; //objRESERVA_GI.AGENCIA;
            int intCON_GI = -1; //objRESERVA_GI.CONTRATO;
            DateTime dtmENTRADA;
            DateTime dtmSALIDA;
            string strREMARKS = "";
            string strDelimitadorLog = "-SALTO-";
            int intHOTEL_WS = -1;
            string strComentarioHabitacion = "";
            string strComentarioRegimen = "";
            string strServiceCode = "";
            string strMontaError = "";
            #endregion

            strUSU_LOG = ""; //deberíamos crear un usuario para este caso

            using (SqlConnection con = new SqlConnection(sCon))
            {
                //Abrimos la conexión.
                con.Open();

                //Iniciamos una transaccion local.
                SqlTransaction sqlTran = con.BeginTransaction();

                SqlCommand cmd = new SqlCommand(strSQL_Edit, con);

                cmd.Transaction = sqlTran;

                try
                {
                    //Determinamos el HOTEL:
                    if (objBookingNotificationRequestBookingRoom.serviceCode != null && objBookingNotificationRequestBookingRoom.serviceCode.Trim() != "")
                    {
                        strServiceCode = objBookingNotificationRequestBookingRoom.serviceCode;

                        if (objBookingNotificationRequestBookingRoom.serviceCode.Trim() == "40617")
                            intHOTEL_WS = 2; //TRITON
                        else if (objBookingNotificationRequestBookingRoom.serviceCode.Trim() == "2827")
                            intHOTEL_WS = 1; //ILLOT
                    }

                    if (intHOTEL_WS == -1)
                    {
                        strERROR_DESC += "NO SE PUEDE DETERMINAR EL HOTEL DE DESTINO (serviceCode)" + strDelimitadorLog;
                        sqlTran.Rollback();
                        con.Close();
                        return strERROR_DESC;
                    }

                    //Determinamos la Agencia y el Contrato
                    this.AgenciaContratoHotetecCreaExiste(strAGENCIA, intHOTEL_WS, ref intAGE_GI, ref intCON_GI);

                    if(intAGE_GI== -1 || intCON_GI==-1)
                    {
                        strERROR_DESC += "NO SE PUEDE DETERMINAR LA AGENCIA Y/O EL CONTRATO EN EL SISTEMA (" + strAGENCIA + ")" + strDelimitadorLog;
                        sqlTran.Rollback();
                        con.Close();
                        return strERROR_DESC;
                    }

                    //Determinamos el tipo de habitacion
                    if (objBookingNotificationRequestBookingRoom.roomType != null && objBookingNotificationRequestBookingRoom.roomType.Trim() != "")
                        strTHA = objBookingNotificationRequestBookingRoom.roomType.Trim();
                    if (objBookingNotificationRequestBookingRoom.roomTypeExtra != null && objBookingNotificationRequestBookingRoom.roomTypeExtra.Trim() != "")
                        strTHA_Extra = objBookingNotificationRequestBookingRoom.roomTypeExtra.Trim();
                    
                    intTHA = DecodificaTHA(strTHA, strTHA_Extra, intHOTEL_WS, ref strComentarioHabitacion);

                    //Determinamos el Régimen
                    if (objBookingNotificationRequestBookingRoom.board != null && objBookingNotificationRequestBookingRoom.board.Trim() != "")
                        strREG = objBookingNotificationRequestBookingRoom.board.Trim();

                    intREG = DecodificaREG(strREG, ref strComentarioRegimen);

                    dtmENTRADA = DateTime.Parse(objBookingNotificationRequestBookingRoom.checkInDate);
                    dtmSALIDA = DateTime.Parse(objBookingNotificationRequestBookingRoom.checkOutDate);
                    tspNOCHES = dtmSALIDA - dtmENTRADA;
                    intNOCHES_RES = tspNOCHES.Days;
                    //objRESERVA = this.VolcarGuestincoming(objRESERVA_GI, intNOCHES_RES, intAGE_GI, intCON_GI);


                    //Querys
                    #region Construir la query de inserción en RESERVAS_NGHRES.
                    strNombreTabla = "RESERVAS_NGHRES";

                    strSQL_PRERESERVAS_NGHPRX = "INSERT INTO " + strNombreTabla + " ("
                                    + "NHOTELID_HOTRES, DVENTA_RES, CUSUARIOID_USURES, DHORA_RES, NTIPOESTANCIAID_TESRES, NAGENCIAID_AGERES, "
                                    + "NCONTRATOID_CONRES, DENTRADA_RES, DSALIDA_RES, NNOCHES_RES, BDAYUSE_RES, BMASTER_RES, BGRUPO_RES, CREFERENCIAFACTURA_RES, BFULLCREDIT_RES, "
                                    + "NTIPOHABITACIONUSOID_THARES, NTIPOHABITACIONFRAID_THARES, NREGIMENUSO_REGRES, NREGIMENFRA_REGRES, NHABITACION_HABRES, DHORAENTRADA_RES, DHORASALIDA_RES, "
                                    + "CVUELOLLEGADA_RES, CVUELOSALIDA_RES, BCREDITO_RES, CNOMBREPRIMEROCUPANTE_RES, NADULTOS_RES, NNINS_RES, NCUNAS_RES, CBONO_RES, NTRATOCLIENTE_TRCRES, "
                                    + "BDESAYUNOLLEGADA_RES, BALMUERZOLLEGADA_RES, BCENALLEGADA_RES, NESTADO_ERERES, NESTADOFACTURA_EFRRES, NSEGMENTO_SEGRES, BDESAYUNOSALIDA_RES, "
                                    + "BALMUERZOSALIDA_RES, BCENASALIDA_RES, DDIAENTRADA_RES, DDIASALIDA_RES, NTURNODESAYUNO_TCORES, NTURNOALMUERZO_TCORES, NTURNOCENA_TCORES, NMERCADO_MERRES, "
                                    + "CMATRICULACOCHE_RES, BNOSHOW_RES, COBSERVACIONES_RES, BDESGLOSADA_RGRRES, NCOMPLEJO_ORIGEN_CPJRES, NCOMPLEJO_CPJRES , NTARJETA_DE_CREDITO_TCCRES, CTITULAR_TARJETA_DE_CREDITO_RES, CNUMERO_TARJETA_DE_CREDITO_RES, "
                                    + "CMES_CADUCIDAD_TARJETA_DE_CREDITO_RES, CANYO_CADUCIDAD_TARJETA_DE_CREDITO_RES, CCSV_TARJETA_DE_CREDITO_RES, "
                                    + "COBSERVACIONES_LLEGADA_RES, COBSERVACIONES_SALIDA_RES, NCAMAS_EXTRAS_RES, NRESERVA_GRUPO_RGRRES, "
                                    + "BECOTASA_APLICADA_RES, NADULTOS_ECOTASA_RES, NNINS_ECOTASA_RES, NECOTASA_VALOR_POR_ADULTO_RES, NECOTASA_VALOR_POR_NIN_RES"
                                    + ") "
                                    + "VALUES ("
                                    + "@NHOTELID_HOTRES, @DVENTA_RES, @CUSUARIOID_USURES, @DHORA_RES, @NTIPOESTANCIAID_TESRES, @NAGENCIAID_AGERES, "
                                    + "@NCONTRATOID_CONRES, @DENTRADA_RES, @DSALIDA_RES, @NNOCHES_RES, @BDAYUSE_RES, @BMASTER_RES, @BGRUPO_RES, @CREFERENCIAFACTURA_RES, @BFULLCREDIT_RES, "
                                    + "@NTIPOHABITACIONUSOID_THARES, @NTIPOHABITACIONFRAID_THARES, @NREGIMENUSO_REGRES, @NREGIMENFRA_REGRES, @NHABITACION_HABRES, @DHORAENTRADA_RES, @DHORASALIDA_RES, "
                                    + "@CVUELOLLEGADA_RES, @CVUELOSALIDA_RES, @BCREDITO_RES, @CNOMBREPRIMEROCUPANTE_RES, @NADULTOS_RES, @NNINS_RES, @NCUNAS_RES, @CBONO_RES, @NTRATOCLIENTE_TRCRES, "
                                    + "@BDESAYUNOLLEGADA_RES, @BALMUERZOLLEGADA_RES, @BCENALLEGADA_RES, @NESTADO_ERERES, @NESTADOFACTURA_EFRRES, @NSEGMENTO_SEGRES, @BDESAYUNOSALIDA_RES, "
                                    + "@BALMUERZOSALIDA_RES, @BCENASALIDA_RES, @DDIAENTRADA_RES, @DDIASALIDA_RES, @NTURNODESAYUNO_TCORES, @NTURNOALMUERZO_TCORES, @NTURNOCENA_TCORES, @NMERCADO_MERRES, "
                                    + "@CMATRICULACOCHE_RES, @BNOSHOW_RES, @COBSERVACIONES_RES, @BDESGLOSADA_RGRRES, @NCOMPLEJO_ORIGEN_CPJRES, @NCOMPLEJO_CPJRES, @NTARJETA_DE_CREDITO_TCCRES, @CTITULAR_TARJETA_DE_CREDITO_RES, @CNUMERO_TARJETA_DE_CREDITO_RES, "
                                    + "@CMES_CADUCIDAD_TARJETA_DE_CREDITO_RES, @CANYO_CADUCIDAD_TARJETA_DE_CREDITO_RES, @CCSV_TARJETA_DE_CREDITO_RES, "
                                    + "@COBSERVACIONES_LLEGADA_RES, @COBSERVACIONES_SALIDA_RES, @NCAMAS_EXTRAS_RES, @NRESERVA_GRUPO_RGRRES, "
                                    + "@BECOTASA_APLICADA_RES, @NADULTOS_ECOTASA_RES, @NNINS_ECOTASA_RES, @NECOTASA_VALOR_POR_ADULTO_RES, @NECOTASA_VALOR_POR_NIN_RES "
                                    + ") "
                                    + "SELECT @@Identity";
                    #endregion

                    #region Construir la query de inserción en RESERVAS_DIARIAS_NGHRED.
                    strNombreTabla = "RESERVAS_DIARIAS_NGHRED";

                    strSQL_PRERESERVASDIA_NGHPRX = "INSERT INTO " + strNombreTabla + " ("
                                    + "DDIA_RED, NRESERVAID_RESRED, NHOTELID_HOTRED, DVENTA_RED, CUSUARIOID_USURED, DHORA_RED, NTIPOESTANCIAID_TESRED, NAGENCIAID_AGERED, "
                                    + "NCONTRATOID_CONRED, DENTRADA_RED, DSALIDA_RED, NNOCHES_RED, "
                                    + "NTIPOHABITACIONUSOID_THARED, NTIPOHABITACIONFRAID_THARED, NREGIMENUSO_REGRED, NREGIMENFRA_REGRED, "
                                    + "CNOMBREPRIMEROCUPANTE_RED, NADULTOS_RED, NNINS_RED, NCUNAS_RED, CBONO_RED, NTRATOCLIENTE_TRCRED, NESTADO_ERERED, NESTADOFACTURA_EFRRED, "
                                    + "NCOMPLEJO_REDCPJ, NPRECIO_DIARIO_RED"
                                    + ") "
                                    + "VALUES ("
                                    + "@DDIA_RED, @NRESERVAID_RESRED, @NHOTELID_HOTRED, @DVENTA_RED, @CUSUARIOID_USURED, @DHORA_RED, @NTIPOESTANCIAID_TESRED, @NAGENCIAID_AGERED, "
                                    + "@NCONTRATOID_CONRED, @DENTRADA_RED, @DSALIDA_RED, @NNOCHES_RED, "
                                    + "@NTIPOHABITACIONUSOID_THARED, @NTIPOHABITACIONFRAID_THARED, @NREGIMENUSO_REGRED, @NREGIMENFRA_REGRED, "
                                    + "@CNOMBREPRIMEROCUPANTE_RED, @NADULTOS_RED, @NNINS_RED, @NCUNAS_RED, @CBONO_RED, @NTRATOCLIENTE_TRCRED, @NESTADO_ERERED, @NESTADOFACTURA_EFRRED, "
                                    + "@NCOMPLEJO_REDCPJ, @NPRECIO_DIARIO_RED"
                                    + ") "
                                    + "SELECT @@Identity";
                    #endregion

                    #region Construir la query de inserción en PRECIOS_MANUALES_NGHPRM.
                    strNombreTabla = "PRECIOS_MANUALES_NGHPRM";

                    strSQL_PRECIOS_PRERESERVA_NGHPRX = "INSERT INTO " + strNombreTabla + " ("
                                    + "NIDENTIFICADOR_RESPRM, NHOTELID_RESPRM, DDESDE_PRM, DHASTA_PRM, NREGIMENID_REGPRM, "
                                    + "NTIPOHABITACIONID_THAPRM, NCOMPLEJO_CPJPRM, NTIPOPRECIO_PRM, NPRECIO_PRM, "
                                    + "NALOJAMIENTO_PRM, NDESAYUNO_PRM, "
                                    + "NALMUERZO_PRM, NCENA_PRM, NALLINCLUSIVE_PRM, NPREMIUM_PRM, NALOJAMIENTOID_CPRPRM, NDESAYUNOID_CPRPRM, NALMUERZOID_CPRPRM, NCENAID_CPRPRM, "
                                    + "NALLINCLUSIVE_CPRPRM, NPREMIUM_CPRPRM, NIMPORTEVENTA_PRM, NPORCENTAJEVENTA_PRM, NPRECIO_TOTAL_PRM, BOFERTA_PRM, APLICAR_DYS_DE_CONTRATO_PRM, APLICAR_EBO_DE_CONTRATO_PRM"
                                    + ") "
                                    + "VALUES ("
                                    + "@NIDENTIFICADOR_RESPRM, @NHOTELID_RESPRM, @DDESDE_PRM, @DHASTA_PRM, @NREGIMENID_REGPRM, "
                                    + "@NTIPOHABITACIONID_THAPRM, @NCOMPLEJO_CPJPRM, @NTIPOPRECIO_PRM, @NPRECIO_PRM, "
                                    + "@NALOJAMIENTO_PRM, @NDESAYUNO_PRM, "
                                    + "@NALMUERZO_PRM, @NCENA_PRM, @NALLINCLUSIVE_PRM, @NPREMIUM_PRM, @NALOJAMIENTOID_CPRPRM, @NDESAYUNOID_CPRPRM, @NALMUERZOID_CPRPRM, @NCENAID_CPRPRM, "
                                    + "@NALLINCLUSIVE_CPRPRM, @NPREMIUM_CPRPRM, @NIMPORTEVENTA_PRM, @NPORCENTAJEVENTA_PRM, @NPRECIO_TOTAL_PRM, @BOFERTA_PRM, @APLICAR_DYS_DE_CONTRATO_PRM, @APLICAR_EBO_DE_CONTRATO_PRM"
                                    + ") "
                                    + "SELECT @@Identity";
                    #endregion

                    switch (intQUE_HACEMOS)
                    {
                        case 0: //NUEVA RESERVA
                            strERROR_DESC += "Reserva NO AÑADIDA. Error al tratar de INSERTAR la Reserva en el Sistema" + strDelimitadorLog;
                            #region Asignamos valores a los parametros para PRERESERVAS_NGHPRX.
                            cmd.Parameters.AddWithValue("@NHOTELID_HOTRES", intHOTEL_WS);
                            cmd.Parameters.AddWithValue("@DVENTA_RES", dtmCREACION);
                            cmd.Parameters.AddWithValue("@CUSUARIOID_USURES", "wshotetec");
                            cmd.Parameters.AddWithValue("@DHORA_RES", dtmCREACION);
                            cmd.Parameters.AddWithValue("@NTIPOESTANCIAID_TESRES", 1); //1: Agencia
                            cmd.Parameters.AddWithValue("@NAGENCIAID_AGERES", intAGE_GI);
                            cmd.Parameters.AddWithValue("@NCONTRATOID_CONRES", intCON_GI);
                            cmd.Parameters.AddWithValue("@DENTRADA_RES", dtmENTRADA);
                            cmd.Parameters.AddWithValue("@DSALIDA_RES", dtmSALIDA);

                            cmd.Parameters.AddWithValue("@NNOCHES_RES", intNOCHES_RES);

                            cmd.Parameters.AddWithValue("@BDAYUSE_RES", false);
                            cmd.Parameters.AddWithValue("@BMASTER_RES", false);
                            cmd.Parameters.AddWithValue("@BGRUPO_RES", false);
                            cmd.Parameters.AddWithValue("@CREFERENCIAFACTURA_RES", "");
                            cmd.Parameters.AddWithValue("@BFULLCREDIT_RES", false);

                            cmd.Parameters.AddWithValue("@NTIPOHABITACIONUSOID_THARES", intTHA);
                            cmd.Parameters.AddWithValue("@NTIPOHABITACIONFRAID_THARES", intTHA);
                            cmd.Parameters.AddWithValue("@NREGIMENUSO_REGRES", intREG);
                            cmd.Parameters.AddWithValue("@NREGIMENFRA_REGRES", intREG);

                            cmd.Parameters.AddWithValue("@NHABITACION_HABRES", DBNull.Value);

                            //if (objRESERVA_GI.HoraEntrada == null)
                            //objRESERVA_GI.HoraEntrada = objRESERVA_GI.Entrada.AddHours(this.HoraEntrada());

                            cmd.Parameters.AddWithValue("@DHORAENTRADA_RES", dtmENTRADA.AddHours(this.HoraEntrada(intHOTEL_WS)));

                            //if (objRESERVA_GI.HoraSalida == null)
                            //objRESERVA_GI.HoraSalida = objRESERVA_GI.Salida.AddHours(this.HoraSalida());

                            cmd.Parameters.AddWithValue("@DHORASALIDA_RES", dtmSALIDA.AddHours(this.HoraSalida(intHOTEL_WS)));

                            cmd.Parameters.AddWithValue("@CVUELOLLEGADA_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CVUELOSALIDA_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@BCREDITO_RES", false);

                            if (objBookingNotificationRequestBookingRoom.guest[0].personName != null)
                                cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_RES", objBookingNotificationRequestBookingRoom.guest[0].personName.givenName + objBookingNotificationRequestBookingRoom.guest[0].personName.surname);
                            else
                                cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_RES", strPrimerOcupante);

                            //DETERMINAR EL NÚMERO DE ADULTOS, NIÑOS, CUNAS CON LA FECHA DE NACIMIENTO
                            cmd.Parameters.AddWithValue("@NADULTOS_RES", objBookingNotificationRequestBookingRoom.guest.Length);
                            cmd.Parameters.AddWithValue("@NNINS_RES", 0);
                            cmd.Parameters.AddWithValue("@NCUNAS_RES", 0);
                            cmd.Parameters.AddWithValue("@CBONO_RES", strBookingReference);

                            cmd.Parameters.AddWithValue("@NTRATOCLIENTE_TRCRES", 1); //1: Cliente
                            cmd.Parameters.AddWithValue("@BDESAYUNOLLEGADA_RES", false);
                            cmd.Parameters.AddWithValue("@BALMUERZOLLEGADA_RES", false);
                            cmd.Parameters.AddWithValue("@BCENALLEGADA_RES", false);

                            cmd.Parameters.AddWithValue("@NESTADO_ERERES", 6); //6: Previo al 2
                            cmd.Parameters.AddWithValue("@NESTADOFACTURA_EFRRES", 1); //1: Sin Facturar

                            cmd.Parameters.AddWithValue("@NSEGMENTO_SEGRES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@BDESAYUNOSALIDA_RES", false);
                            cmd.Parameters.AddWithValue("@BALMUERZOSALIDA_RES", false);
                            cmd.Parameters.AddWithValue("@BCENASALIDA_RES", false);

                            cmd.Parameters.AddWithValue("@DDIAENTRADA_RES", dtmENTRADA);
                            cmd.Parameters.AddWithValue("@DDIASALIDA_RES", dtmSALIDA);

                            cmd.Parameters.AddWithValue("@NTURNODESAYUNO_TCORES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@NTURNOALMUERZO_TCORES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@NTURNOCENA_TCORES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@NMERCADO_MERRES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CMATRICULACOCHE_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@BNOSHOW_RES", false);

                            strREMARKS = strComentarioHabitacion + " * " + strComentarioRegimen;
                            if (objBookingNotificationRequestBookingRoom.remark != null)
                            {
                                for (int intRemarks = 0; intRemarks < objBookingNotificationRequestBookingRoom.remark.Length; intRemarks++)
                                {
                                    strREMARKS += " * ";
                                    strREMARKS += objBookingNotificationRequestBookingRoom.remark[intRemarks].text;
                                }
                            }

                            if (objBookingNotificationRequestBookingRoom.bookingSupplement != null)
                            {
                                //Voy a añadir aquí las características de los suplementos, si hay
                                for (int intSupplement = 0; intSupplement < objBookingNotificationRequestBookingRoom.bookingSupplement.Length; intSupplement++)
                                {
                                    strREMARKS += " * ";
                                    strREMARKS += objBookingNotificationRequestBookingRoom.bookingSupplement[intSupplement].supplementCode + ",";
                                    strREMARKS += objBookingNotificationRequestBookingRoom.bookingSupplement[intSupplement].supplementName + ",";
                                    strREMARKS += objBookingNotificationRequestBookingRoom.bookingSupplement[intSupplement].supplementStatus + ",";
                                    strREMARKS += objBookingNotificationRequestBookingRoom.bookingSupplement[intSupplement].amount + ",";
                                }
                            }

                            cmd.Parameters.AddWithValue("@COBSERVACIONES_RES", strREMARKS);

                            cmd.Parameters.AddWithValue("@BDESGLOSADA_RGRRES", false);
                            cmd.Parameters.AddWithValue("@NCOMPLEJO_ORIGEN_CPJRES", 1);

                            cmd.Parameters.AddWithValue("@NCOMPLEJO_CPJRES", 1); //1: Sin Complejo

                            cmd.Parameters.AddWithValue("@NTARJETA_DE_CREDITO_TCCRES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CTITULAR_TARJETA_DE_CREDITO_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CNUMERO_TARJETA_DE_CREDITO_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CMES_CADUCIDAD_TARJETA_DE_CREDITO_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CANYO_CADUCIDAD_TARJETA_DE_CREDITO_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CCSV_TARJETA_DE_CREDITO_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@COBSERVACIONES_LLEGADA_RES", DBNull.Value);
                            cmd.Parameters.AddWithValue("@COBSERVACIONES_SALIDA_RES", DBNull.Value);

                            cmd.Parameters.AddWithValue("@NCAMAS_EXTRAS_RES", 0); //???????


                            cmd.Parameters.AddWithValue("@NRESERVA_GRUPO_RGRRES", DBNull.Value);

                            cmd.Parameters.AddWithValue("@BECOTASA_APLICADA_RES", false);

                            cmd.Parameters.AddWithValue("@NADULTOS_ECOTASA_RES", objBookingNotificationRequestBookingRoom.guest.Length);
                            cmd.Parameters.AddWithValue("@NNINS_ECOTASA_RES", 0);
                            if (this.ValoresTasaTuristica(intHOTEL_WS, ref dblAdutosTasa, ref dblNinsTasa))
                            {
                                cmd.Parameters.AddWithValue("@NECOTASA_VALOR_POR_ADULTO_RES", dblAdutosTasa);
                                cmd.Parameters.AddWithValue("@NECOTASA_VALOR_POR_NIN_RES", dblNinsTasa);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NECOTASA_VALOR_POR_ADULTO_RES", 0);
                                cmd.Parameters.AddWithValue("@NECOTASA_VALOR_POR_NIN_RES", 0);
                            }

                            //cmd.Parameters.AddWithValue("@CCREATORID_GUESTINCOMING_RES", objRESERVA_GI.CreatorID);
                            //cmd.Parameters.AddWithValue("@CUNIQUEID_GUESTINCOMING_RES", objRESERVA_GI.UniqueID);
                            //cmd.Parameters.AddWithValue("@CLASTMODIFYDATETIME_GUESTINCOMING_RES", objRESERVA_GI.LastModifyDateTime);

                            //for (int intPRECIO = 0; intPRECIO < objRESERVA_GI.Noches; intPRECIO++)
                            //{
                            //    dblTOTAL += arrPrecios[intPRECIO];
                            //}
                            #endregion

                            cmd.CommandText = strSQL_PRERESERVAS_NGHPRX;
                            t = Convert.ToInt32(cmd.ExecuteScalar());
                            cmd.Parameters.Clear();

                            intRES_PRX = t;

                            strERROR_DESC += "Reserva NO AÑADIDA. Error al tratar de INSERTAR las Reservas Diarias en el Sistema." + strDelimitadorLog;
                            #region Asignamos valores a los parámetros para PRERESERVASDIA_NGHPRX y para PRECIOS_PRERESERVA_NGHPRX y ejecutamos las querys.
                            for (intI = 0; intI <= intNOCHES_RES - 1; intI++)
                            {
                                //PRE RESERVAS DIARIAS
                                #region Parámetros para PRERESERVASDIA_NGHPRX
                                cmd.Parameters.AddWithValue("@NRESERVAID_RESRED", intRES_PRX);
                                cmd.Parameters.AddWithValue("@DDIA_RED", dtmENTRADA.AddDays(double.Parse(intI.ToString())));
                                cmd.Parameters.AddWithValue("@NHOTELID_HOTRED", intHOTEL_WS);
                                cmd.Parameters.AddWithValue("@DVENTA_RED", dtmCREACION);
                                cmd.Parameters.AddWithValue("@CUSUARIOID_USURED", "wshotetec");
                                cmd.Parameters.AddWithValue("@DHORA_RED", dtmCREACION);
                                cmd.Parameters.AddWithValue("@NTIPOESTANCIAID_TESRED", 1); //1: Agencia
                                cmd.Parameters.AddWithValue("@NAGENCIAID_AGERED", intAGE_GI);
                                cmd.Parameters.AddWithValue("@NCONTRATOID_CONRED", intCON_GI);
                                cmd.Parameters.AddWithValue("@DENTRADA_RED", dtmENTRADA);
                                cmd.Parameters.AddWithValue("@DSALIDA_RED", dtmSALIDA);
                                cmd.Parameters.AddWithValue("@NNOCHES_RED", intNOCHES_RES);
                                cmd.Parameters.AddWithValue("@NTIPOHABITACIONUSOID_THARED", intTHA);
                                cmd.Parameters.AddWithValue("@NREGIMENUSO_REGRED", intREG);
                                cmd.Parameters.AddWithValue("@NTIPOHABITACIONFRAID_THARED", intTHA);
                                cmd.Parameters.AddWithValue("@NREGIMENFRA_REGRED", intREG);

                                if (objBookingNotificationRequestBookingRoom.guest[0].personName != null)
                                    cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_RED", objBookingNotificationRequestBookingRoom.guest[0].personName.givenName + objBookingNotificationRequestBookingRoom.guest[0].personName.surname);
                                else
                                {
                                    cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_RED", strPrimerOcupante);
                                }
                                    

                                //DETERMINAR EL NÚMERO DE ADULTOS, NIÑOS, CUNAS CON LA FECHA DE NACIMIENTO
                                cmd.Parameters.AddWithValue("@NADULTOS_RED", objBookingNotificationRequestBookingRoom.guest.Length);
                                cmd.Parameters.AddWithValue("@NNINS_RED", 0);
                                cmd.Parameters.AddWithValue("@NCUNAS_RED", 0);

                                cmd.Parameters.AddWithValue("@CBONO_RED", strBookingReference);
                                cmd.Parameters.AddWithValue("@NTRATOCLIENTE_TRCRED", 1); //1: Cliente
                                cmd.Parameters.AddWithValue("@NESTADO_ERERED", 6); //6: Previo al 2
                                cmd.Parameters.AddWithValue("@NESTADOFACTURA_EFRRED", 1); //1: Sin Facturar
                                cmd.Parameters.AddWithValue("@NCOMPLEJO_REDCPJ", 1); //1: Sin Complejo

                                try
                                {
                                    if (objBookingNotificationRequestBookingRoom.amount != null && objBookingNotificationRequestBookingRoom.amount.Trim() != "")
                                        cmd.Parameters.AddWithValue("@NPRECIO_DIARIO_RED", double.Parse(objBookingNotificationRequestBookingRoom.amount.Replace(".", ",")) / intNOCHES_RES);
                                    else
                                        cmd.Parameters.AddWithValue("@NPRECIO_DIARIO_RED", 0);
                                }
                                catch
                                {
                                    cmd.Parameters.AddWithValue("@NPRECIO_DIARIO_RED", 0);
                                }

                                #endregion

                                cmd.CommandText = strSQL_PRERESERVASDIA_NGHPRX;
                                cmd.ExecuteNonQuery();
                                cmd.Parameters.Clear();

                                if (blnCrearPrecioManual && (objBookingNotificationRequestBookingRoom.amount != null && objBookingNotificationRequestBookingRoom.amount.Trim() != ""))
                                {
                                    strERROR_DESC += "Reserva NO AÑADIDA. Error al tratar de INSERTAR los PRECIOS de la Reserva en el Sistema" + strDelimitadorLog;
                                    //PRECIOS
                                    #region Parámetros para PRECIOS_PRERESERVA_NGHPRX
                                    cmd.Parameters.AddWithValue("@NIDENTIFICADOR_RESPRM", intRES_PRX);
                                    cmd.Parameters.AddWithValue("@DDESDE_PRM", dtmENTRADA.AddDays(double.Parse(intI.ToString())));
                                    cmd.Parameters.AddWithValue("@DHASTA_PRM", dtmENTRADA.AddDays(double.Parse(intI.ToString())));
                                    cmd.Parameters.AddWithValue("@NHOTELID_RESPRM", intHOTEL_WS);
                                    cmd.Parameters.AddWithValue("@NREGIMENID_REGPRM", intREG);
                                    cmd.Parameters.AddWithValue("@NTIPOHABITACIONID_THAPRM", intTHA);
                                    cmd.Parameters.AddWithValue("@NCOMPLEJO_CPJPRM", 1);
                                    cmd.Parameters.AddWithValue("@NTIPOPRECIO_PRM", 1); //Por HABITACIÓN (1)
                                    cmd.Parameters.AddWithValue("@NPRECIO_PRM", DBNull.Value); //Esto es Tipo Precio, 1: Total

                                    //POR AHORA PRECIO A PIÑÓN EN ALOJAMIENTO, ESCANDALLAR MEDIENTA PRECIOS Y CPRS POR DEFECTO
                                    dtPRE_HOT = new DataTable();
                                    dblDIA = 0;
                                    dblALOJAMIENTO = 0;
                                    dblDESAYUNO = 0;
                                    dblALMUERZO = 0;
                                    dblCENA = 0;
                                    dblALLINCLUSIVE = 0;
                                    dblPREMIUM = 0;

                                    //dblDIA = arrPrecios[intI];
                                    try
                                    {
                                        dblDIA = double.Parse(objBookingNotificationRequestBookingRoom.amount.Replace(".", ",")) / intNOCHES_RES;
                                    }
                                    catch
                                    {
                                        dblDIA = 0;
                                    }
                                    dblALOJAMIENTO = dblDIA;

                                    cmd.Parameters.AddWithValue("@NALOJAMIENTO_PRM", dblALOJAMIENTO);
                                    cmd.Parameters.AddWithValue("@NDESAYUNO_PRM", dblDESAYUNO);
                                    cmd.Parameters.AddWithValue("@NALMUERZO_PRM", dblALMUERZO);
                                    cmd.Parameters.AddWithValue("@NCENA_PRM", dblCENA);
                                    cmd.Parameters.AddWithValue("@NALLINCLUSIVE_PRM", dblALLINCLUSIVE);
                                    cmd.Parameters.AddWithValue("@NPREMIUM_PRM", dblPREMIUM);

                                    if (dblALOJAMIENTO == 0)
                                        cmd.Parameters.AddWithValue("@NALOJAMIENTOID_CPRPRM", DBNull.Value);
                                    else
                                        cmd.Parameters.AddWithValue("@NALOJAMIENTOID_CPRPRM", this.RecuperaCPR_Defecto("CPR_ALOJAMIENTO", intHOTEL_WS)); //13-08-2015 AHORA BUSCO POR RÉGIMEN

                                    if (dblDESAYUNO == 0)
                                        cmd.Parameters.AddWithValue("@NDESAYUNOID_CPRPRM", DBNull.Value);
                                    else
                                        cmd.Parameters.AddWithValue("@NDESAYUNOID_CPRPRM", this.RecuperaCPR_Defecto("CPR_DESAYUNO", intHOTEL_WS));


                                    if (dblALMUERZO == 0)
                                        cmd.Parameters.AddWithValue("@NALMUERZOID_CPRPRM", DBNull.Value);
                                    else
                                        cmd.Parameters.AddWithValue("@NALMUERZOID_CPRPRM", this.RecuperaCPR_Defecto("CPR_ALMUERZO", intHOTEL_WS));

                                    if (dblCENA == 0)
                                        cmd.Parameters.AddWithValue("@NCENAID_CPRPRM", DBNull.Value);
                                    else
                                        cmd.Parameters.AddWithValue("@NCENAID_CPRPRM", this.RecuperaCPR_Defecto("CPR_CENA", intHOTEL_WS));

                                    if (dblALLINCLUSIVE == 0)
                                        cmd.Parameters.AddWithValue("@NALLINCLUSIVE_CPRPRM", DBNull.Value);
                                    else
                                        cmd.Parameters.AddWithValue("@NALLINCLUSIVE_CPRPRM", this.RecuperaCPR_Defecto("CPR_ALL_INCLUSIVE", intHOTEL_WS));

                                    if (dblPREMIUM == 0)
                                        cmd.Parameters.AddWithValue("@NPREMIUM_CPRPRM", DBNull.Value);
                                    else
                                        cmd.Parameters.AddWithValue("@NPREMIUM_CPRPRM", this.RecuperaCPR_Defecto("CPR_PREMIUM", intHOTEL_WS));

                                    cmd.Parameters.AddWithValue("@NIMPORTEVENTA_PRM", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@NPORCENTAJEVENTA_PRM", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@NPRECIO_TOTAL_PRM", dblDIA);
                                    cmd.Parameters.AddWithValue("@BOFERTA_PRM", false);
                                    cmd.Parameters.AddWithValue("@APLICAR_DYS_DE_CONTRATO_PRM", false);
                                    cmd.Parameters.AddWithValue("@APLICAR_EBO_DE_CONTRATO_PRM", false);
                                    #endregion

                                    cmd.CommandText = strSQL_PRECIOS_PRERESERVA_NGHPRX;
                                    cmd.ExecuteNonQuery();
                                    cmd.Parameters.Clear();
                                }
                            }

                            #endregion

                            this.InsertarOcupantes(cmd, objBookingNotificationRequestBookingRoom, t);
                            break;

                        case 1: //MODIFICACIÓN
                            strERROR_DESC += "Reserva NO MODIFICADA." + strDelimitadorLog;
                            clsReservas objRESERVA = new clsReservas();
                            //PRIMERO ELIMINO TODO, DESPUÉS INSERTO DE NUEVO
                            if (this.VolcarReservaEnObjetoReserva(drRESERVA, intHOTEL_WS, ref objRESERVA))
                            {
                                intResultadoUpdate = this.EliminarReservaEnOtraTransaccion(cmd, objRESERVA, false, intHOTEL_WS);

                                if (intResultadoUpdate != 1)
                                {
                                    strERROR_DESC += "Error al tratar de MODIFICAR la Reserva en el Sistema (1)" + strDelimitadorLog;
                                    sqlTran.Rollback();
                                    con.Close();
                                    //strLOG_HOTETEC += "ERROR AL TRATAR DE MODIFICAR LA RESERVA EN EL SISTEMA (1)" + strDelimitadorLog;
                                    return strERROR_DESC;
                                }
                                else
                                {
                                    #region Asignamos valores a los parametros para PRERESERVAS_NGHPRX.
                                    cmd.Parameters.AddWithValue("@NHOTELID_HOTRES", intHOTEL_WS);
                                    cmd.Parameters.AddWithValue("@DVENTA_RES", dtmCREACION); //Será la que indique la descarga del listado donde esta la reserva
                                    cmd.Parameters.AddWithValue("@CUSUARIOID_USURES", "wshotetec");
                                    cmd.Parameters.AddWithValue("@DHORA_RES", dtmCREACION); //Será la que indique la descarga del listado donde esta la reserva
                                    cmd.Parameters.AddWithValue("@NTIPOESTANCIAID_TESRES", 1); //1: Agencia
                                    cmd.Parameters.AddWithValue("@NAGENCIAID_AGERES", intAGE_GI); //2 para Palas
                                    cmd.Parameters.AddWithValue("@NCONTRATOID_CONRES", intCON_GI); //PREGUNTAR CUAL ES
                                    cmd.Parameters.AddWithValue("@DENTRADA_RES", dtmENTRADA);
                                    cmd.Parameters.AddWithValue("@DSALIDA_RES", dtmSALIDA);

                                    cmd.Parameters.AddWithValue("@NNOCHES_RES", intNOCHES_RES);

                                    cmd.Parameters.AddWithValue("@BDAYUSE_RES", false);
                                    cmd.Parameters.AddWithValue("@BMASTER_RES", false);
                                    cmd.Parameters.AddWithValue("@BGRUPO_RES", false);
                                    cmd.Parameters.AddWithValue("@CREFERENCIAFACTURA_RES", "");
                                    cmd.Parameters.AddWithValue("@BFULLCREDIT_RES", false);

                                    cmd.Parameters.AddWithValue("@NTIPOHABITACIONUSOID_THARES", intTHA);
                                    cmd.Parameters.AddWithValue("@NTIPOHABITACIONFRAID_THARES", intTHA);
                                    cmd.Parameters.AddWithValue("@NREGIMENUSO_REGRES", intREG);
                                    cmd.Parameters.AddWithValue("@NREGIMENFRA_REGRES", intREG);

                                    cmd.Parameters.AddWithValue("@NHABITACION_HABRES", DBNull.Value);

                                    //if (objRESERVA_GI.HoraEntrada == null)
                                    //objRESERVA_GI.HoraEntrada = objRESERVA_GI.Entrada.AddHours(this.HoraEntrada());

                                    cmd.Parameters.AddWithValue("@DHORAENTRADA_RES", dtmENTRADA.AddHours(this.HoraEntrada(intHOTEL_WS)));

                                    //if (objRESERVA_GI.HoraSalida == null)
                                    //objRESERVA_GI.HoraSalida = objRESERVA_GI.Salida.AddHours(this.HoraSalida());

                                    cmd.Parameters.AddWithValue("@DHORASALIDA_RES", dtmSALIDA.AddHours(this.HoraSalida(intHOTEL_WS)));

                                    cmd.Parameters.AddWithValue("@CVUELOLLEGADA_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@CVUELOSALIDA_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@BCREDITO_RES", false);

                                    if (objBookingNotificationRequestBookingRoom.guest[0].personName != null)
                                        cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_RES", objBookingNotificationRequestBookingRoom.guest[0].personName.givenName + objBookingNotificationRequestBookingRoom.guest[0].personName.surname);
                                    else
                                        cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_RES", strPrimerOcupante);

                                    //DETERMINAR EL NÚMERO DE ADULTOS, NIÑOS, CUNAS CON LA FECHA DE NACIMIENTO
                                    cmd.Parameters.AddWithValue("@NADULTOS_RES", objBookingNotificationRequestBookingRoom.guest.Length);
                                    cmd.Parameters.AddWithValue("@NNINS_RES", 0);
                                    cmd.Parameters.AddWithValue("@NCUNAS_RES", 0);
                                    cmd.Parameters.AddWithValue("@CBONO_RES", strBookingReference);

                                    cmd.Parameters.AddWithValue("@NTRATOCLIENTE_TRCRES", 1); //1: Cliente
                                    cmd.Parameters.AddWithValue("@BDESAYUNOLLEGADA_RES", false);
                                    cmd.Parameters.AddWithValue("@BALMUERZOLLEGADA_RES", false);
                                    cmd.Parameters.AddWithValue("@BCENALLEGADA_RES", false);

                                    cmd.Parameters.AddWithValue("@NESTADO_ERERES", 6); //6: Previo al 2
                                    cmd.Parameters.AddWithValue("@NESTADOFACTURA_EFRRES", 1); //1: Sin Facturar

                                    cmd.Parameters.AddWithValue("@NSEGMENTO_SEGRES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@BDESAYUNOSALIDA_RES", false);
                                    cmd.Parameters.AddWithValue("@BALMUERZOSALIDA_RES", false);
                                    cmd.Parameters.AddWithValue("@BCENASALIDA_RES", false);

                                    cmd.Parameters.AddWithValue("@DDIAENTRADA_RES", dtmENTRADA);
                                    cmd.Parameters.AddWithValue("@DDIASALIDA_RES", dtmSALIDA);

                                    cmd.Parameters.AddWithValue("@NTURNODESAYUNO_TCORES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@NTURNOALMUERZO_TCORES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@NTURNOCENA_TCORES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@NMERCADO_MERRES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@CMATRICULACOCHE_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@BNOSHOW_RES", false);

                                    strREMARKS = strComentarioHabitacion + " * " + strComentarioRegimen;
                                    if (objBookingNotificationRequestBookingRoom.remark != null)
                                    {
                                        for (int intRemarks = 0; intRemarks < objBookingNotificationRequestBookingRoom.remark.Length; intRemarks++)
                                        {
                                            strREMARKS += " * ";
                                            strREMARKS += objBookingNotificationRequestBookingRoom.remark[intRemarks].text;
                                        }
                                    }

                                    if (objBookingNotificationRequestBookingRoom.bookingSupplement != null)
                                    {
                                        //Voy a añadir aquí las características de los suplementos, si hay
                                        for (int intSupplement = 0; intSupplement < objBookingNotificationRequestBookingRoom.bookingSupplement.Length; intSupplement++)
                                        {
                                            strREMARKS += " * ";
                                            strREMARKS += objBookingNotificationRequestBookingRoom.bookingSupplement[intSupplement].supplementCode + ",";
                                            strREMARKS += objBookingNotificationRequestBookingRoom.bookingSupplement[intSupplement].supplementName + ",";
                                            strREMARKS += objBookingNotificationRequestBookingRoom.bookingSupplement[intSupplement].supplementStatus + ",";
                                            strREMARKS += objBookingNotificationRequestBookingRoom.bookingSupplement[intSupplement].amount + ",";
                                        }
                                    }

                                    cmd.Parameters.AddWithValue("@COBSERVACIONES_RES", strREMARKS);

                                    cmd.Parameters.AddWithValue("@BDESGLOSADA_RGRRES", false);
                                    cmd.Parameters.AddWithValue("@NCOMPLEJO_ORIGEN_CPJRES", 1);

                                    cmd.Parameters.AddWithValue("@NCOMPLEJO_CPJRES", 1); //1: Sin Complejo

                                    cmd.Parameters.AddWithValue("@NTARJETA_DE_CREDITO_TCCRES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@CTITULAR_TARJETA_DE_CREDITO_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@CNUMERO_TARJETA_DE_CREDITO_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@CMES_CADUCIDAD_TARJETA_DE_CREDITO_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@CANYO_CADUCIDAD_TARJETA_DE_CREDITO_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@CCSV_TARJETA_DE_CREDITO_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@COBSERVACIONES_LLEGADA_RES", DBNull.Value);
                                    cmd.Parameters.AddWithValue("@COBSERVACIONES_SALIDA_RES", DBNull.Value);

                                    cmd.Parameters.AddWithValue("@NCAMAS_EXTRAS_RES", 0); //???????


                                    cmd.Parameters.AddWithValue("@NRESERVA_GRUPO_RGRRES", DBNull.Value);

                                    cmd.Parameters.AddWithValue("@BECOTASA_APLICADA_RES", false);

                                    cmd.Parameters.AddWithValue("@NADULTOS_ECOTASA_RES", objBookingNotificationRequestBookingRoom.guest.Length);
                                    cmd.Parameters.AddWithValue("@NNINS_ECOTASA_RES", 0);

                                    if (this.ValoresTasaTuristica(intHOTEL_WS, ref dblAdutosTasa, ref dblNinsTasa))
                                    {
                                        cmd.Parameters.AddWithValue("@NECOTASA_VALOR_POR_ADULTO_RES", dblAdutosTasa);
                                        cmd.Parameters.AddWithValue("@NECOTASA_VALOR_POR_NIN_RES", dblNinsTasa);
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@NECOTASA_VALOR_POR_ADULTO_RES", 0);
                                        cmd.Parameters.AddWithValue("@NECOTASA_VALOR_POR_NIN_RES", 0);
                                    }

                                    //cmd.Parameters.AddWithValue("@CCREATORID_GUESTINCOMING_RES", objRESERVA_GI.CreatorID);
                                    //cmd.Parameters.AddWithValue("@CUNIQUEID_GUESTINCOMING_RES", objRESERVA_GI.UniqueID);
                                    //cmd.Parameters.AddWithValue("@CLASTMODIFYDATETIME_GUESTINCOMING_RES", objRESERVA_GI.LastModifyDateTime);

                                    //for (int intPRECIO = 0; intPRECIO < objRESERVA_GI.Noches; intPRECIO++)
                                    //{
                                    //    dblTOTAL += arrPrecios[intPRECIO];
                                    //}
                                    #endregion

                                    cmd.CommandText = strSQL_PRERESERVAS_NGHPRX;
                                    t = Convert.ToInt32(cmd.ExecuteScalar());
                                    cmd.Parameters.Clear();

                                    intRES_PRX = t;

                                    strERROR_DESC += "Reserva NO MODIFICADA. Error al tratar de INSERTAR las Reservas Diarias en el Sistema (1)." + strDelimitadorLog;
                                    #region Asignamos valores a los parámetros para PRERESERVASDIA_NGHPRX y para PRECIOS_PRERESERVA_NGHPRX y ejecutamos las querys.
                                    for (intI = 0; intI <= intNOCHES_RES - 1; intI++)
                                    {
                                        //PRE RESERVAS DIARIAS
                                        #region Parámetros para PRERESERVASDIA_NGHPRX
                                        cmd.Parameters.AddWithValue("@NRESERVAID_RESRED", intRES_PRX);
                                        cmd.Parameters.AddWithValue("@DDIA_RED", dtmENTRADA.AddDays(double.Parse(intI.ToString())));
                                        cmd.Parameters.AddWithValue("@NHOTELID_HOTRED", intHOTEL_WS);
                                        cmd.Parameters.AddWithValue("@DVENTA_RED", dtmCREACION);
                                        cmd.Parameters.AddWithValue("@CUSUARIOID_USURED", "wshotetec");
                                        cmd.Parameters.AddWithValue("@DHORA_RED", dtmCREACION);
                                        cmd.Parameters.AddWithValue("@NTIPOESTANCIAID_TESRED", 1); //1: Agencia
                                        cmd.Parameters.AddWithValue("@NAGENCIAID_AGERED", intAGE_GI);
                                        cmd.Parameters.AddWithValue("@NCONTRATOID_CONRED", intCON_GI);
                                        cmd.Parameters.AddWithValue("@DENTRADA_RED", dtmENTRADA);
                                        cmd.Parameters.AddWithValue("@DSALIDA_RED", dtmSALIDA);
                                        cmd.Parameters.AddWithValue("@NNOCHES_RED", intNOCHES_RES);
                                        cmd.Parameters.AddWithValue("@NTIPOHABITACIONUSOID_THARED", intTHA);
                                        cmd.Parameters.AddWithValue("@NREGIMENUSO_REGRED", intREG);
                                        cmd.Parameters.AddWithValue("@NTIPOHABITACIONFRAID_THARED", intTHA);
                                        cmd.Parameters.AddWithValue("@NREGIMENFRA_REGRED", intREG);

                                        if (objBookingNotificationRequestBookingRoom.guest[0].personName != null)
                                            cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_RED", objBookingNotificationRequestBookingRoom.guest[0].personName.givenName + objBookingNotificationRequestBookingRoom.guest[0].personName.surname);
                                        else
                                            cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_RED", strPrimerOcupante);

                                        //DETERMINAR EL NÚMERO DE ADULTOS, NIÑOS, CUNAS CON LA FECHA DE NACIMIENTO
                                        cmd.Parameters.AddWithValue("@NADULTOS_RED", objBookingNotificationRequestBookingRoom.guest.Length);
                                        cmd.Parameters.AddWithValue("@NNINS_RED", 0);
                                        cmd.Parameters.AddWithValue("@NCUNAS_RED", 0);

                                        cmd.Parameters.AddWithValue("@CBONO_RED", strBookingReference);

                                        cmd.Parameters.AddWithValue("@NTRATOCLIENTE_TRCRED", 1); //1: Cliente
                                        cmd.Parameters.AddWithValue("@NESTADO_ERERED", 6); //6: Previo al 2
                                        cmd.Parameters.AddWithValue("@NESTADOFACTURA_EFRRED", 1); //1: Sin Facturar
                                        cmd.Parameters.AddWithValue("@NCOMPLEJO_REDCPJ", 1); //1: Sin Complejo

                                        try
                                        {
                                            if (objBookingNotificationRequestBookingRoom.amount != null && objBookingNotificationRequestBookingRoom.amount.Trim() != "")
                                                cmd.Parameters.AddWithValue("@NPRECIO_DIARIO_RED", double.Parse(objBookingNotificationRequestBookingRoom.amount.Replace(".", ",")) / intNOCHES_RES);
                                            else
                                                cmd.Parameters.AddWithValue("@NPRECIO_DIARIO_RED", 0);
                                        }
                                        catch
                                        {
                                            cmd.Parameters.AddWithValue("@NPRECIO_DIARIO_RED", 0);
                                        }
                                        #endregion

                                        cmd.CommandText = strSQL_PRERESERVASDIA_NGHPRX;
                                        cmd.ExecuteNonQuery();
                                        cmd.Parameters.Clear();

                                        if (objBookingNotificationRequestBookingRoom.amount != null && objBookingNotificationRequestBookingRoom.amount.Trim() != "")
                                        {
                                            strERROR_DESC += "Reserva NO AÑADIDA. Error al tratar de INSERTAR los PRECIOS de la Reserva en el Sistema" + strDelimitadorLog;
                                            //PRECIOS
                                            #region Parámetros para PRECIOS_PRERESERVA_NGHPRX
                                            cmd.Parameters.AddWithValue("@NIDENTIFICADOR_RESPRM", intRES_PRX);
                                            cmd.Parameters.AddWithValue("@DDESDE_PRM", dtmENTRADA.AddDays(double.Parse(intI.ToString())));
                                            cmd.Parameters.AddWithValue("@DHASTA_PRM", dtmENTRADA.AddDays(double.Parse(intI.ToString())));
                                            cmd.Parameters.AddWithValue("@NHOTELID_RESPRM", intHOTEL_WS);
                                            cmd.Parameters.AddWithValue("@NREGIMENID_REGPRM", intREG);
                                            cmd.Parameters.AddWithValue("@NTIPOHABITACIONID_THAPRM", intTHA);
                                            cmd.Parameters.AddWithValue("@NCOMPLEJO_CPJPRM", 1);
                                            cmd.Parameters.AddWithValue("@NTIPOPRECIO_PRM", 1); //Por HABITACIÓN (1)
                                            cmd.Parameters.AddWithValue("@NPRECIO_PRM", DBNull.Value); //Esto es Tipo Precio, 1: Total

                                            //POR AHORA PRECIO A PIÑÓN EN ALOJAMIENTO, ESCANDALLAR MEDIENTA PRECIOS Y CPRS POR DEFECTO
                                            dtPRE_HOT = new DataTable();
                                            dblDIA = 0;
                                            dblALOJAMIENTO = 0;
                                            dblDESAYUNO = 0;
                                            dblALMUERZO = 0;
                                            dblCENA = 0;
                                            dblALLINCLUSIVE = 0;
                                            dblPREMIUM = 0;

                                            //dblDIA = arrPrecios[intI];
                                            try
                                            {
                                                dblDIA = double.Parse(objBookingNotificationRequestBookingRoom.amount.Replace(".", ",")) / intNOCHES_RES;
                                            }
                                            catch
                                            {
                                                dblDIA = 0;
                                            }
                                            dblALOJAMIENTO = dblDIA;

                                            cmd.Parameters.AddWithValue("@NALOJAMIENTO_PRM", dblALOJAMIENTO);
                                            cmd.Parameters.AddWithValue("@NDESAYUNO_PRM", dblDESAYUNO);
                                            cmd.Parameters.AddWithValue("@NALMUERZO_PRM", dblALMUERZO);
                                            cmd.Parameters.AddWithValue("@NCENA_PRM", dblCENA);
                                            cmd.Parameters.AddWithValue("@NALLINCLUSIVE_PRM", dblALLINCLUSIVE);
                                            cmd.Parameters.AddWithValue("@NPREMIUM_PRM", dblPREMIUM);

                                            if (dblALOJAMIENTO == 0)
                                                cmd.Parameters.AddWithValue("@NALOJAMIENTOID_CPRPRM", DBNull.Value);
                                            else
                                                cmd.Parameters.AddWithValue("@NALOJAMIENTOID_CPRPRM", this.RecuperaCPR_Defecto("CPR_ALOJAMIENTO", intHOTEL_WS)); //13-08-2015 AHORA BUSCO POR RÉGIMEN

                                            if (dblDESAYUNO == 0)
                                                cmd.Parameters.AddWithValue("@NDESAYUNOID_CPRPRM", DBNull.Value);
                                            else
                                                cmd.Parameters.AddWithValue("@NDESAYUNOID_CPRPRM", this.RecuperaCPR_Defecto("CPR_DESAYUNO", intHOTEL_WS));


                                            if (dblALMUERZO == 0)
                                                cmd.Parameters.AddWithValue("@NALMUERZOID_CPRPRM", DBNull.Value);
                                            else
                                                cmd.Parameters.AddWithValue("@NALMUERZOID_CPRPRM", this.RecuperaCPR_Defecto("CPR_ALMUERZO", intHOTEL_WS));

                                            if (dblCENA == 0)
                                                cmd.Parameters.AddWithValue("@NCENAID_CPRPRM", DBNull.Value);
                                            else
                                                cmd.Parameters.AddWithValue("@NCENAID_CPRPRM", this.RecuperaCPR_Defecto("CPR_CENA", intHOTEL_WS));

                                            if (dblALLINCLUSIVE == 0)
                                                cmd.Parameters.AddWithValue("@NALLINCLUSIVE_CPRPRM", DBNull.Value);
                                            else
                                                cmd.Parameters.AddWithValue("@NALLINCLUSIVE_CPRPRM", this.RecuperaCPR_Defecto("CPR_ALL_INCLUSIVE", intHOTEL_WS));

                                            if (dblPREMIUM == 0)
                                                cmd.Parameters.AddWithValue("@NPREMIUM_CPRPRM", DBNull.Value);
                                            else
                                                cmd.Parameters.AddWithValue("@NPREMIUM_CPRPRM", this.RecuperaCPR_Defecto("CPR_PREMIUM", intHOTEL_WS));

                                            cmd.Parameters.AddWithValue("@NIMPORTEVENTA_PRM", DBNull.Value);
                                            cmd.Parameters.AddWithValue("@NPORCENTAJEVENTA_PRM", DBNull.Value);
                                            cmd.Parameters.AddWithValue("@NPRECIO_TOTAL_PRM", dblDIA);
                                            cmd.Parameters.AddWithValue("@BOFERTA_PRM", false);
                                            cmd.Parameters.AddWithValue("@APLICAR_DYS_DE_CONTRATO_PRM", false);
                                            cmd.Parameters.AddWithValue("@APLICAR_EBO_DE_CONTRATO_PRM", false);
                                            #endregion

                                            cmd.CommandText = strSQL_PRECIOS_PRERESERVA_NGHPRX;
                                            cmd.ExecuteNonQuery();
                                            cmd.Parameters.Clear();
                                        }
                                    }
                                    #endregion

                                    this.InsertarOcupantes(cmd, objBookingNotificationRequestBookingRoom, t);
                                }
                            }
                            break;
                        case 2: //CANCELACIÓN
                            strERROR_DESC += "Reserva NO CANCELADA." + strDelimitadorLog;
                            clsReservas objRESERVA_CANCELACION = new clsReservas();
                            if (this.VolcarReservaEnObjetoReserva(drRESERVA, intHOTEL_WS, ref objRESERVA_CANCELACION))
                            {
                                intResultadoUpdate = this.EliminarReservaEnOtraTransaccion(cmd, objRESERVA_CANCELACION, false, intHOTEL_WS);

                                if (intResultadoUpdate != 1)
                                {
                                    strERROR_DESC += "Error al tratar de CANCELAR la Reserva en el Sistema" + strDelimitadorLog;
                                    sqlTran.Rollback();
                                    con.Close();
                                    //strLOG_HOTETEC += "ERROR AL TRATAR DE MODIFICAR LA CANCELAR EN EL SISTEMA (1)" + strDelimitadorLog;
                                    return strERROR_DESC;
                                }
                            }
                            break;
                    }

                    sqlTran.Commit();
                    con.Close();

                    intRESERVA_CREADA = t;

                    return "";
                }
                catch (Exception ex)
                {
                    sqlTran.Rollback();
                    strMontaError = strERROR_DESC + strDelimitadorLog + "SE HA PRODUCCIDO UN ERROR (2): " + ex.Message;
                    strMontaError += strDelimitadorLog + "ServiceCode = " + strServiceCode;
                    strMontaError += strDelimitadorLog + "Hotel = " + intHOTEL_WS;
                    strMontaError += strDelimitadorLog + "Agencia = " + intAGE_GI;
                    strMontaError += strDelimitadorLog + "Contrato = " + intCON_GI;
                    strMontaError += strDelimitadorLog + "Tipo Habitación = " + intTHA;
                    strMontaError += strDelimitadorLog + "Régimen = " + intREG;

                    return strMontaError;
                    //return strERROR_DESC + strDelimitadorLog + "SE HA PRODUCCIDO UN ERROR (2): " + ex.Message;
                }
            }
        }
        private int HoraEntrada(int intHOTEL_WS)
        {
            string strSQL = "";
            DataTable dtDataTable_PAR;
            SqlDataAdapter daDataAdapter_PAR;
            try
            {
                strSQL = "SELECT NHORA_ENTRADA_HOT AS HORA FROM HOTELES_NGHHOT ";
                strSQL += "WHERE NHOTELID_HOT = " + intHOTEL_WS + " ";

                daDataAdapter_PAR = new SqlDataAdapter(strSQL, this.strCadenaDeConexion);

                dtDataTable_PAR = new DataTable();

                // Llenar la tabla con los datos indicados
                daDataAdapter_PAR.Fill(dtDataTable_PAR);

                // Y mostrar el primer registro
                if (dtDataTable_PAR != null && dtDataTable_PAR.Rows.Count > 0)
                {
                    if (dtDataTable_PAR.Rows[0]["HORA"] != DBNull.Value)
                        return int.Parse(dtDataTable_PAR.Rows[0]["HORA"].ToString());
                }

                return 13;
            }
            catch (Exception ex)
            {
                return 13;
            }
        }
        private int HoraSalida(int intHOTEL_WS)
        {
            string strSQL = "";
            DataTable dtDataTable_PAR;
            SqlDataAdapter daDataAdapter_PAR;
            try
            {
                strSQL = "SELECT NHORA_SALIDA_HOT AS HORA FROM HOTELES_NGHHOT ";
                strSQL += "WHERE NHOTELID_HOT = " + intHOTEL_WS + " ";

                daDataAdapter_PAR = new SqlDataAdapter(strSQL, this.strCadenaDeConexion);

                dtDataTable_PAR = new DataTable();

                // Llenar la tabla con los datos indicados
                daDataAdapter_PAR.Fill(dtDataTable_PAR);

                // Y mostrar el primer registro
                if (dtDataTable_PAR != null && dtDataTable_PAR.Rows.Count > 0)
                {
                    if (dtDataTable_PAR.Rows[0]["HORA"] != DBNull.Value)
                        return int.Parse(dtDataTable_PAR.Rows[0]["HORA"].ToString());
                }

                return 12;
            }
            catch (Exception ex)
            {
                return 12;
            }
        }
        private bool ValoresTasaTuristica(int intHotel, ref double dblADULTOS, ref double dblNINS)
        {
            SqlDataAdapter daDataAdapter_AUX;
            DataSet dsDataSet_AUX;
            DataTable dtDataTable_AUX;
            string strSQL_AUX = ""; //Guarda la query que enviamos a la base de datos.
            bool blnGenerarCargo = false;

            try
            {

                strSQL_AUX = "SELECT BECOTASA_GENERAR_CARGO_HOT, NECOTASA_ADULTO_HOT, NECOTASA_NIN_HOT ";
                strSQL_AUX = strSQL_AUX + "FROM HOTELES_NGHHOT ";
                strSQL_AUX = strSQL_AUX + "WHERE NHOTELID_HOT=" + intHotel + " ";

                daDataAdapter_AUX = new SqlDataAdapter(strSQL_AUX, this.strCadenaDeConexion);
                dsDataSet_AUX = new DataSet();
                dtDataTable_AUX = new DataTable();

                daDataAdapter_AUX.Fill(dsDataSet_AUX);

                if (dsDataSet_AUX == null || dsDataSet_AUX.Tables.Count == 0)
                    return false;

                dtDataTable_AUX = dsDataSet_AUX.Tables[0];

                if (dtDataTable_AUX == null || dtDataTable_AUX.Rows.Count == 0)
                    return false;

                if (dtDataTable_AUX.Rows[0]["BECOTASA_GENERAR_CARGO_HOT"] != DBNull.Value)
                    blnGenerarCargo = bool.Parse(dtDataTable_AUX.Rows[0]["BECOTASA_GENERAR_CARGO_HOT"].ToString());

                if (blnGenerarCargo)
                {
                    if (dtDataTable_AUX.Rows[0]["NECOTASA_ADULTO_HOT"] != DBNull.Value)
                        dblADULTOS = double.Parse(dtDataTable_AUX.Rows[0]["NECOTASA_ADULTO_HOT"].ToString());
                    if (dtDataTable_AUX.Rows[0]["NECOTASA_NIN_HOT"] != DBNull.Value)
                        dblNINS = double.Parse(dtDataTable_AUX.Rows[0]["NECOTASA_NIN_HOT"].ToString());
                }
                return blnGenerarCargo;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private int RecuperaCPR_Defecto(string strCentro, int intHOTEL_WS)
        {
            string strSQL = "";
            DataTable dtDataTable_PAR;
            SqlDataAdapter daDataAdapter_PAR;

            try
            {
                strSQL = "SELECT NVALOR_INT_PAR FROM PARAMETROS_AGHPAR ";
                strSQL += "WHERE CIDENTIFICADOR_PAR = '" + strCentro + "' ";
                strSQL += "AND NHOTEL_HOTPAR = " + intHOTEL_WS + " ";

                daDataAdapter_PAR = new SqlDataAdapter(strSQL, this.strCadenaDeConexion);

                dtDataTable_PAR = new DataTable();

                // Llenar la tabla con los datos indicados
                daDataAdapter_PAR.Fill(dtDataTable_PAR);

                // Y mostrar el primer registro
                if (dtDataTable_PAR.Rows.Count > 0)
                {

                    return int.Parse(dtDataTable_PAR.Rows[0][0].ToString());
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        private void InsertarOcupantes(SqlCommand cmd, BookingNotificationBookingNotificationRequestBookingRoom objBookingNotificationRequestBookingRoom, int intRES)
        {
            string strSQL_Edit;
            string strNombreTabla;
            int intAD = 0; //Adultos
            int intNI = 0; //Niños
            int intCU = 0; //Cunas
            int intPosicion = 1;
            int intI;
            DateTime dtmENTRADA;
            DateTime dtmSALIDA;

            // 28/03/2010
            //LOS OCUPANTES SE INSERTAN AL CREAR LA RESERVA
            // Insert en OCUPANTES_NGHOCU
            intAD = objBookingNotificationRequestBookingRoom.guest.Length;
            intNI = 0;
            intCU = 0;

            dtmENTRADA = DateTime.Parse(objBookingNotificationRequestBookingRoom.checkInDate);
            dtmSALIDA = DateTime.Parse(objBookingNotificationRequestBookingRoom.checkOutDate);

            #region Construir la query de inserción en OCUPANTES_NGHOCU.
            strNombreTabla = "OCUPANTES_NGHOCU";

            strSQL_Edit = "INSERT INTO " + strNombreTabla + " ("
                            + "RESERVA_RESOCU, NPOSICION_OCU, BCHECKIN_OCU, DENTRADA_OCU, DSALIDA_OCU, NTIPOPERSONA_TPEOCU, CPAISRESIDENCIA_PAIOCU, NTIPODOCUMENTO_TPIOCU, "
                            + "CNUMERODOCUMENTO_OCU, DFECHAEXPEDICION_OCU, CLUGAREXPEDICION_OCU, DFECHAVENCIMIENTO_OCU, CPAISNACIMIENTO_PAIOCU, DFECHANACIMIENTO_OCU, CPOBLACIONRESIDENCIA_OCU, CCPRESIDENCIA_OCU, CPROVINCIARESIDENCIA_OCU, "
                            + "CCOMUNIDADAUTONOMARESIDENCIA_OCU, CCOMUNIDADAUTONOMANACIMIENTO_OCU, CCALLERESIDENCIA_OCU, CPOBLACIONNACIMIENTO_OCU, CPROVINCIANACIMIENTO_OCU, CNOMBRE_OCU, CPRIMERAPELLIDO_OCU, "
                            + "CSEGUNDOAPELLIDO_OCU, NSEXO_SEXOCU, BNOSHOW_OCU, CNACIONALIDAD_PAIOCU, CIDIOMA_LANOCU, DFECHADESDE_OCU, DFECHAHASTA_OCU, CVUELOENTRADA_OCU, CVUELOSALIDA_OCU, "
                            + "DFECHABODA_OCU, CNOMBREPADRE_OCU, CNOMBREMADRE_OCU, CTELEFONO_OCU, CTELEFONOMOVIL_OCU, CFAX_OCU, "
                            + "CEMAIL_OCU, CCREDITCARD_OCU, NTIPOCREDITCARD_TCCOCU, CNUMEROPARTEVIAJERO_OCU, CMATRICULACOCHE_OCU, NNUMEROHABITACION_OCU, NEDAD_OCU "
                            + ") "
                            + "VALUES ("
                            + "@RESERVA_RESOCU, @NPOSICION_OCU, @BCHECKIN_OCU, @DENTRADA_OCU, @DSALIDA_OCU, @NTIPOPERSONA_TPEOCU, @CPAISRESIDENCIA_PAIOCU, @NTIPODOCUMENTO_TPIOCU, "
                            + "@CNUMERODOCUMENTO_OCU, @DFECHAEXPEDICION_OCU, @CLUGAREXPEDICION_OCU, @DFECHAVENCIMIENTO_OCU, @CPAISNACIMIENTO_PAIOCU, @DFECHANACIMIENTO_OCU, @CPOBLACIONRESIDENCIA_OCU, @CCPRESIDENCIA_OCU, @CPROVINCIARESIDENCIA_OCU, "
                            + "@CCOMUNIDADAUTONOMARESIDENCIA_OCU, @CCOMUNIDADAUTONOMANACIMIENTO_OCU, @CCALLERESIDENCIA_OCU, @CPOBLACIONNACIMIENTO_OCU, @CPROVINCIANACIMIENTO_OCU, @CNOMBRE_OCU, @CPRIMERAPELLIDO_OCU, "
                            + "@CSEGUNDOAPELLIDO_OCU, @NSEXO_SEXOCU, @BNOSHOW_OCU, @CNACIONALIDAD_PAIOCU, @CIDIOMA_LANOCU, @DFECHADESDE_OCU, @DFECHAHASTA_OCU, @CVUELOENTRADA_OCU, @CVUELOSALIDA_OCU, "
                            + "@DFECHABODA_OCU, @CNOMBREPADRE_OCU, @CNOMBREMADRE_OCU, @CTELEFONO_OCU, @CTELEFONOMOVIL_OCU, @CFAX_OCU, "
                            + "@CEMAIL_OCU, @CCREDITCARD_OCU, @NTIPOCREDITCARD_TCCOCU, @CNUMEROPARTEVIAJERO_OCU, @CMATRICULACOCHE_OCU, @NNUMEROHABITACION_OCU, @NEDAD_OCU "
                            + ") "
                            + "SELECT @@Identity";
            #endregion

            #region Asignamos valores a los parámetros para OCUPANTES_NGHOCU para ADULTOS.
            for (intI = 1; intI <= intAD; intI++)
            {
                cmd.Parameters.AddWithValue("@RESERVA_RESOCU", intRES);
                cmd.Parameters.AddWithValue("@NPOSICION_OCU", intPosicion);
                cmd.Parameters.AddWithValue("@BCHECKIN_OCU", false);
                cmd.Parameters.AddWithValue("@DENTRADA_OCU", dtmENTRADA);
                cmd.Parameters.AddWithValue("@DSALIDA_OCU", dtmSALIDA);
                cmd.Parameters.AddWithValue("@NTIPOPERSONA_TPEOCU", 2); //ADULTO
                cmd.Parameters.AddWithValue("@CPAISRESIDENCIA_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NTIPODOCUMENTO_TPIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNUMERODOCUMENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHAEXPEDICION_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CLUGAREXPEDICION_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHAVENCIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPAISNACIMIENTO_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHANACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPOBLACIONRESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCPRESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPROVINCIARESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCOMUNIDADAUTONOMARESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCOMUNIDADAUTONOMANACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCALLERESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPOBLACIONNACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPROVINCIANACIMIENTO_OCU", DBNull.Value);
                if (intPosicion == 1)
                {
                    if (objBookingNotificationRequestBookingRoom.guest[0].personName != null)
                        cmd.Parameters.AddWithValue("@CNOMBRE_OCU", objBookingNotificationRequestBookingRoom.guest[0].personName.givenName + objBookingNotificationRequestBookingRoom.guest[0].personName.surname);
                    else
                        cmd.Parameters.AddWithValue("@CNOMBRE_OCU", "");
                }
                else
                    cmd.Parameters.AddWithValue("@CNOMBRE_OCU", DBNull.Value);

                cmd.Parameters.AddWithValue("@CPRIMERAPELLIDO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CSEGUNDOAPELLIDO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NSEXO_SEXOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@BNOSHOW_OCU", false);
                cmd.Parameters.AddWithValue("@CNACIONALIDAD_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CIDIOMA_LANOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHADESDE_OCU", dtmENTRADA);
                cmd.Parameters.AddWithValue("@DFECHAHASTA_OCU", dtmSALIDA);
                //if (objReservasParam.VueloLlegada.Trim() == "")
                cmd.Parameters.AddWithValue("@CVUELOENTRADA_OCU", DBNull.Value);
                //else
                //    cmd.Parameters.AddWithValue("@CVUELOENTRADA_OCU", objReservasParam.VueloLlegada);
                //if (objReservasParam.VueloSalida.Trim() == "")
                cmd.Parameters.AddWithValue("@CVUELOSALIDA_OCU", DBNull.Value);
                //else
                //    cmd.Parameters.AddWithValue("@CVUELOSALIDA_OCU", objReservasParam.VueloSalida);
                cmd.Parameters.AddWithValue("@DFECHABODA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNOMBREPADRE_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNOMBREMADRE_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CTELEFONO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CTELEFONOMOVIL_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CFAX_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CEMAIL_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCREDITCARD_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NTIPOCREDITCARD_TCCOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNUMEROPARTEVIAJERO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CMATRICULACOCHE_OCU", DBNull.Value);

                //if (objReservasParam.Habitacion == "" || objReservasParam.Habitacion == null)
                cmd.Parameters.AddWithValue("@NNUMEROHABITACION_OCU", DBNull.Value);
                //else
                //    cmd.Parameters.AddWithValue("@NNUMEROHABITACION_OCU", objReservasParam.Habitacion);

                cmd.Parameters.AddWithValue("@NEDAD_OCU", DBNull.Value);

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                intPosicion++;
            }
            #endregion

            #region Asignamos valores a los parámetros para OCUPANTES_NGHOCU para NIÑOS.
            for (intI = 1; intI <= intNI; intI++)
            {
                cmd.Parameters.AddWithValue("@RESERVA_RESOCU", intRES);
                cmd.Parameters.AddWithValue("@NPOSICION_OCU", intPosicion);
                cmd.Parameters.AddWithValue("@BCHECKIN_OCU", false);
                cmd.Parameters.AddWithValue("@DENTRADA_OCU", dtmENTRADA);
                cmd.Parameters.AddWithValue("@DSALIDA_OCU", dtmSALIDA);
                cmd.Parameters.AddWithValue("@NTIPOPERSONA_TPEOCU", 4); //NIÑO
                cmd.Parameters.AddWithValue("@CPAISRESIDENCIA_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NTIPODOCUMENTO_TPIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNUMERODOCUMENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHAEXPEDICION_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CLUGAREXPEDICION_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHAVENCIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPAISNACIMIENTO_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHANACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPOBLACIONRESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCPRESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPROVINCIARESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCOMUNIDADAUTONOMARESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCOMUNIDADAUTONOMANACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCALLERESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPOBLACIONNACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPROVINCIANACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNOMBRE_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPRIMERAPELLIDO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CSEGUNDOAPELLIDO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NSEXO_SEXOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@BNOSHOW_OCU", false);
                cmd.Parameters.AddWithValue("@CNACIONALIDAD_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CIDIOMA_LANOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHADESDE_OCU", dtmENTRADA);
                cmd.Parameters.AddWithValue("@DFECHAHASTA_OCU", dtmSALIDA);
                //if (objReservasParam.VueloLlegada.Trim() == "")
                cmd.Parameters.AddWithValue("@CVUELOENTRADA_OCU", DBNull.Value);
                //else
                //    cmd.Parameters.AddWithValue("@CVUELOENTRADA_OCU", objReservasParam.VueloLlegada);
                //if (objReservasParam.VueloSalida.Trim() == "")
                cmd.Parameters.AddWithValue("@CVUELOSALIDA_OCU", DBNull.Value);
                //else
                //    cmd.Parameters.AddWithValue("@CVUELOSALIDA_OCU", objReservasParam.VueloSalida);
                cmd.Parameters.AddWithValue("@DFECHABODA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNOMBREPADRE_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNOMBREMADRE_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CTELEFONO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CTELEFONOMOVIL_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CFAX_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CEMAIL_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCREDITCARD_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NTIPOCREDITCARD_TCCOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNUMEROPARTEVIAJERO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CMATRICULACOCHE_OCU", DBNull.Value);
                //if (objReservasParam.Habitacion == "" || objReservasParam.Habitacion == null)
                cmd.Parameters.AddWithValue("@NNUMEROHABITACION_OCU", DBNull.Value);
                //else
                //    cmd.Parameters.AddWithValue("@NNUMEROHABITACION_OCU", objReservasParam.Habitacion);

                //if (objReservasParam.EdadNinsRequerida)
                //cmd.Parameters.AddWithValue("@NEDAD_OCU", objReservasParam.EdadNins[intI - 1]);
                //else
                cmd.Parameters.AddWithValue("@NEDAD_OCU", DBNull.Value);

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                intPosicion++;
            }
            #endregion

            #region Asignamos valores a los parámetros para OCUPANTES_NGHOCU para CUNAS.
            for (intI = 1; intI <= intCU; intI++)
            {
                cmd.Parameters.AddWithValue("@RESERVA_RESOCU", intRES);
                cmd.Parameters.AddWithValue("@NPOSICION_OCU", intPosicion);
                cmd.Parameters.AddWithValue("@BCHECKIN_OCU", false);
                cmd.Parameters.AddWithValue("@DENTRADA_OCU", dtmENTRADA);
                cmd.Parameters.AddWithValue("@DSALIDA_OCU", dtmSALIDA);
                cmd.Parameters.AddWithValue("@NTIPOPERSONA_TPEOCU", 5); //CUNA
                cmd.Parameters.AddWithValue("@CPAISRESIDENCIA_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NTIPODOCUMENTO_TPIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNUMERODOCUMENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHAEXPEDICION_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CLUGAREXPEDICION_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHAVENCIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPAISNACIMIENTO_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHANACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPOBLACIONRESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCPRESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPROVINCIARESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCOMUNIDADAUTONOMARESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCOMUNIDADAUTONOMANACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCALLERESIDENCIA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPOBLACIONNACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPROVINCIANACIMIENTO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNOMBRE_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CPRIMERAPELLIDO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CSEGUNDOAPELLIDO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NSEXO_SEXOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@BNOSHOW_OCU", false);
                cmd.Parameters.AddWithValue("@CNACIONALIDAD_PAIOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CIDIOMA_LANOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHADESDE_OCU", dtmENTRADA);
                cmd.Parameters.AddWithValue("@DFECHAHASTA_OCU", dtmSALIDA);
                //if (objReservasParam.VueloLlegada.Trim() == "")
                cmd.Parameters.AddWithValue("@CVUELOENTRADA_OCU", DBNull.Value);
                //else
                //cmd.Parameters.AddWithValue("@CVUELOENTRADA_OCU", objReservasParam.VueloLlegada);
                //if (objReservasParam.VueloSalida.Trim() == "")
                cmd.Parameters.AddWithValue("@CVUELOSALIDA_OCU", DBNull.Value);
                //else
                //cmd.Parameters.AddWithValue("@CVUELOSALIDA_OCU", objReservasParam.VueloSalida);
                //cmd.Parameters.AddWithValue("@CVUELOENTRADA_OCU", DBNull.Value);
                //cmd.Parameters.AddWithValue("@CVUELOSALIDA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@DFECHABODA_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNOMBREPADRE_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNOMBREMADRE_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CTELEFONO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CTELEFONOMOVIL_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CFAX_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CEMAIL_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CCREDITCARD_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@NTIPOCREDITCARD_TCCOCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CNUMEROPARTEVIAJERO_OCU", DBNull.Value);
                cmd.Parameters.AddWithValue("@CMATRICULACOCHE_OCU", DBNull.Value);
                //if (objReservasParam.Habitacion == "" || objReservasParam.Habitacion == null)
                cmd.Parameters.AddWithValue("@NNUMEROHABITACION_OCU", DBNull.Value);
                //else
                //cmd.Parameters.AddWithValue("@NNUMEROHABITACION_OCU", objReservasParam.Habitacion);

                cmd.Parameters.AddWithValue("@NEDAD_OCU", DBNull.Value);

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                intPosicion++;
            }
            #endregion
        }
        private bool VolcarReservaEnObjetoReserva(DataRow drRESERVA, int intHOTEL_WS, ref clsReservas objReservas_ins)
        {
            try
            {
                if (drRESERVA != null)
                {
                    //ID

                    objReservas_ins.ID = int.Parse(drRESERVA["NIDENTIFICADOR_RES"].ToString());
                    //Hotel ID

                    objReservas_ins.Hotel = intHOTEL_WS;
                    //Fecha Venta

                    objReservas_ins.FechaVenta = DateTime.Parse(drRESERVA["DVENTA_RES"].ToString());
                    //Usuario
                    objReservas_ins.Usuario = "";
                    objReservas_ins.Usuario_LOG = "";
                    //Hora
                    objReservas_ins.Hora = DateTime.Parse(drRESERVA["DHORA_RES"].ToString());
                    //Tipo Estancia
                    objReservas_ins.TipoEstancia = int.Parse(drRESERVA["NTIPOESTANCIAID_TESRES"].ToString());
                    //Agencia
                    objReservas_ins.Agencia = int.Parse(drRESERVA["NAGENCIAID_AGERES"].ToString());
                    //Contrato
                    objReservas_ins.Contrato = int.Parse(drRESERVA["NCONTRATOID_CONRES"].ToString());
                    //Entrada
                    objReservas_ins.Entrada = DateTime.Parse(drRESERVA["DENTRADA_RES"].ToString());
                    //Salida
                    objReservas_ins.Salida = DateTime.Parse(drRESERVA["DSALIDA_RES"].ToString());
                    //Noches
                    objReservas_ins.Noches = int.Parse(drRESERVA["NNOCHES_RES"].ToString());
                    //Day Use
                    objReservas_ins.DayUse = this.ValidaNullBLNAFalse(drRESERVA, "BDAYUSE_RES");
                    //Master
                    objReservas_ins.Master = this.ValidaNullBLNAFalse(drRESERVA, "BMASTER_RES");
                    //Grupo
                    objReservas_ins.Grupo = this.ValidaNullBLNAFalse(drRESERVA, "BGRUPO_RES");
                    //Referencia Factura
                    objReservas_ins.ReferenciaFactura = ValidaDatableNullStr(drRESERVA, "CREFERENCIAFACTURA_RES"); //Puede ser Null
                    //Full Credit
                    objReservas_ins.FullCredit = this.ValidaNullBLNAFalse(drRESERVA, "BFULLCREDIT_RES");
                    //Tipo Habitación Uso
                    objReservas_ins.TipoHabitacionUso = int.Parse(drRESERVA["NTIPOHABITACIONUSOID_THARES"].ToString());
                    //Tipo Habitación Factura
                    objReservas_ins.TipoHabitacionFra = int.Parse(drRESERVA["NTIPOHABITACIONFRAID_THARES"].ToString());
                    //Régimen Uso
                    objReservas_ins.RegimenUso = int.Parse(drRESERVA["NREGIMENUSO_REGRES"].ToString());
                    //Régimen Factura
                    objReservas_ins.RegimenFra = int.Parse(drRESERVA["NREGIMENFRA_REGRES"].ToString());
                    //Habitación
                    objReservas_ins.Habitacion = ValidaDatableNullStr(drRESERVA, "NHABITACION_HABRES");
                    //Hora Entrada
                    if (ValidaNullDTEAFalse(drRESERVA, "DHORAENTRADA_RES"))
                        objReservas_ins.HoraEntrada = DateTime.Parse(drRESERVA["DHORAENTRADA_RES"].ToString());
                    else
                        objReservas_ins.HoraEntrada = null;
                    //Hora Salida
                    if (ValidaNullDTEAFalse(drRESERVA, "DHORASALIDA_RES"))
                        objReservas_ins.HoraSalida = DateTime.Parse(drRESERVA["DHORASALIDA_RES"].ToString());
                    else
                        objReservas_ins.HoraSalida = null;
                    //Vuelo Llegada
                    objReservas_ins.VueloLlegada = ValidaDatableNullStr(drRESERVA, "CVUELOLLEGADA_RES"); //Puede ser Null
                    //Vuelo Salida

                    objReservas_ins.VueloSalida = ValidaDatableNullStr(drRESERVA, "CVUELOSALIDA_RES"); //Puede ser Null
                    //Crédito

                    objReservas_ins.Credito = this.ValidaNullBLNAFalse(drRESERVA, "BCREDITO_RES");
                    //Nombre Primer Ocupante

                    objReservas_ins.NombrePrimerOcupante = drRESERVA["CNOMBREPRIMEROCUPANTE_RES"].ToString();
                    //Adultos

                    objReservas_ins.Adultos = int.Parse(drRESERVA["NADULTOS_RES"].ToString());
                    //Niños

                    objReservas_ins.Nins = int.Parse(drRESERVA["NNINS_RES"].ToString());
                    //Cunas

                    objReservas_ins.Cunas = int.Parse(drRESERVA["NCUNAS_RES"].ToString());
                    //Bono

                    objReservas_ins.Bono = ValidaDatableNullStr(drRESERVA, "CBONO_RES"); //Puede ser Null
                    //Trato Cliente

                    objReservas_ins.TratoCliente = int.Parse(drRESERVA["NTRATOCLIENTE_TRCRES"].ToString());
                    //Desayuno Llegada

                    objReservas_ins.DesayunoLlegada = this.ValidaNullBLNAFalse(drRESERVA, "BDESAYUNOLLEGADA_RES");
                    //Almuerzo Llegada

                    objReservas_ins.AlmuerzoLlegada = this.ValidaNullBLNAFalse(drRESERVA, "BALMUERZOLLEGADA_RES");
                    //Cena Llegada

                    objReservas_ins.CenaLlegada = this.ValidaNullBLNAFalse(drRESERVA, "BCENALLEGADA_RES");
                    //Estado

                    objReservas_ins.Estado = int.Parse(drRESERVA["NESTADO_ERERES"].ToString());
                    //Estado Factura

                    objReservas_ins.EstadoFra = int.Parse(drRESERVA["NESTADOFACTURA_EFRRES"].ToString());
                    //Segmento

                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NSEGMENTO_SEGRES"))
                        objReservas_ins.Segmento = int.Parse(drRESERVA["NSEGMENTO_SEGRES"].ToString());
                    else
                        objReservas_ins.Segmento = null;
                    //Desayuno Salida

                    objReservas_ins.DesayunoSalida = this.ValidaNullBLNAFalse(drRESERVA, "BDESAYUNOSALIDA_RES");
                    //Almuerzo Salida

                    objReservas_ins.AlmuerzoSalida = this.ValidaNullBLNAFalse(drRESERVA, "BALMUERZOSALIDA_RES");
                    //Cena Salida

                    objReservas_ins.CenaSalida = this.ValidaNullBLNAFalse(drRESERVA, "BCENASALIDA_RES");
                    //Día Entrada

                    if (ValidaNullDTEAFalse(drRESERVA, "DDIAENTRADA_RES"))
                        objReservas_ins.DiaEntrada = DateTime.Parse(drRESERVA["DDIAENTRADA_RES"].ToString());
                    else
                        objReservas_ins.DiaEntrada = null;
                    //Día Salida

                    if (ValidaNullDTEAFalse(drRESERVA, "DDIASALIDA_RES"))
                        objReservas_ins.DiaSalida = DateTime.Parse(drRESERVA["DDIASALIDA_RES"].ToString());
                    else
                        objReservas_ins.DiaSalida = null;
                    //Turno Desayuno

                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NTURNODESAYUNO_TCORES"))
                        objReservas_ins.TurnoDesayuno = int.Parse(drRESERVA["NTURNODESAYUNO_TCORES"].ToString());
                    else
                        objReservas_ins.TurnoDesayuno = null;
                    //Turno Almuerzo

                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NTURNOALMUERZO_TCORES"))
                        objReservas_ins.TurnoAlmuerzo = int.Parse(drRESERVA["NTURNOALMUERZO_TCORES"].ToString());
                    else
                        objReservas_ins.TurnoAlmuerzo = null;
                    //Turno Cena

                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NTURNOCENA_TCORES"))
                        objReservas_ins.TurnoCena = int.Parse(drRESERVA["NTURNOCENA_TCORES"].ToString());
                    else
                        objReservas_ins.TurnoCena = null;
                    //Mercado

                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NMERCADO_MERRES"))
                        objReservas_ins.Mercado = int.Parse(drRESERVA["NMERCADO_MERRES"].ToString());
                    else
                        objReservas_ins.Mercado = null;
                    //Matrícula Coche

                    objReservas_ins.MatriculaCoche = ValidaDatableNullStr(drRESERVA, "CMATRICULACOCHE_RES"); //Puede ser Null
                    //No Show

                    objReservas_ins.NoShow = this.ValidaNullBLNAFalse(drRESERVA, "BNOSHOW_RES");
                    //Observaciones

                    objReservas_ins.Observaciones = ValidaDatableNullStr(drRESERVA, "COBSERVACIONES_RES"); //Puede ser Null
                    //Complejo Origen

                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NCOMPLEJO_ORIGEN_CPJRES"))
                        objReservas_ins.Complejo_Origen = int.Parse(drRESERVA["NCOMPLEJO_ORIGEN_CPJRES"].ToString());
                    else
                        objReservas_ins.Complejo_Origen = 1;
                    //Complejo

                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NCOMPLEJO_CPJRES"))
                        objReservas_ins.Complejo = int.Parse(drRESERVA["NCOMPLEJO_CPJRES"].ToString());
                    else
                        objReservas_ins.Complejo = 1;

                    //Tarjeta de Crédito
                    //Tipo

                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NTARJETA_DE_CREDITO_TCCRES"))
                        objReservas_ins.TipoTarjetaCredito = int.Parse(drRESERVA["NTARJETA_DE_CREDITO_TCCRES"].ToString());
                    else
                        objReservas_ins.TipoTarjetaCredito = null;
                    //Titular

                    objReservas_ins.TitularTarjetaCredito = ValidaDatableNullStr(drRESERVA, "CTITULAR_TARJETA_DE_CREDITO_RES"); //Puede ser Null
                    //Número

                    objReservas_ins.NumeroTarjetaCredito = ValidaDatableNullStr(drRESERVA, "CNUMERO_TARJETA_DE_CREDITO_RES"); //Puede ser Null
                    //Mes

                    objReservas_ins.MesCaducidadTarjetaCredito = ValidaDatableNullStr(drRESERVA, "CMES_CADUCIDAD_TARJETA_DE_CREDITO_RES"); //Puede ser Null
                    //Año

                    objReservas_ins.AnyoCaducidadTarjetaCredito = ValidaDatableNullStr(drRESERVA, "CANYO_CADUCIDAD_TARJETA_DE_CREDITO_RES"); //Puede ser Null
                    //CSV

                    objReservas_ins.CSVTarjetaCredito = ValidaDatableNullStr(drRESERVA, "CCSV_TARJETA_DE_CREDITO_RES"); //Puede ser Null

                    //Observaciones Llegada
                    objReservas_ins.Observaciones_Llegada = ValidaDatableNullStr(drRESERVA, "COBSERVACIONES_LLEGADA_RES"); //Puede ser Null
                    //Observaciones Salida
                    objReservas_ins.Observaciones_Salida = ValidaDatableNullStr(drRESERVA, "COBSERVACIONES_SALIDA_RES"); //Puede ser Null
                    //Camas Extras
                    if (this.ValidaDatableNullIntAFalse(drRESERVA, "NCAMAS_EXTRAS_RES"))
                        objReservas_ins.CamasExtra = int.Parse(drRESERVA["NCAMAS_EXTRAS_RES"].ToString());
                    else
                        objReservas_ins.CamasExtra = 0;

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private bool ValidaNullBLNAFalse(DataRow drValidaNull, string strColumna)
        {
            bool blnAux;
            try
            {
                blnAux = bool.Parse(drValidaNull[strColumna].ToString());
                return blnAux;
            }
            catch (FormatException ex)
            {
                return false;
            }
        }
        private string ValidaDatableNullStr(DataRow drValidaNull, string strNombreColumna)
        {
            try
            {
                return drValidaNull[strNombreColumna].ToString();
            }
            catch (FormatException ex)
            {
                return "";
            }
        }
        private bool ValidaDatableNullIntAFalse(DataRow drValidaNull, string strColumna)
        {
            int intAux;
            try
            {
                intAux = int.Parse(drValidaNull[strColumna].ToString());
                return true;
            }
            catch (FormatException ex)
            {
                return false;
            }
        }
        private bool ValidaNullDTEAFalse(DataRow drValidaNull, string strColumna)
        {
            DateTime dteAux;
            try
            {
                dteAux = DateTime.Parse(drValidaNull[strColumna].ToString());
                return true;
            }
            catch (FormatException ex)
            {
                return false;
            }
        }
        public int EliminarReservaEnOtraTransaccion(SqlCommand cmd, clsReservas objReservasParam, bool blnAnticipos, int intHOTEL_WS)
        {
            string sCon = this.strCadenaDeConexion;
            string strSQL_Edit;
            string strNombreTabla;
            string strUSU_LOG = "ws";
            DataTable dtANTICIPOS = new DataTable();
            DateTime dtmHora = new DateTime();
            bool blnEFECTIVO = false;
            decimal decIMPORTE = 0;

            strUSU_LOG = objReservasParam.Usuario_LOG;

            #region Construir la query de actualización en HABITACIONES_NGHHAB.
            strNombreTabla = "HABITACIONES_NGHHAB";

            strSQL_Edit = "UPDATE " + strNombreTabla + " SET "
                            + "BPREASIGNADA_HAB=@BPREASIGNADA_HAB "
                           + " WHERE CIDENTIFICADOR_HAB = @CIDENTIFICADOR_HAB AND NHOTEL_HOTHAB = @NHOTEL_HOTHAB ";
            #endregion

            try
            {
                if (objReservasParam.Habitacion != "" && objReservasParam.Habitacion != null)
                {
                    //Actualizamos el estado de la Habitación
                    this.HabitacionEliminarReserva(cmd, strSQL_Edit, objReservasParam.ID, objReservasParam.Hotel, objReservasParam.Habitacion);
                }

                //28/03/2010
                //NO ELIMINAMOS LA RESERVA DE LA BD SI NO QUE LA PASAMOS A ESTADO "ANULADA"
                #region Construir la query de eliminación en RESERVAS_DIARIAS_NGHRED.
                strNombreTabla = "RESERVAS_DIARIAS_NGHRED";

                strSQL_Edit = "UPDATE " + strNombreTabla + " SET "
                            + "NESTADO_ERERED=@NESTADO_ERERED "
                            + " WHERE NRESERVAID_RESRED=@NRESERVAID_RESRED ";

                //strSQL_Edit = "DELETE FROM " + strNombreTabla + " WHERE "
                //                    + " NRESERVAID_RESRED=@NRESERVAID_RESRED";
                #endregion

                #region Asignamos valores a los parametros para RESERVAS_DIARIAS_NGHRED.
                cmd.Parameters.AddWithValue("@NRESERVAID_RESRED", objReservasParam.ID);
                cmd.Parameters.AddWithValue("@NESTADO_ERERED", 1); //Anulada
                #endregion

                cmd.CommandText = strSQL_Edit;
                int t = cmd.ExecuteNonQuery();

                //020310
                //Eliminar tb precios manuales
                #region Construir la query de eliminación en CONDICIONES_ACOMP_MANUALES_NGHCOM.
                strNombreTabla = "CONDICIONES_ACOMP_MANUALES_NGHCOM";

                strSQL_Edit = "DELETE FROM " + strNombreTabla + " WHERE "
                                    + " NRESERVAID_DSMCOM=@NRESERVAID_DSMCOM";
                #endregion

                #region Asignamos valores a los parametros para CONDICIONES_ACOMP_MANUALES_NGHCOM.
                cmd.Parameters.AddWithValue("@NRESERVAID_DSMCOM", objReservasParam.ID);
                #endregion

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                #region Construir la query de eliminación en CONDICIONES_FECHA_MANUALES_NGHCFM.
                strNombreTabla = "CONDICIONES_FECHA_MANUALES_NGHCFM";

                strSQL_Edit = "DELETE FROM " + strNombreTabla + " WHERE "
                                    + " NRESERVAID_DSMCFM=@NRESERVAID_DSMCFM";
                #endregion

                #region Asignamos valores a los parametros para CONDICIONES_FECHA_MANUALES_NGHCFM.
                cmd.Parameters.AddWithValue("@NRESERVAID_DSMCFM", objReservasParam.ID);
                #endregion

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                #region Construir la query de eliminación en DESCUENTOS_SOBRE_ESTANCIAS_MANUALES_NGHDEM.
                strNombreTabla = "DESCUENTOS_SOBRE_ESTANCIAS_MANUALES_NGHDEM";

                strSQL_Edit = "DELETE FROM " + strNombreTabla + " WHERE "
                                    + " NRESERVAID_DSMDEM=@NRESERVAID_DSMDEM";
                #endregion

                #region Asignamos valores a los parametros para DESCUENTOS_SOBRE_ESTANCIAS_MANUALES_NGHDEM.
                cmd.Parameters.AddWithValue("@NRESERVAID_DSMDEM", objReservasParam.ID);
                #endregion

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                #region Construir la query de eliminación en DESCUENTOS_SOBRE_PERSONAS_MANUALES_NGHDPM.
                strNombreTabla = "DESCUENTOS_SOBRE_PERSONAS_MANUALES_NGHDPM";

                strSQL_Edit = "DELETE FROM " + strNombreTabla + " WHERE "
                                    + " NRESERVAID_DSMDPM=@NRESERVAID_DSMDPM";
                #endregion

                #region Asignamos valores a los parametros para DESCUENTOS_SOBRE_PERSONAS_MANUALES_NGHDPM.
                cmd.Parameters.AddWithValue("@NRESERVAID_DSMDPM", objReservasParam.ID);
                #endregion

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                #region Construir la query de eliminación en DESCUENTOS_Y_SUPLEMENTOS_MANUALES_NGHDSM.
                strNombreTabla = "DESCUENTOS_Y_SUPLEMENTOS_MANUALES_NGHDSM";

                strSQL_Edit = "DELETE FROM " + strNombreTabla + " WHERE "
                                    + " NRESERVAID_RESDSM=@NRESERVAID_RESDSM";
                #endregion

                #region Asignamos valores a los parametros para DESCUENTOS_Y_SUPLEMENTOS_MANUALES_NGHDSM.
                cmd.Parameters.AddWithValue("@NRESERVAID_RESDSM", objReservasParam.ID);
                #endregion

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                #region Construir la query de eliminación en PRECIOS_MANUALES_NGHPRM.
                strNombreTabla = "PRECIOS_MANUALES_NGHPRM";

                strSQL_Edit = "DELETE FROM " + strNombreTabla + " WHERE "
                                    + " NIDENTIFICADOR_RESPRM=@NIDENTIFICADOR_RESPRM";
                #endregion

                #region Asignamos valores a los parametros para PRECIOS_MANUALES_NGHPRM.
                cmd.Parameters.AddWithValue("@NIDENTIFICADOR_RESPRM", objReservasParam.ID);
                #endregion

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                #region Construir la query de actualización en RESERVAS_NGHRES.
                strNombreTabla = "RESERVAS_NGHRES";

                strSQL_Edit = "UPDATE " + strNombreTabla + " SET "
                            + "NESTADO_ERERES=@NESTADO_ERERES "
                            + " WHERE NIDENTIFICADOR_RES=@NIDENTIFICADOR_RES ";

                //strSQL_Edit = "DELETE FROM " + strNombreTabla + " WHERE "
                //                    + " NIDENTIFICADOR_RES=@NIDENTIFICADOR_RES";
                #endregion

                #region Asignamos valores a los parámetros para RESERVAS_NGHRES.
                cmd.Parameters.AddWithValue("@NIDENTIFICADOR_RES", objReservasParam.ID);
                cmd.Parameters.AddWithValue("@NESTADO_ERERES", 1); //Anulada
                #endregion

                cmd.CommandText = strSQL_Edit;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //ANTICIPOS
                if (blnAnticipos)
                {
                    dtANTICIPOS = this.AnticiposReserva(objReservasParam.ID, intHOTEL_WS);

                    if (dtANTICIPOS != null && dtANTICIPOS.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtANTICIPOS.Rows)
                        {
                            blnEFECTIVO = bool.Parse(dr["BEFECTIVO_MCA"].ToString());
                            decIMPORTE = decimal.Parse(dr["NTOTAL_IMPORTE_MCA"].ToString());
                            decIMPORTE = -decIMPORTE;

                            #region Construir la query de inserción en CAJAS_MOVIMIENTO_NGHMCA.
                            strNombreTabla = "CAJAS_MOVIMIENTO_NGHMCA";

                            strSQL_Edit = "INSERT INTO " + strNombreTabla + " SELECT "
                                            + "NHOTEL_HOT_MCA, CCAJA_CAJMCA, @DDIA_MCA_CONTRAANTICIPO, @DHORA_MCA_CONTRAANTICIPO, @CTURNO_MCA_CONTRAANTICIPO, @NCOBRO_MCA_CONTRAANTICIPO, CTIPO_COBRO_TCAMCA, "
                                            + "BEFECTIVO_MCA, @CFACTURA_FACMCA_CONTRAANTICIPO, CFACTURA_ANTICIPO_FACMCA, @CUSUARIO_USUMCA_CONTRAANTICIPO, @CCONCEPTO_COCMCA_CONTRAANTICIPO, @CDESCRIPCION_MCA_CONTRAANTICIPO, NCANTIDAD_MCA, "
                                            + "-NIMPORTE_MCA, -NTOTAL_IMPORTE_LOCAL_MCA, NIMPORTE_DEPOSITO_INTEGRADO_MCA, NANYO_RESERVA_MCA, NRESERVA_RESMCA, NDESGLOSE_DESMCA, NTEMPORAL_ARDMCA, "
                                            + "NAGRUPACION_AGRMCA, BREGISTRADO_MCA, @BFACTURADO_MCA_CONTRAANTICIPO, @DFECHA_FACTURADO_MCA_CONTRAANTICIPO, @NESTADO_DEPOSITO_EDEMCA_CONTRAANTICIPO, NESTADO_ANTICIPOS_DIRECTOS_EADMCA, CTIPO_MOVIMIENTO_TMVMCA, "
                                            + "NCAJA_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, DDIA_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, NTURNO_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, NCOBRO_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, NHOTEL_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, @BCONTABILIZADO_MCA_CONTRAANTICIPO, CEMPRESA_DESTINO_MCA, "
                                            + "@NESTADO_RESERVA_EREMCA_CONTRAANTICIPO, CDIVISA_DIVMCA, -NTOTAL_IMPORTE_MCA, CTITULAR_TARJETA_MCA, NNUMERO_TARJETA_MCA, NPUNTOS_FIDELIZACION_MCA, DFECHA_CADUCIDAD_TARJETA_MCA, "
                                            + "NMODIFICACIONES_MCA, CEVENTO_EVEMCA, CDOCUMENTO_EXTERNO_MCA, DFECHA_COBRO_CONTABILIZACION_EXTERNA_MCA, NRESERVA_PARKING_MCA, @BANULADO_MCA_CONTRAANTICIPO, NTICKET_TCKMCA, "
                                            + "NEXTENSION_EXTMCA, NDURACION_MCA, NPASOS_MCA, CTELEFONO_MCA, NIMPORTE_TIPO_CAMBIO_FRA_MCA, @BFACTURADO_FA_CONTRAANTICIPO "
                                            + "FROM " + strNombreTabla + " "
                                            + "WHERE "
                                            + "NHOTEL_HOT_MCA = @NHOTEL_HOT_MCA "
                                            + "AND CCAJA_CAJMCA = @CCAJA_CAJMCA "
                                            + "AND DDIA_MCA = @DDIA_MCA "
                                            + "AND DHORA_MCA = @DHORA_MCA ";
                            #endregion

                            #region Asignamos valores a los parámetros para CAJAS_MOVIMIENTO_NGHMCA.
                            cmd.Parameters.AddWithValue("@NHOTEL_HOT_MCA", int.Parse(dr["NHOTEL_HOT_MCA"].ToString()));
                            cmd.Parameters.AddWithValue("@CCAJA_CAJMCA", dr["CCAJA_CAJMCA"].ToString());
                            cmd.Parameters.AddWithValue("@DDIA_MCA", DateTime.Parse(dr["DDIA_MCA"].ToString()));
                            cmd.Parameters.AddWithValue("@DHORA_MCA", DateTime.Parse(dr["DHORA_MCA"].ToString()));

                            cmd.Parameters.AddWithValue("@DDIA_MCA_CONTRAANTICIPO", this.EstableceFechaTrabajo(intHOTEL_WS));
                            dtmHora = DateTime.Now;
                            //dtmHora = this.objUtilidades.FormateaHora(dtmHora, this.objParametrosRUTA.FECHA_DE_TRABAJO);
                            cmd.Parameters.AddWithValue("@DHORA_MCA_CONTRAANTICIPO", dtmHora);
                            cmd.Parameters.AddWithValue("@CTURNO_MCA_CONTRAANTICIPO", DBNull.Value);
                            cmd.Parameters.AddWithValue("@NCOBRO_MCA_CONTRAANTICIPO", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CFACTURA_FACMCA_CONTRAANTICIPO", DBNull.Value);
                            cmd.Parameters.AddWithValue("@CUSUARIO_USUMCA_CONTRAANTICIPO", "");
                            cmd.Parameters.AddWithValue("@CCONCEPTO_COCMCA_CONTRAANTICIPO", "ANTICIPO");
                            cmd.Parameters.AddWithValue("@CDESCRIPCION_MCA_CONTRAANTICIPO", "DEVOLUCION ANTICIPO RESERVA " + objReservasParam.ID);
                            cmd.Parameters.AddWithValue("@BFACTURADO_MCA_CONTRAANTICIPO", false);
                            cmd.Parameters.AddWithValue("@DFECHA_FACTURADO_MCA_CONTRAANTICIPO", DBNull.Value);
                            cmd.Parameters.AddWithValue("@NESTADO_DEPOSITO_EDEMCA_CONTRAANTICIPO", DBNull.Value);
                            cmd.Parameters.AddWithValue("@NESTADO_RESERVA_EREMCA_CONTRAANTICIPO", objReservasParam.Estado);
                            cmd.Parameters.AddWithValue("@BCONTABILIZADO_MCA_CONTRAANTICIPO", false);
                            cmd.Parameters.AddWithValue("@BANULADO_MCA_CONTRAANTICIPO", false);
                            cmd.Parameters.AddWithValue("@BFACTURADO_FA_CONTRAANTICIPO", false);
                            #endregion

                            cmd.CommandText = strSQL_Edit;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            #region Construir la query de actualización en CAJAS_CONTROL_NGHCCA.
                            strNombreTabla = "CAJAS_CONTROL_NGHCCA";

                            strSQL_Edit = "UPDATE " + strNombreTabla + " SET "
                                            + "NEFECTIVO_LOCAL_CCA=ISNULL(NEFECTIVO_LOCAL_CCA, 0) + @NEFECTIVO_LOCAL_CCA, "
                                            + "NNO_EFECTIVO_LOCAL_CCA=ISNULL(NNO_EFECTIVO_LOCAL_CCA, 0) + @NNO_EFECTIVO_LOCAL_CCA, "
                                            + "NEFECTIVO_CCA=ISNULL(NEFECTIVO_CCA, 0) + @NEFECTIVO_CCA, "
                                            + "NNO_EFECTIVO_CCA=ISNULL(NNO_EFECTIVO_CCA, 0) + @NNO_EFECTIVO_CCA, "
                                            + "CDIVISA_DIVCCA=@CDIVISA_DIVCCA "
                                            + "WHERE NHOTEL_HOTCCA = @NHOTEL_HOTCCA "
                                            + "AND CCAJA_CAJCCA=@CCAJA_CAJCCA "
                                            + "AND DDIA_CCA=@DDIA_CCA ";
                            #endregion

                            #region Asignamos valores a los parametros para CAJAS_CONTROL_NGHCCA.
                            cmd.Parameters.AddWithValue("@NHOTEL_HOTCCA", intHOTEL_WS);
                            cmd.Parameters.AddWithValue("@CCAJA_CAJCCA", dr["CCAJA_CAJMCA"].ToString());
                            cmd.Parameters.AddWithValue("@DDIA_CCA", DateTime.Parse(dr["DDIA_MCA"].ToString()));
                            if (blnEFECTIVO)
                            {
                                cmd.Parameters.AddWithValue("@NEFECTIVO_LOCAL_CCA", decIMPORTE);
                                cmd.Parameters.AddWithValue("@NNO_EFECTIVO_LOCAL_CCA", 0);
                                cmd.Parameters.AddWithValue("@NEFECTIVO_CCA", decIMPORTE);
                                cmd.Parameters.AddWithValue("@NNO_EFECTIVO_CCA", 0);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NEFECTIVO_LOCAL_CCA", 0);
                                cmd.Parameters.AddWithValue("@NNO_EFECTIVO_LOCAL_CCA", decIMPORTE);
                                cmd.Parameters.AddWithValue("@NEFECTIVO_CCA", 0);
                                cmd.Parameters.AddWithValue("@NNO_EFECTIVO_CCA", decIMPORTE);
                            }
                            cmd.Parameters.AddWithValue("@CDIVISA_DIVCCA", "EUR");
                            #endregion

                            cmd.CommandText = strSQL_Edit;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            #region Construir la query de actualización en CAJAS_CONTROL_TURNOS_NGHCTC.
                            strNombreTabla = "CAJAS_CONTROL_TURNOS_NGHCTC";

                            strSQL_Edit = "UPDATE " + strNombreTabla + " SET "
                                            + "NEFECTIVO_LOCAL_CTC=ISNULL(NEFECTIVO_LOCAL_CTC, 0) + @NEFECTIVO_LOCAL_CTC, "
                                            + "NNO_EFECTIVO_LOCAL_CTC=ISNULL(NNO_EFECTIVO_LOCAL_CTC, 0) + @NNO_EFECTIVO_LOCAL_CTC, "
                                            + "NEFECTIVO_CTC=ISNULL(NEFECTIVO_CTC, 0) + @NEFECTIVO_CTC, "
                                            + "NNO_EFECTIVO_CTC=ISNULL(NNO_EFECTIVO_CTC, 0) + @NNO_EFECTIVO_CTC, "
                                            + "CDIVISA_DIVCTC=@CDIVISA_DIVCTC "
                                            + "WHERE NHOTEL_HOTCTC = @NHOTEL_HOTCTC "
                                            + "AND CCAJA_CAJCTC=@CCAJA_CAJCTC "
                                            + "AND CTURNO_CTC=@CTURNO_CTC "
                                            + "AND DDIA_CTC=@DDIA_CTC ";
                            #endregion

                            #region Asignamos valores a los parametros para CAJAS_CONTROL_TURNOS_NGHCTC.
                            cmd.Parameters.AddWithValue("@NHOTEL_HOTCTC", intHOTEL_WS);
                            cmd.Parameters.AddWithValue("@CCAJA_CAJCTC", dr["CCAJA_CAJMCA"].ToString());
                            cmd.Parameters.AddWithValue("@CTURNO_CTC", dr["CTURNO_MCA"].ToString());
                            cmd.Parameters.AddWithValue("@DDIA_CTC", DateTime.Parse(dr["DDIA_MCA"].ToString()));
                            if (blnEFECTIVO)
                            {
                                cmd.Parameters.AddWithValue("@NEFECTIVO_LOCAL_CTC", decIMPORTE);
                                cmd.Parameters.AddWithValue("@NNO_EFECTIVO_LOCAL_CTC", 0);
                                cmd.Parameters.AddWithValue("@NEFECTIVO_CTC", decIMPORTE);
                                cmd.Parameters.AddWithValue("@NNO_EFECTIVO_CTC", 0);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@NEFECTIVO_LOCAL_CTC", 0);
                                cmd.Parameters.AddWithValue("@NNO_EFECTIVO_LOCAL_CTC", decIMPORTE);
                                cmd.Parameters.AddWithValue("@NEFECTIVO_CTC", 0);
                                cmd.Parameters.AddWithValue("@NNO_EFECTIVO_CTC", decIMPORTE);
                            }

                            cmd.Parameters.AddWithValue("@CDIVISA_DIVCTC", "EUR");
                            #endregion

                            cmd.CommandText = strSQL_Edit;
                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                    }
                }

                this.LOG_RESERVAS(cmd, objReservasParam, "ANULAR RESERVA", strUSU_LOG);

                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        private void HabitacionEliminarReserva(SqlCommand cmd, string strSQL_Edit, int intReserva, int intHotel, string strHabitacion)
        {
            SqlDataAdapter daDataAdapter_HCI;
            DataSet dsDataSet_HCI;
            DataTable dtDataTable_HCI;
            string strSQL_HCI = ""; //Guarda la query que enviamos a la base de datos.

            //Buscamos Reservas con Estado "RESERVA" que tengan asociada esa Habitación como Preasignada.
            strSQL_HCI = "SELECT NIDENTIFICADOR_RES FROM RESERVAS_NGHRES ";
            strSQL_HCI = string.Format("{0}WHERE NHABITACION_HABRES='{1}'", strSQL_HCI, strHabitacion);
            strSQL_HCI = string.Format("{0} AND NHOTELID_HOTRES={1} ", strSQL_HCI, intHotel);
            strSQL_HCI = string.Format("{0} AND NESTADO_ERERES=2 AND NIDENTIFICADOR_RES<>{1} ", strSQL_HCI, intReserva);

            daDataAdapter_HCI = new SqlDataAdapter(strSQL_HCI, this.strCadenaDeConexion);
            dsDataSet_HCI = new DataSet();
            dtDataTable_HCI = new DataTable();

            daDataAdapter_HCI.Fill(dsDataSet_HCI);

            dtDataTable_HCI = dsDataSet_HCI.Tables[0];

            if (dtDataTable_HCI.Rows.Count == 0) //Esto significa que la habitación no está asociada a ningua reserva "activa" como Preasignada
            {
                //Actualizamos la tabla de habitaciones a NO PREASIGNADA
                this.ActualizarPreasignacionHabitacion(cmd, strSQL_Edit, false, strHabitacion, intHotel);
            }
            else
            {
                //Actualizamos la tabla de habitaciones a PREASIGNADA
                this.ActualizarPreasignacionHabitacion(cmd, strSQL_Edit, true, strHabitacion, intHotel);
            }
        }
        private void ActualizarPreasignacionHabitacion(SqlCommand cmd, string strSQL_Edit, bool blnPreasignada, string strHabitacion, int intHotel)
        {
            #region Asignamos valores a los parámetros para HABITACIONES_NGHHAB.

            cmd.Parameters.AddWithValue("@BPREASIGNADA_HAB", blnPreasignada);
            cmd.Parameters.AddWithValue("@CIDENTIFICADOR_HAB", strHabitacion);
            cmd.Parameters.AddWithValue("@NHOTEL_HOTHAB", intHotel);
            cmd.CommandText = strSQL_Edit;
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
            #endregion
        }
        private DataTable AnticiposReserva(int intRESParam, int intHOTEL_WS)
        {
            SqlDataAdapter daDataAdapter_RES_Anticipos;
            DataTable dtDataTable_RES_Anticipos;
            string strSQL = ""; //Guarda la query que enviamos a la base de datos.

            //Generamos la consulta.
            strSQL = "SELECT NHOTEL_HOT_MCA, CCAJA_CAJMCA, DDIA_MCA, DHORA_MCA, CTURNO_MCA, NCOBRO_MCA, CTIPO_COBRO_TCAMCA, BEFECTIVO_MCA, CFACTURA_FACMCA, CFACTURA_ANTICIPO_FACMCA, CUSUARIO_USUMCA, CCONCEPTO_COCMCA, ";
            strSQL += "CDESCRIPCION_MCA, NCANTIDAD_MCA, NIMPORTE_MCA, NTOTAL_IMPORTE_LOCAL_MCA, NIMPORTE_DEPOSITO_INTEGRADO_MCA, NANYO_RESERVA_MCA, NRESERVA_RESMCA, NDESGLOSE_DESMCA, NTEMPORAL_ARDMCA, NAGRUPACION_AGRMCA, ";
            strSQL += "BREGISTRADO_MCA, BFACTURADO_MCA, DFECHA_FACTURADO_MCA, NESTADO_DEPOSITO_EDEMCA, NESTADO_ANTICIPOS_DIRECTOS_EADMCA, CTIPO_MOVIMIENTO_TMVMCA, NCAJA_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, DDIA_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, ";
            strSQL += "NTURNO_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, NCOBRO_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, NHOTEL_DEPOSITO_TRASPASOS_ASOCIADO_DTAMCA, BCONTABILIZADO_MCA, CEMPRESA_DESTINO_MCA, NESTADO_RESERVA_EREMCA, ";
            strSQL += "CDIVISA_DIVMCA, NTOTAL_IMPORTE_MCA, CTITULAR_TARJETA_MCA, NNUMERO_TARJETA_MCA, NPUNTOS_FIDELIZACION_MCA, DFECHA_CADUCIDAD_TARJETA_MCA, NMODIFICACIONES_MCA, CEVENTO_EVEMCA, CDOCUMENTO_EXTERNO_MCA, ";
            strSQL += "DFECHA_COBRO_CONTABILIZACION_EXTERNA_MCA, NRESERVA_PARKING_MCA, BANULADO_MCA, NTICKET_TCKMCA, NEXTENSION_EXTMCA, NDURACION_MCA, NPASOS_MCA, CTELEFONO_MCA, NIMPORTE_TIPO_CAMBIO_FRA_MCA, BFACTURADO_FA ";
            strSQL += "FROM CAJAS_MOVIMIENTO_NGHMCA ";
            strSQL += "WHERE CCONCEPTO_COCMCA = 'ANTICIPO' AND NRESERVA_RESMCA = " + intRESParam + " ";
            //strSQL += "AND NIMPORTE_MCA > 0 ";
            strSQL += "AND NHOTEL_HOT_MCA =  " + intHOTEL_WS + " ";

            try
            {
                daDataAdapter_RES_Anticipos = new SqlDataAdapter(strSQL, this.strCadenaDeConexion);

                dtDataTable_RES_Anticipos = new DataTable();

                daDataAdapter_RES_Anticipos.Fill(dtDataTable_RES_Anticipos);

                if (dtDataTable_RES_Anticipos != null && dtDataTable_RES_Anticipos.Rows.Count > 0)
                {
                    return dtDataTable_RES_Anticipos;
                }
                else
                {
                    string strETX = "";
                    strETX = "No se recuperan Anticipos"; //¿¿ES REALMENTE UN ERROR??
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private DateTime EstableceFechaTrabajo(int intHOTEL_WS)
        {
            //Coge el máximo de la tabla Dias Cerrados y le suma uno
            string strSQL = ""; //Guarda la query que enviamos a la base de datos.

            //Generamos la consulta.
            strSQL = this.ObtieneDiaTrabajo(intHOTEL_WS);

            SqlDataAdapter daDataAdapter = new SqlDataAdapter(strSQL, this.strCadenaDeConexion);

            DataTable dtDataTable = new DataTable();

            daDataAdapter.Fill(dtDataTable);

            if (dtDataTable.Rows.Count == 1)
            {
                if (this.ValidaNullDTEAFalse(dtDataTable.Rows[0], "DIA"))
                {
                    return DateTime.Parse(dtDataTable.Rows[0][0].ToString()).AddDays(1);
                }
            }

            return DateTime.Today;
        }
        private string ObtieneDiaTrabajo(int intHotel)
        {
            string strSQL = "";

            strSQL = "SELECT MAX(DDIA_DCE) AS DIA ";
            strSQL = strSQL + "FROM DIAS_CERRADOS_NGHDCE ";
            strSQL = strSQL + "WHERE ";
            strSQL = string.Format("{0}NHOTEL_HOTDCE={1} ", strSQL, intHotel);

            return strSQL;
        }
        public void LOG_RESERVAS(SqlCommand cmd, clsReservas objReservasParam, string strAccion, string strUSU_LOG)
        {
            string strSQL_Edit;
            string strNombreTabla;

            #region Construir la query de inserción en RESERVAS_HISTORIAL_NGHREH.
            strNombreTabla = "RESERVAS_HISTORIAL_NGHREH";

            strSQL_Edit = "INSERT INTO " + strNombreTabla + " ("
                            + "NRESERVA_RESREH, DFECHA_REH, CACCION_REH, CUSUARIO_LOG_USUREH, NHOTELID_HOTREH, DVENTA_REH, CUSUARIOID_USUREH, DHORA_REH, NTIPOESTANCIAID_TESREH, NAGENCIAID_AGEREH, "
                            + "NCONTRATOID_CONREH, DENTRADA_REH, DSALIDA_REH, NNOCHES_REH, BDAYUSE_REH, BMASTER_REH, BGRUPO_REH, CREFERENCIAFACTURA_REH, BFULLCREDIT_REH, "
                            + "NTIPOHABITACIONUSOID_THAREH, NTIPOHABITACIONFRAID_THAREH, NREGIMENUSO_REGREH, NREGIMENFRA_REGREH, NHABITACION_HABREH, DHORAENTRADA_REH, DHORASALIDA_REH, "
                            + "CVUELOLLEGADA_REH, CVUELOSALIDA_REH, BCREDITO_REH, CNOMBREPRIMEROCUPANTE_REH, NADULTOS_REH, NNINS_REH, NCUNAS_REH, CBONO_REH, NTRATOCLIENTE_TRCREH, "
                            + "BDESAYUNOLLEGADA_REH, BALMUERZOLLEGADA_REH, BCENALLEGADA_REH, NESTADO_EREREH, NESTADOFACTURA_EFRREH, NSEGMENTO_SEGREH, BDESAYUNOSALIDA_REH, "
                            + "BALMUERZOSALIDA_REH, BCENASALIDA_REH, DDIAENTRADA_REH, DDIASALIDA_REH, NTURNODESAYUNO_TCOREH, NTURNOALMUERZO_TCOREH, NTURNOCENA_TCOREH, NMERCADO_MERREH, "
                            + "CMATRICULACOCHE_REH, BNOSHOW_REH, COBSERVACIONES_REH "
                            + ") "
                            + "VALUES ("
                            + "@NRESERVA_RESREH, @DFECHA_REH, @CACCION_REH, @CUSUARIO_LOG_USUREH, @NHOTELID_HOTREH, @DVENTA_REH, @CUSUARIOID_USUREH, @DHORA_REH, @NTIPOESTANCIAID_TESREH, @NAGENCIAID_AGEREH, "
                            + "@NCONTRATOID_CONREH, @DENTRADA_REH, @DSALIDA_REH, @NNOCHES_REH, @BDAYUSE_REH, @BMASTER_REH, @BGRUPO_REH, @CREFERENCIAFACTURA_REH, @BFULLCREDIT_REH, "
                            + "@NTIPOHABITACIONUSOID_THAREH, @NTIPOHABITACIONFRAID_THAREH, @NREGIMENUSO_REGREH, @NREGIMENFRA_REGREH, @NHABITACION_HABREH, @DHORAENTRADA_REH, @DHORASALIDA_REH, "
                            + "@CVUELOLLEGADA_REH, @CVUELOSALIDA_REH, @BCREDITO_REH, @CNOMBREPRIMEROCUPANTE_REH, @NADULTOS_REH, @NNINS_REH, @NCUNAS_REH, @CBONO_REH, @NTRATOCLIENTE_TRCREH, "
                            + "@BDESAYUNOLLEGADA_REH, @BALMUERZOLLEGADA_REH, @BCENALLEGADA_REH, @NESTADO_EREREH, @NESTADOFACTURA_EFRREH, @NSEGMENTO_SEGREH, @BDESAYUNOSALIDA_REH, "
                            + "@BALMUERZOSALIDA_REH, @BCENASALIDA_REH, @DDIAENTRADA_REH, @DDIASALIDA_REH, @NTURNODESAYUNO_TCOREH, @NTURNOALMUERZO_TCOREH, @NTURNOCENA_TCOREH, @NMERCADO_MERREH, "
                            + "@CMATRICULACOCHE_REH, @BNOSHOW_REH, @COBSERVACIONES_REH "
                            + ") ";
            #endregion

            #region Asignamos valores a los parametros para RESERVAS_HISTORIAL_NGHREH.
            cmd.Parameters.AddWithValue("@NRESERVA_RESREH", objReservasParam.ID);
            cmd.Parameters.AddWithValue("@DFECHA_REH", DateTime.Now);
            cmd.Parameters.AddWithValue("@CACCION_REH", strAccion);
            if (strUSU_LOG == "" || strUSU_LOG == string.Empty)
                cmd.Parameters.AddWithValue("@CUSUARIO_LOG_USUREH", "");
            else
                cmd.Parameters.AddWithValue("@CUSUARIO_LOG_USUREH", strUSU_LOG);
            cmd.Parameters.AddWithValue("@NHOTELID_HOTREH", objReservasParam.Hotel);
            cmd.Parameters.AddWithValue("@DVENTA_REH", objReservasParam.FechaVenta);
            if (objReservasParam.Usuario == "" || objReservasParam.Usuario == null)
                cmd.Parameters.AddWithValue("@CUSUARIOID_USUREH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@CUSUARIOID_USUREH", objReservasParam.Usuario);
            cmd.Parameters.AddWithValue("@DHORA_REH", objReservasParam.Hora);
            cmd.Parameters.AddWithValue("@NTIPOESTANCIAID_TESREH", objReservasParam.TipoEstancia);
            cmd.Parameters.AddWithValue("@NAGENCIAID_AGEREH", objReservasParam.Agencia);
            cmd.Parameters.AddWithValue("@NCONTRATOID_CONREH", objReservasParam.Contrato);
            cmd.Parameters.AddWithValue("@DENTRADA_REH", objReservasParam.Entrada);
            cmd.Parameters.AddWithValue("@DSALIDA_REH", objReservasParam.Salida);
            cmd.Parameters.AddWithValue("@NNOCHES_REH", objReservasParam.Noches);
            cmd.Parameters.AddWithValue("@BDAYUSE_REH", objReservasParam.DayUse);
            cmd.Parameters.AddWithValue("@BMASTER_REH", objReservasParam.Master);
            cmd.Parameters.AddWithValue("@BGRUPO_REH", objReservasParam.Grupo);
            if (objReservasParam.ReferenciaFactura.Trim() == "")
                cmd.Parameters.AddWithValue("@CREFERENCIAFACTURA_REH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@CREFERENCIAFACTURA_REH", objReservasParam.ReferenciaFactura);
            cmd.Parameters.AddWithValue("@BFULLCREDIT_REH", objReservasParam.FullCredit);
            cmd.Parameters.AddWithValue("@NTIPOHABITACIONUSOID_THAREH", objReservasParam.TipoHabitacionUso);
            cmd.Parameters.AddWithValue("@NTIPOHABITACIONFRAID_THAREH", objReservasParam.TipoHabitacionFra);
            cmd.Parameters.AddWithValue("@NREGIMENUSO_REGREH", objReservasParam.RegimenUso);
            cmd.Parameters.AddWithValue("@NREGIMENFRA_REGREH", objReservasParam.RegimenFra);
            if (objReservasParam.Habitacion == "" || objReservasParam.Habitacion == null)
                cmd.Parameters.AddWithValue("@NHABITACION_HABREH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@NHABITACION_HABREH", objReservasParam.Habitacion);
            if (objReservasParam.HoraEntrada == null)
                cmd.Parameters.AddWithValue("@DHORAENTRADA_REH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@DHORAENTRADA_REH", objReservasParam.HoraEntrada);
            if (objReservasParam.HoraSalida == null)
                cmd.Parameters.AddWithValue("@DHORASALIDA_REH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@DHORASALIDA_REH", objReservasParam.HoraSalida);
            if (objReservasParam.VueloLlegada.Trim() == "")
                cmd.Parameters.AddWithValue("@CVUELOLLEGADA_REH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@CVUELOLLEGADA_REH", objReservasParam.VueloLlegada);
            if (objReservasParam.VueloSalida.Trim() == "")
                cmd.Parameters.AddWithValue("@CVUELOSALIDA_REH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@CVUELOSALIDA_REH", objReservasParam.VueloSalida);
            cmd.Parameters.AddWithValue("@BCREDITO_REH", objReservasParam.Credito);
            cmd.Parameters.AddWithValue("@CNOMBREPRIMEROCUPANTE_REH", objReservasParam.NombrePrimerOcupante);
            cmd.Parameters.AddWithValue("@NADULTOS_REH", objReservasParam.Adultos);
            cmd.Parameters.AddWithValue("@NNINS_REH", objReservasParam.Nins);
            cmd.Parameters.AddWithValue("@NCUNAS_REH", objReservasParam.Cunas);
            if (objReservasParam.Bono.Trim() == "")
                cmd.Parameters.AddWithValue("@CBONO_REH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@CBONO_REH", objReservasParam.Bono);
            cmd.Parameters.AddWithValue("@NTRATOCLIENTE_TRCREH", objReservasParam.TratoCliente);
            cmd.Parameters.AddWithValue("@BDESAYUNOLLEGADA_REH", objReservasParam.DesayunoLlegada);
            cmd.Parameters.AddWithValue("@BALMUERZOLLEGADA_REH", objReservasParam.AlmuerzoLlegada);
            cmd.Parameters.AddWithValue("@BCENALLEGADA_REH", objReservasParam.CenaLlegada);
            cmd.Parameters.AddWithValue("@NESTADO_EREREH", objReservasParam.Estado);
            cmd.Parameters.AddWithValue("@NESTADOFACTURA_EFRREH", objReservasParam.EstadoFra);
            if (objReservasParam.Segmento == null)
                cmd.Parameters.AddWithValue("@NSEGMENTO_SEGREH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@NSEGMENTO_SEGREH", objReservasParam.Segmento);
            cmd.Parameters.AddWithValue("@BDESAYUNOSALIDA_REH", objReservasParam.DesayunoSalida);
            cmd.Parameters.AddWithValue("@BALMUERZOSALIDA_REH", objReservasParam.AlmuerzoSalida);
            cmd.Parameters.AddWithValue("@BCENASALIDA_REH", objReservasParam.CenaSalida);
            if (objReservasParam.DiaEntrada == null)
                cmd.Parameters.AddWithValue("@DDIAENTRADA_REH", objReservasParam.Entrada);
            else
                cmd.Parameters.AddWithValue("@DDIAENTRADA_REH", objReservasParam.DiaEntrada);
            if (objReservasParam.DiaSalida == null)
                cmd.Parameters.AddWithValue("@DDIASALIDA_REH", objReservasParam.Salida);
            else
                cmd.Parameters.AddWithValue("@DDIASALIDA_REH", objReservasParam.DiaSalida);
            if (objReservasParam.TurnoDesayuno == null)
                cmd.Parameters.AddWithValue("@NTURNODESAYUNO_TCOREH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@NTURNODESAYUNO_TCOREH", objReservasParam.TurnoDesayuno);
            if (objReservasParam.TurnoAlmuerzo == null)
                cmd.Parameters.AddWithValue("@NTURNOALMUERZO_TCOREH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@NTURNOALMUERZO_TCOREH", objReservasParam.TurnoAlmuerzo);
            if (objReservasParam.TurnoCena == null)
                cmd.Parameters.AddWithValue("@NTURNOCENA_TCOREH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@NTURNOCENA_TCOREH", objReservasParam.TurnoCena);
            if (objReservasParam.Mercado == null)
                cmd.Parameters.AddWithValue("@NMERCADO_MERREH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@NMERCADO_MERREH", objReservasParam.Mercado);
            if (objReservasParam.MatriculaCoche.Trim() == "")
                cmd.Parameters.AddWithValue("@CMATRICULACOCHE_REH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@CMATRICULACOCHE_REH", objReservasParam.MatriculaCoche);
            cmd.Parameters.AddWithValue("@BNOSHOW_REH", objReservasParam.NoShow);
            if (objReservasParam.Observaciones.Trim() == "")
                cmd.Parameters.AddWithValue("@COBSERVACIONES_REH", DBNull.Value);
            else
                cmd.Parameters.AddWithValue("@COBSERVACIONES_REH", objReservasParam.Observaciones);
            #endregion

            cmd.CommandText = strSQL_Edit;
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }
        private int DecodificaTHA(string strRoomType, string strRoomTypeExtra, int intHotel, ref string strComentarioHabitacion)
        {
            int intTHA = -1;

            //TRITON (2)
            //ROOMS roomType    roomTypeExtra	Description                             SISTEMA
            //      SGL		    STD		        Individual Estándar                     ? es virtual
            //      DBL		    WBL		        Doble Sin Balcon                        D
            //      DBL		    WBC		        Doble Con Balcon                        DB
            //      DBL		    SSV		        Doble Vista lateral al mar              VML
            //      JSU		    STD		        Junior Suite Estandar                   JS
            //      JSU		    SSV		        Junior Suite Vista lateral al mar       JSM
            //      ROM		    DLX	            Habitacion Deluxe                       CHI

            //ILLOT (1)
            //ROOMS	roomType	roomTypeExtra	Description
            //      SGL	        STD	            Individual Estándar                     AP todas...
            //      DBL	        STD	            Doble Estandar
            //      DBL	        1	            Doble 1
            //      DBL	        2	            Doble 2
            //      TPL	        STD	            Triple Estandar
            //      TPL	        1	            Triple 1


            switch (strRoomType.ToLower())
            {
                case "sgl":
                    switch (intHotel)
                    {
                        case 1:
                            intTHA = 1; //IND
                            strComentarioHabitacion = "Individual Estándar";
                            break;
                        default:
                            intTHA = 17;
                            strComentarioHabitacion = "Individual Estándar";
                            break;
                    }
                    break;
                case "dbl":
                    switch (strRoomTypeExtra.ToLower())
                    {
                        case "wbl":
                            intTHA = 17;
                            strComentarioHabitacion = "Doble Sin Balcon";
                            break;
                        case "wbc":
                            intTHA = 18;
                            strComentarioHabitacion = "Doble Con Balcon";
                            break;
                        case "ssv":
                            intTHA = 19;
                            strComentarioHabitacion = "Doble Vista lateral al mar";
                            break;
                        case "std":
                            intTHA = 1;
                            strComentarioHabitacion = "Doble Estandar";
                            break;
                        case "1":
                            intTHA = 1;
                            strComentarioHabitacion = "Doble 1";
                            break;
                        case "2":
                            intTHA = 1;
                            strComentarioHabitacion = "Doble 2";
                            break;
                    }
                    break;
                case "jsu":
                    switch (strRoomTypeExtra.ToLower())
                    {
                        case "std":
                            intTHA = 20;
                            strComentarioHabitacion = "Junior Suite Estandar";
                            break;
                        case "ssv":
                            intTHA = 21;
                            strComentarioHabitacion = "Junior Suite Vista lateral al mar";
                            break;
                    }
                    break;
                case "rom":
                    intTHA = 22;
                    strComentarioHabitacion = "Habitacion Deluxe";
                    break;
                case "tpl":
                    switch (strRoomTypeExtra.ToLower())
                    {
                        case "std":
                            intTHA = 1;
                            strComentarioHabitacion = "Triple Estandar";
                            break;
                        case "1":
                            intTHA = 1;
                            strComentarioHabitacion = "Triple 1";
                            break;
                    }
                    break;
                default:
                    break;
            }

            if (intTHA == -1)
            {
                strComentarioHabitacion = "No se puede determinar el Tipo de Habitación";
                switch (intHotel)
                {
                    case 1:
                        intTHA = 1;
                        strComentarioHabitacion = "No se puede determinar el Tipo de Habitación. Se asigna 'AP' por defecto.";
                        break;
                    case 2:
                        intTHA = 17;
                        strComentarioHabitacion = "No se puede determinar el Tipo de Habitación. Se asigna 'D' por defecto.";
                        break;
                }
            }

            return intTHA;
        }
        private int DecodificaREG(string strBoard, ref string strComentarioRegimen)
        {
            int intREG = 1; //HD

            switch (strBoard.ToLower())
            {
                case "bb":
                    intREG = 1;
                    strComentarioRegimen = "Alojamiento y Desayuno";
                    break;
                case "hb":
                    intREG = 2;
                    strComentarioRegimen = "Media Pensión";
                    break;
                case "ai":
                    intREG = 4;
                    strComentarioRegimen = "Todo Incluido";
                    break;
                default:
                    strComentarioRegimen = "No se puede determinar el Régimen. Se asigna por defecto Alojamiento y Desayuno";
                    break;
            }

            return intREG;
        }
        private void AgenciaContratoHotetecCreaExiste(string strCODE, int intHotel, ref int intAGE, ref int intCON)
        {
            SqlDataAdapter daDataAdapter;
            DataTable dtDataTable;

            string strSQL = ""; //Guarda la query que enviamos a la base de datos.
            intAGE = -1;
            intCON = -1;

            try
            {
                //Generamos la consulta.
                strSQL = "SELECT NAGENCIA_AGEAHO, NCONTRATO_CONAHO ";
                strSQL += "FROM AGENCIAS_HOTETEC_NGHAHO ";
                strSQL += string.Format("WHERE CELEMENT_CODE_AHO = '{0}' ", strCODE);
                strSQL += string.Format(" AND NHOTEL_HOTAHO = {0} ", intHotel);

                daDataAdapter = new SqlDataAdapter(strSQL, this.strCadenaDeConexion);

                dtDataTable = new DataTable();

                daDataAdapter.Fill(dtDataTable);

                if (dtDataTable != null && dtDataTable.Rows.Count > 0)
                {
                    if (dtDataTable.Rows[0]["NAGENCIA_AGEAHO"] != DBNull.Value && dtDataTable.Rows[0]["NAGENCIA_AGEAHO"].ToString() != "")
                        intAGE = int.Parse(dtDataTable.Rows[0]["NAGENCIA_AGEAHO"].ToString());

                    if (dtDataTable.Rows[0]["NCONTRATO_CONAHO"] != DBNull.Value && dtDataTable.Rows[0]["NCONTRATO_CONAHO"].ToString() != "")
                        intCON = int.Parse(dtDataTable.Rows[0]["NCONTRATO_CONAHO"].ToString());
                }
            }
            catch (Exception ex)
            {
                return; //Si da error nos aseguramos que no inserte nada.
            }
        }
    }
}